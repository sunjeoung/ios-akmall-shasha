//
//  AKProperty.m
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 30..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKProperty.h"

@implementation AKProperty

#pragma mark - Public

+ (BOOL)isLogin {
    NSUserDefaults *ud = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUP_NAME];
    
    return [ud boolForKey:LOGIN_KEY];
}

+ (NSString *)getLoginToken {
    NSUserDefaults *ud = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUP_NAME];
    NSString *loginToken = [ud stringForKey:LOGINTOKEN_KEY];
    
    return loginToken ? loginToken : @"";
}

+ (void)setNotice:(NSString *)notice {
    NSUserDefaults *ud = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUP_NAME];
    
    [ud setObject:notice forKey:NOTICE_KEY];
    [ud synchronize];
}

+ (NSString *)getNotice {
    NSUserDefaults *ud = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUP_NAME];
    NSString *notice = [ud stringForKey:NOTICE_KEY];
    
    return notice ? notice : @"";
}

+ (void)setRefreshDate {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    [ud setFloat:[NSDate timeIntervalSinceReferenceDate] forKey:POPUPTODAYDATE_KEY];
    [ud synchronize];
}

+ (float)getRefreshDate {
    return [[NSUserDefaults standardUserDefaults] floatForKey:POPUPTODAYDATE_KEY];
}

+ (NSString *)getDateString:(NSDate *)date format:(NSString *)format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:format];
    
    return [formatter stringFromDate:date];
}

@end
