//
//  AKNetworkUtil.h
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 30..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "GlobalHeader.h"

typedef void (^successBlock)(NSDictionary * __nullable response);
typedef void (^failureBlock)(NSError * __nonnull error);

#define _successBlock       successBlock _Nullable
#define _failureBlock       failureBlock _Nullable

@interface AKNetworkUtil : NSObject

+ (void)requestLoginState:(UIActivityIndicatorView * __nullable)indicator success:(_successBlock)success failure:(_failureBlock)failure;

@end
