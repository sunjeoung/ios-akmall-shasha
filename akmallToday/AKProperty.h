//
//  AKProperty.h
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 30..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GlobalHeader.h"

@interface AKProperty : NSObject

+ (BOOL)isLogin;
+ (NSString * __nonnull)getLoginToken;
+ (void)setNotice:(NSString * __nonnull)notice;
+ (NSString * __nullable)getNotice;
+ (void)setRefreshDate;
+ (float)getRefreshDate;
+ (NSString * __nullable)getDateString:(NSDate * __nonnull)date format:(NSString * __nullable)format;

@end
