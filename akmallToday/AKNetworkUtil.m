//
//  AKNetworkUtil.m
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 30..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKNetworkUtil.h"

@implementation AKNetworkUtil

+ (AFHTTPSessionManager *)getManager {
    return [self getManager:@"text/html"];
}

+ (AFHTTPSessionManager *)getManager:(NSString *)contentType {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithArray:@[contentType]]];
    
    return manager;
}

+ (BOOL)onCommonResponse:(NSDictionary *)response {
    NSString *resultCode = response[@"resultCode"];
    
    if ([resultCode isEqualToString:@"0000"]) {
        return YES;
    }
    
    return NO;
}

+ (void)sendRequest:(NSString *)urlString parameters:(NSDictionary *)params success:(void (^)(NSDictionary *response))success failure:(void (^)(NSError * __nonnull))failure indicator:(UIActivityIndicatorView *)indicator {
    AFHTTPSessionManager *manager = [self getManager];
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_URL, urlString];
    NSLog(@"url = %@", url);
    NSLog(@"params = %@", params);
    indicator.hidden = NO;
    [indicator startAnimating];
    indicator.superview.userInteractionEnabled = NO;
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        [indicator stopAnimating];
        indicator.superview.userInteractionEnabled = YES;

        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dic = (NSDictionary *)responseObject;
            
            if ([self onCommonResponse:dic]) {
                if (success) {
                    success(dic);
                }
                
                return;
            }
        }
        
        if (failure) {
            NSError *error = [NSError errorWithDomain:@"akmall" code:-1 userInfo:@{NSLocalizedDescriptionKey: @"ResponseObject is invalid."}];
            
            failure(error);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [indicator stopAnimating];
        indicator.superview.userInteractionEnabled = YES;
        NSLog(@"Error: %@", error);
        
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark - Public

+ (void)requestLoginState:(UIActivityIndicatorView *)indicator success:(_successBlock)success failure:(_failureBlock)failure {
    [self sendRequest:URL_WIDGET_INFO parameters:nil success:success failure:failure indicator:indicator];
}

@end
