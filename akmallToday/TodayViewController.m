//
//  TodayViewController.m
//  akmallToday
//
//  Created by KimJinoug on 2016. 10. 21..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import "AKProperty.h"
#import "AKNetworkUtil.h"

@interface TodayViewController () <NCWidgetProviding>

@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UIView *noticeView;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@property (weak, nonatomic) IBOutlet UIView *loginView;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UILabel *loginLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *expandView;

@end

@implementation TodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userDefaultsDidChange:)
                                                 name:NSUserDefaultsDidChangeNotification
                                               object:nil];
    _searchView.layer.borderColor = [UIColor whiteColor].CGColor;
    [_loginBtn setBackgroundImage:[self imageFromColor:UIColorMake(0xE2, 0x01, 0x67)]
                         forState:UIControlStateNormal];
    self.extensionContext.widgetLargestAvailableDisplayMode = NCWidgetDisplayModeExpanded;
}

- (void)widgetActiveDisplayModeDidChange:(NCWidgetDisplayMode)activeDisplayMode withMaximumSize:(CGSize)maxSize {
    if (activeDisplayMode == NCWidgetDisplayModeExpanded) {
        _expandView.hidden = NO;
        self.preferredContentSize = CGSizeMake(0, 207);
    } else if (activeDisplayMode == NCWidgetDisplayModeCompact) {
        _expandView.hidden = YES;
        self.preferredContentSize = maxSize;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData
    [self updateLoginState];
    [self checkRefresh];

    completionHandler(NCUpdateResultNewData);
}

#pragma mark - Private

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)userDefaultsDidChange:(NSNotification *)notification {
    [self updateLoginState];
}

- (void)updateLoginState {
    _loginView.hidden = [AKProperty isLogin] && ![[AKProperty getLoginToken] isEqualToString:@""];
    _noticeView.hidden = ![AKProperty isLogin] || [[AKProperty getLoginToken] isEqualToString:@""];
    _bottomLabel.text = [AKProperty getNotice];
    _expandView.hidden = self.extensionContext.widgetActiveDisplayMode == NCWidgetDisplayModeCompact;
}

- (void)checkRefresh {
    if ([AKProperty isLogin] && ![[AKProperty getLoginToken] isEqualToString:@""]) {
        float current = [NSDate timeIntervalSinceReferenceDate];
        float thatTime = [AKProperty getRefreshDate];
        float dif = current - thatTime;
        
        if (dif >= 3600) {
            [self clickRefresh:nil];
        }
    }
}

- (NSString *)getCookieValue:(NSString *)key {
    NSString *value = nil;
    
    for (NSHTTPCookie *cookie in [NSHTTPCookieStorage sharedHTTPCookieStorage].cookies) {
        if ([cookie.properties[NSHTTPCookieName] isEqualToString:key]) {
            value = cookie.properties[NSHTTPCookieValue];
            break;
        }
    }
    
    return value;
}

- (void)setCookie:(NSString *)key value:(NSString *)value {
#ifdef PRIVATE_SERVER
    NSString *url = [BASE_URL substringFromIndex:@"http://".length];
#else
    NSString *url = @".akmall.com";
#endif
    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:@{NSHTTPCookieDomain: url, NSHTTPCookiePath: @"/", NSHTTPCookieName: key, NSHTTPCookieValue: value, NSHTTPCookieExpires: [NSDate dateWithTimeIntervalSinceNow:3600 * 24 * 365]}];
    
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
}

#pragma mark - IBAction

- (IBAction)clickSearch:(id)sender {
    NSURL *url = [NSURL URLWithString:@"akmall://search"];
    
    [self.extensionContext openURL:url completionHandler:nil];
}

- (IBAction)clickRefresh:(id)sender {
    [self setCookie:@"loginf" value:[NSString stringWithFormat:@"Y%@", [AKProperty isLogin] ? @"Y" : @"N"]];
    [self setCookie:@"loginToken" value:[AKProperty getLoginToken]];
    [AKProperty setRefreshDate];
    self->_bottomLabel.text = [AKProperty getNotice];
    

//    [AKNetworkUtil requestLoginState:(UIActivityIndicatorView *)_indicator success:^(NSDictionary * _Nullable response) {
//        NSString *feedMsg = response[@"myfeed"][@"feed_msg"];
//
//        if (feedMsg) {
//            [AKProperty setNotice:feedMsg];
//            self->_bottomLabel.text = feedMsg;
//        } else {
//            [AKProperty setNotice:@""];
//            self->_bottomLabel.text = @"";
//        }
//    } failure:^(NSError * _Nonnull error) {
//    }];
}

- (IBAction)clickLogin:(id)sender {
    NSURL *url = [NSURL URLWithString:@"akmall://login"];
    
    [self.extensionContext openURL:url completionHandler:nil];
}

- (IBAction)clickHome:(id)sender {
    NSURL *url = [NSURL URLWithString:@"akmall://home"];
    
    [self.extensionContext openURL:url completionHandler:nil];
}

- (IBAction)clickCheck:(id)sender {
    NSURL *url = [NSURL URLWithString:@"akmall://check"];
    
    [self.extensionContext openURL:url completionHandler:nil];
}

- (IBAction)clickEvent:(id)sender {
    NSURL *url = [NSURL URLWithString:@"akmall://event"];
    
    [self.extensionContext openURL:url completionHandler:nil];
}

- (IBAction)clickBag:(id)sender {
    NSURL *url = [NSURL URLWithString:@"akmall://bag"];
    
    [self.extensionContext openURL:url completionHandler:nil];
}

- (IBAction)clickCar:(id)sender {
    NSURL *url = [NSURL URLWithString:@"akmall://car"];
    
    [self.extensionContext openURL:url completionHandler:nil];
}

- (IBAction)clickMyFeed:(id)sender {
    NSURL *url = [NSURL URLWithString:@"akmall://feed"];
    
    [self.extensionContext openURL:url completionHandler:nil];
}

@end
