//
//  UserDataManager.h
//  BatonSOS
//
//  Created by Sanghong Han on 2018. 2. 27..
//  Copyright © 2018년 directionsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#define IUserDataManager [UserDataManager sharedInstance]

@interface UserDataManager : NSObject

+ (UserDataManager *)sharedInstance;

//- (void)setAppGuide;
//- (BOOL)getShowAppGuide;

// 프리로드 데이터 저장
- (void)setPreloadData:(NSDictionary *)preload;
- (NSDictionary *)getPrelaod;

// 헤더숨김 URL정보
- (void)setHiddenHeader:(NSArray *)arrUrls;
- (NSArray *)getHiddenHeaderUrl;

// 하단탭바숨김 URL정보
- (void)setHiddenTabbar:(NSArray *)arrUrls;
- (NSArray *)getHiddenTabbarUrl;

// 새창 URL정보
- (void)setNewWindow:(NSArray *)arrUrls;
- (NSArray *)getNewWindowUrl;

// 권한화면 보기 정보
- (void)setShowRights:(BOOL)isShow;
- (BOOL)getShowRights;

// lastPurchaseJson
- (void)setLastPurchaseJson:(NSString *)lastPurchaseJson;
- (NSString *)getLastPurchaseJson;


// 모바일 팝업 정보 저장
- (void)setMobilePopup:(NSArray *)arrPopup;
- (NSArray *)getMobilePopup;


// 모바일 팝업 '오늘 하루 보기 않기' 터치 한 date 정보 저장
- (void)setDidTouchDontShowTodayDate:(NSDate *)date;
- (NSDate *)getDidTouchDontShowTodayDate;


// 푸시알림화면 버튼 터치 한 date 정보 저장
- (void)setDidTouchPushAlarmDate:(NSDate *)date;
- (NSDate *)getDidTouchPushAlarmDate;

// 핑거푸시 보기 정보 최초진입시 노출
- (void)setFingerPushShow:(BOOL)isFingerPushShow;
- (BOOL)getFingerPushShow;

@end
