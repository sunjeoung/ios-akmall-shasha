//
//  UserDataManager.m
//  BatonSOS
//
//  Created by Sanghong Han on 2018. 2. 27..
//  Copyright © 2018년 directionsoft. All rights reserved.
//

#import "UserDataManager.h"

@implementation UserDataManager

#pragma mark - Create Singletone

+ (UserDataManager *)sharedInstance{
    static UserDataManager *userDataManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        userDataManager = [[super alloc] init];
        
    });
    
    return userDataManager;
}

#pragma mark - init

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    
    return self;
}

#pragma mark - Public Method

- (void)setPreloadData:(NSDictionary *)preload
{
    [[NSUserDefaults standardUserDefaults] setValue:preload forKey:@"preload"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSDictionary *)getPrelaod
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"preload"];
}

// 헤더숨김 URL정보
- (void)setHiddenHeader:(NSArray *)arrUrls
{
    [[NSUserDefaults standardUserDefaults] setValue:arrUrls forKey:@"hiddenHeader"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray *)getHiddenHeaderUrl
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"hiddenHeader"];
}

// 하단탭바숨김 URL정보
- (void)setHiddenTabbar:(NSArray *)arrUrls
{
    [[NSUserDefaults standardUserDefaults] setValue:arrUrls forKey:@"hiddenTabbar"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray *)getHiddenTabbarUrl
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"hiddenTabbar"];
}

// 새창 URL정보
- (void)setNewWindow:(NSArray *)arrUrls
{
    [[NSUserDefaults standardUserDefaults] setValue:arrUrls forKey:@"newWindow"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray *)getNewWindowUrl
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"newWindow"];
}

// 권한화면 보기 정보
- (void)setShowRights:(BOOL)isShow
{
    [[NSUserDefaults standardUserDefaults] setBool:isShow forKey:@"ShowRights"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)getShowRights
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"ShowRights"];
}

// lastPurchaseJson
- (void)setLastPurchaseJson:(NSString *)lastPurchaseJson
{
    [[NSUserDefaults standardUserDefaults] setValue:lastPurchaseJson forKey:@"lastPurchaseJson"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)getLastPurchaseJson
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"lastPurchaseJson"];
}

// 모바일 팝업
- (void)setMobilePopup:(NSArray *)arrPopup
{
    [[NSUserDefaults standardUserDefaults] setValue:arrPopup forKey:@"mobilePopup"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray *)getMobilePopup
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"mobilePopup"];
}

- (void)setDidTouchDontShowTodayDate:(NSDate *)date
{
    [[NSUserDefaults standardUserDefaults] setObject:date forKey:@"didTouchDontShowTodayDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSDate *)getDidTouchDontShowTodayDate
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"didTouchDontShowTodayDate"];
}


// 푸시알림화면 버튼 터치 한 date 정보 저장
- (void)setDidTouchPushAlarmDate:(NSDate *)date
{
    [[NSUserDefaults standardUserDefaults] setObject:date forKey:@"PushAlarmDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSDate *)getDidTouchPushAlarmDate
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"PushAlarmDate"];
}

// 핑거푸시 보기 정보 최초진입시 노출
- (void)setFingerPushShow:(BOOL)isFingerPushShow
{
    [[NSUserDefaults standardUserDefaults] setBool:isFingerPushShow forKey:@"FingerPushShow"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)getFingerPushShow
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"FingerPushShow"];
}

@end
