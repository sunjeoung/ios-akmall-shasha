//
//  CommonFunc.m
//  AKMall
//
//  Created by Sanghong Han on 2018. 4. 10..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "CommonFunc.h"

@implementation CommonFunc

+ (CGFloat)safeBottomY {
    if (@available(iOS 11.0, *)) {
        return UIApplication.sharedApplication.windows[0].safeAreaInsets.bottom;
    } else {
        return 0;
    }
}

@end
