//
//  CommonFunc.h
//  AKMall
//
//  Created by Sanghong Han on 2018. 4. 10..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonFunc : NSObject

+ (CGFloat)safeBottomY;

@end
