//
//  NSDictionary+TabDatas.m
//  AKMall
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "NSDictionary+TabDatas.h"

@implementation NSDictionary (TabDatas)

- (NSString *)tabName
{
    if (self[@"name"]) {
        return self[@"name"];
    }
    else return @"";
}

- (BOOL)isHasFlagName
{
    if (self[@"description"]) {
        return ![self[@"description"] isEqualToString:@""];
    }
    else return NO;
}

- (NSString *)flagName
{
    return @"";
}

- (BOOL)isWebView
{
    if (self[@"type"]) {
        return [self[@"type"] isEqualToString:@"web"];
    }
    else return YES;
}

- (NSString *)url
{
    if (self[@"url"]) {
        return self[@"url"];
    }
    else return @"";
}

- (NSString *)description
{
    if (self[@"description"]) {
        return self[@"description"];
    }
    else return @"";
}

- (BOOL)isNew
{
    if (self[@"newyn"]) {
        return [self[@"newyn"] isEqualToString:@"Y"];
    }
    else return NO;
}

@end
