//
//  UIScrollView+Customs.h
//  AKMall
//
//  Created by Sanghong Han on 2018. 4. 10..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (Customs)

- (BOOL)isAtbottom;

@end
