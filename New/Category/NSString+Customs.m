//
//  NSString+Customs.m
//  AKMall
//
//  Created by Sanghong Han on 2018. 4. 25..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "NSString+Customs.h"

@implementation NSString (Customs)

- (NSDictionary *)parseURLParams {
    NSArray *pairs = [self componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        if ([kv count] == 2) {
            NSString *val = [kv[1] stringByRemovingPercentEncoding];
            params[kv[0]] = val;
        }
    }
    
    return params;
}

@end
