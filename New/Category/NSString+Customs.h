//
//  NSString+Customs.h
//  AKMall
//
//  Created by Sanghong Han on 2018. 4. 25..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Customs)

- (NSDictionary *)parseURLParams;

@end
