//
//  UIScrollView+Customs.m
//  AKMall
//
//  Created by Sanghong Han on 2018. 4. 10..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "UIScrollView+Customs.h"

@implementation UIScrollView (Customs)

- (BOOL)isAtbottom
{
    CGFloat scrollViewHeight = self.bounds.size.height;
    CGFloat scrollContentSizeHeight = self.contentSize.height;
    CGFloat bottomInset = self.contentInset.bottom;
    CGFloat scrollViewBottomOffset = scrollContentSizeHeight + bottomInset - scrollViewHeight;
    
    return (self.contentOffset.y >= scrollViewBottomOffset);
}

@end
