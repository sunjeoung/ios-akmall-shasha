//
//  NSDictionary+TabDatas.h
//  AKMall
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (TabDatas)

- (NSString *)tabName;
- (BOOL)isHasFlagName;
- (NSString *)flagName;
- (BOOL)isWebView;
- (NSString *)url;
- (NSString *)description;
- (BOOL)isNew;

@end
