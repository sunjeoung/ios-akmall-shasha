//
//  SubToolbar.h
//  AKMall
//
//  Created by seoyeon on 2018. 3. 27..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SubToolbarType) {
    SubToolbarTypeNone = -1,
    SubToolbarTypeBack = 1300,
    SubToolbarTypeLikeIt,
    SubToolbarTypeOrder,
    SubToolbarTypeHome,
    SubToolbarTypeMyAK,
    SubToolbarTypeHistory
};

@protocol SubToolbarDelegate <NSObject>

@optional
- (void)didTouchSubToolbar:(SubToolbarType)type;
@end

@interface SubToolbar : UIView

@property (nonatomic, assign) id<SubToolbarDelegate> delegate;

+ (CGFloat)height;

@end
