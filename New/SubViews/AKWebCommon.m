//
//  AKWebCommon.m
//  AKMall
//
//  Created by Sanghong Han on 2020/08/18.
//  Copyright © 2020 sjsofttech. All rights reserved.
//

#import "AKWebCommon.h"

@interface AKWebCommon() {
    
}

@end

@implementation WKWebViewPoolHandler

+ (WKProcessPool *) pool
{
    static dispatch_once_t onceToken;
    static WKProcessPool *_pool;
    dispatch_once(&onceToken, ^{
        _pool = [[WKProcessPool alloc] init];
    });
    return _pool;
}

@end

@implementation AKWebCommon

@end
