//
//  AKSubHeaderVie.m
//  AKMall
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "AKSubHeaderView.h"
#import "GlobalHeader.h"

#define MAS_SHORTHAND
#define MAS_SHORTHAND_GLOBALS
#import "Masonry.h"

@interface AKSubHeaderView()
{
    UIButton *_btnCategory;
    UIButton *_btnLogo;
    UIButton *_btnSearch;
    
    UILabel *_cartCountLabel;
    
    UIView *_line;
}
@property (nonatomic, strong) UIButton *btnCart;
@end

@implementation AKSubHeaderView

#pragma mark - Button Touch Event

- (void)didTouchSubHeaderButton:(UIButton *)btn
{
    AKSubHeaderViewType type = (AKSubHeaderViewType)btn.tag;
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTouchSubHeaderView:)]) {
        [self.delegate didTouchSubHeaderView:type];
    }
}

#pragma mark - NSNotification Method

- (void)setNewAlarm
{
    _btnCategory.selected = [appDelegate isNewAlarm];
}

- (void)setCartCount
{
    _cartCountLabel.text = [NSString stringWithFormat:@"%d", [appDelegate nCartCount]];
    _btnCart.selected = ([appDelegate nCartCount] > 0);
}

#pragma mark - init

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) {
        return self;
    }
    
    _btnCategory = ({
        UIButton *btn = UIButton.new;
        //btn.backgroundColor = [UIColor redColor];
        btn.tag = AKSubHeaderViewTypeCategory;
        [btn setImage:[UIImage imageNamed:@"main_gnb_menu"] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"main_gnb_menu_on"] forState:UIControlStateSelected];
        [btn addTarget:self action:@selector(didTouchSubHeaderButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    _btnLogo = ({
        UIButton *btn = UIButton.new;
        //btn.backgroundColor = [UIColor yellowColor];
        btn.tag = AKSubHeaderViewTypeLogo;
        [btn setImage:[UIImage imageNamed:@"main_gnb_logo"] forState:UIControlStateNormal];
        //[btn setImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
        [btn addTarget:self action:@selector(didTouchSubHeaderButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    self.btnCart = ({
        UIButton *btn = UIButton.new;
        //btn.backgroundColor = [UIColor blueColor];
        btn.tag = AKSubHeaderViewTypeCart;
        [btn setImage:[UIImage imageNamed:@"main_gnb_cart"] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"main_gnb_cart_on"] forState:UIControlStateSelected];
        [btn addTarget:self action:@selector(didTouchSubHeaderButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    _btnSearch = ({
        UIButton *btn = UIButton.new;
       // btn.backgroundColor = [UIColor cyanColor];
        btn.tag = AKSubHeaderViewTypeSearchIcon;
        //[btn setImage:[UIImage imageNamed:@"main_gnb_search"] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:@"main_gnb_search"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(didTouchSubHeaderButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    _cartCountLabel = ({
        UILabel *label = UILabel.new;
        label.backgroundColor = [UIColor clearColor];
        label.textColor = UIColorFromRGB(0xFFFFFF);
        label.font = [UIFont systemFontOfSize:9.f];
        label.textAlignment = NSTextAlignmentCenter;
        [_btnCart addSubview:label];
        label;
    });
    
    _line = ({
        UIView *view = UIView.new;
        view.backgroundColor = UIColorFromRGB(0xE0E0E0);
        [self addSubview:view];
        view;
    });
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setNewAlarm) name:kNewAlarmNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setCartCount) name:kCartCountNotification object:nil];
    
    return self;
}

- (void)layoutSubviews
{
    [_btnCategory updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(17);
        make.top.equalTo(15);
        make.width.equalTo(26);
        make.height.equalTo(20);
    }];
    
    [_btnLogo updateConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(16);
        make.width.equalTo(70);
        make.height.equalTo(20);
    }];
    
    [_btnCart updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.right).offset(-10);
        make.top.equalTo(9);
        make.width.equalTo(35);
        make.height.equalTo(28);
    }];

    
    [_btnSearch updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.btnCart.left).offset(-18);
        make.centerY.equalTo(self);
        make.width.equalTo(20);
        make.height.equalTo(20);
    }];
    
    [_cartCountLabel updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.btnCart.right);
        make.top.equalTo(self.btnCart.top);
        make.height.equalTo(17);
        make.width.equalTo(17);
    }];
    
    /*
    [_line updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.left);
        make.right.equalTo(self.right);
        make.bottom.equalTo(self.bottom).offset(-1);
        make.height.equalTo(1);
    }];
    */
    
    _cartCountLabel.text = [NSString stringWithFormat:@"%d", [appDelegate nCartCount]];
    _btnCategory.selected = [appDelegate isNewAlarm];
    _btnCart.selected = ([appDelegate nCartCount] > 0);
}

@end
