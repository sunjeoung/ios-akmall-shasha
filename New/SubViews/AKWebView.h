//
//  AKWebView.h
//  AKMall
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@protocol AKWebViewDelegate <NSObject>

@optional
- (void)didGotoSubViewController:(NSString *)aUrl;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)moveTabIndex:(NSInteger)nIndex;
@end

@interface AKWebView : UIView

@property (atomic, strong) WKWebView *webView;
@property (nonatomic, assign) id<AKWebViewDelegate> delegate;

- (void)requestUrl:(NSString *)url;
- (void)reloadWebView;
- (void)callGATrackerJavaScript:(NSString *)script;

@end
