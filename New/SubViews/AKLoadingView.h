//
//  AKLoadingView.h
//  AKMall
//
//  Created by Sanghong Han on 2018. 9. 18..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kHideLoadingNotification  @"HideLoadingNotification"

@interface AKLoadingView : UIView

+ (void)showLoadingView;
+ (void)hideLoadingView;

@end
