//
//  MainToolbar.h
//  AKMall
//
//  Created by seoyeon on 2018. 3. 27..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MainToolbarType) {
    MainToolbarTypeNone = -1,
    MainToolbarTypeLikeIt = 1200,
    MainToolbarTypeOrder,
    MainToolbarTypeHome,
    MainToolbarTypeMyAK,
    MainToolbarTypeHistory
};

@protocol MainToolbarDelegate <NSObject>

@optional
- (void)didTouchMainToolbar:(MainToolbarType)type;
@end

@interface MainToolbar : UIView

@property (nonatomic, assign) id<MainToolbarDelegate> delegate;

+ (CGFloat)height;

@end
