//
//  CPTabMenuView.h
//  11st
//
//  Created by spearhead on 2014. 8. 28..
//  Copyright (c) 2014년 Commerce Planet. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kTabMenuHeight  50

@protocol CPTabMenuViewDelegate;

@interface CPTabMenuView : UIScrollView;

@property (nonatomic, weak) id<CPTabMenuViewDelegate> tabdelegate;
@property (nonatomic, strong) NSArray *arrMenus;

- (id)initWithFrame:(CGRect)frame menuTitleItems:(NSArray *)menuTitleItems menuContentsItems:(NSArray *)menuContentsItems;
- (void)tabMenuCurrentItemIndexDidChange:(NSInteger)index;
- (void)loadTabMenu;

@end

@protocol CPTabMenuViewDelegate <NSObject>
@optional
- (void)didTouchTabMenuButton:(NSInteger)index;

@end
