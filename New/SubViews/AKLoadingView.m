//
//  AKLoadingView.m
//  AKMall
//
//  Created by Sanghong Han on 2018. 9. 18..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "AKLoadingView.h"
#import "DGActivityIndicatorView.h"

#define kLoadingViewSize    30

@interface AKLoadingView()
{
    UIImageView *imgLoader;
    DGActivityIndicatorView *activityIndicatorView;
}
@end

@implementation AKLoadingView

#pragma mark - Private Method

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self ) {
        
        self.backgroundColor = [UIColor clearColor];
        
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        
        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallSpinFadeLoader tintColor:UIColorFromRGB(0xe60168) size:kLoadingViewSize];
        activityIndicatorView.frame = CGRectMake(0.0f, 0.0f, 50, 50);
        [self addSubview:activityIndicatorView];
        
        /*
        imgLoader = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kLoadingViewSize, kLoadingViewSize)];
        imgLoader.clipsToBounds = YES;
        imgLoader.hidden = YES;
        imgLoader.animationDuration = .5f;
        imgLoader.animationRepeatCount = 0;
        imgLoader.animationImages = [NSArray arrayWithObjects:[UIImage imageNamed:@"img_loading_1"],
                                                            [UIImage imageNamed:@"img_loading_2"],
                                                            [UIImage imageNamed:@"img_loading_3"],
                                                            [UIImage imageNamed:@"img_loading_4"],
                                                            [UIImage imageNamed:@"img_loading_5"],
                                                            [UIImage imageNamed:@"img_loading_6"],
                                                            [UIImage imageNamed:@"img_loading_7"],
                                                            [UIImage imageNamed:@"img_loading_8"], nil];
        [self addSubview:imgLoader];
         */
        [window.rootViewController.view addSubview:self];
        
        self.center = window.rootViewController.view.center;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(hideLoadingView)
                                                     name:kHideLoadingNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)showLoadingView
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [activityIndicatorView startAnimating];
    
//    imgLoader.hidden = NO;
//    [imgLoader startAnimating];
}

- (void)hideLoadingView {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [activityIndicatorView stopAnimating];
    
    //[imgLoader stopAnimating];
   
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kHideLoadingNotification
                                                  object:nil];
    [self removeFromSuperview];
}

#pragma mark - Static Method

+ (void)showLoadingView
{
    
    BOOL existLoadingView = NO;
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    for (UIView *view in [window rootViewController].view.subviews) {
        if([view isKindOfClass:[AKLoadingView class]]) {
            existLoadingView = YES;
        }
    }
    
    if (existLoadingView == NO) {
        dispatch_async(dispatch_get_main_queue(), ^{
            AKLoadingView *view = [[AKLoadingView alloc] initWithFrame:CGRectMake(0, 0, kLoadingViewSize, kLoadingViewSize)];
            [view showLoadingView];
        });
    }
}

+ (void)hideLoadingView {
    [[NSNotificationCenter defaultCenter] postNotificationName:kHideLoadingNotification object:nil];
}
@end
