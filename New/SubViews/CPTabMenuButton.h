//
//  CPTabMenuButton.h
//  11st
//
//  Created by Sanghong Han on 2015. 1. 20..
//  Copyright (c) 2015년 Commerce Planet. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CPTabMenuButtonDelegate <NSObject>

@required
- (void)touchTabMenuButton:(id)sender;

@end

@interface CPTabMenuButton : UIView

@property (nonatomic, assign) id<CPTabMenuButtonDelegate> delegate;

- (id)initWithFrameWithTitle:(CGRect)frame title:(NSDictionary *)aDict tag:(NSInteger)aTag;
- (void)setSelected:(BOOL)aBool;

@end
