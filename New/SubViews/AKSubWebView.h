//
//  AKSubWebView.h
//  AKMall
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

typedef NS_ENUM(NSInteger, AKSubWebViewType) {
    AKSubWebViewTypeNone = -1,
    AKSubWebViewTypeCategoryMenu = 2000,
    AKSubWebViewTypeHistoryMenu,
    AKSubWebViewTypeOutView,
    AKSubWebViewTypePopupView,
    AKSubWebViewTypeNormal
};

@protocol AKSubWebViewDelegate <NSObject>

@optional
- (void)hiddenTabbar:(BOOL)isHidden;
- (void)hiddenHeader:(BOOL)isHidden;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)gotoSubViewController:(NSString *)aUrl;
- (void)closePopupView:(NSString *)type;
- (void)openBrowser:(NSString *)aUrl;
- (void)moveTabIndex:(NSInteger)nIndex;
- (void)updateHeight:(CGFloat)nHeight;
- (void)closeAndOpenWebview:(NSString *)url;
- (void)updateTitle:(NSString *)sTitle;

@end

@interface AKSubWebView : UIView

@property (atomic, strong) WKWebView *webView;
@property (nonatomic, assign) AKSubWebViewType webViewType;
@property (nonatomic, assign) id<AKSubWebViewDelegate> delegate;

- (void)requestUrl:(NSString *)url;
- (BOOL)canGoBack;
- (void)goBack;
- (void)callJavaScript:(NSString *)aScript;
- (void)resizeWebView;


@end
