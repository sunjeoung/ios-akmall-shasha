//
//  AKSubHeaderVie.h
//  AKMall
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, AKSubHeaderViewType) {
    AKSubKHeaderViewTypeNone = -1,
    AKSubHeaderViewTypeCategory = 2000,
    AKSubHeaderViewTypeLogo,
    AKSubHeaderViewTypeCart,
    AKSubHeaderViewTypeSearchIcon
};

@protocol AKSubHeaderViewDelegate <NSObject>

- (void)didTouchSubHeaderView:(AKSubHeaderViewType)type;
@end

@interface AKSubHeaderView : UIView

@property (nonatomic, assign) id<AKSubHeaderViewDelegate> delegate;
@end
