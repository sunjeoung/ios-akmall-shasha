//
//  AKHeaderView.m
//  AKMall
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "AKHeaderView.h"
#import "GlobalHeader.h"

#define MAS_SHORTHAND
#define MAS_SHORTHAND_GLOBALS
#import "Masonry.h"

@interface AKHeaderView()
{
}

@property (nonatomic, strong) UIButton *btnCategory;
@property (nonatomic, strong) UIButton *btnCart;
@property (nonatomic, strong) UIButton *btnLogo;
@property (nonatomic, strong) UIButton *btnSearchText;
@property (nonatomic, strong) UIButton *btnSearch;
@property (nonatomic, strong) UILabel *cartCountLabel;
@property (nonatomic, strong) UIView *line;

@end

@implementation AKHeaderView

#pragma mark - Button Touch Event

- (void)didTouchHeaderButton:(UIButton *)btn
{
    AKHeaderViewType type = (AKHeaderViewType)btn.tag;
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTouchHeaderView:)]) {
        [self.delegate didTouchHeaderView:type];
    }
}

#pragma mark - NSNotification Method

- (void)showSearchText
{
    NSDictionary *dictSearch = [appDelegate dictSearchInfo];
    if (dictSearch) {
        [_btnSearchText setTitle:dictSearch[@"search_word"] forState:UIControlStateNormal];
    }
}

- (void)setNewAlarm
{
    _btnCategory.selected = [appDelegate isNewAlarm];
}

- (void)setCartCount
{
    _cartCountLabel.text = [NSString stringWithFormat:@"%d", [appDelegate nCartCount]];
    _btnCart.selected = ([appDelegate nCartCount] > 0);
}

#pragma mark - init

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) {
        return self;
    }
    
    _btnCategory = ({
        UIButton *btn = UIButton.new;
        //btn.backgroundColor = [UIColor redColor];
        btn.tag = AKHeaderViewTypeCategory;
        [btn setImage:[UIImage imageNamed:@"main_gnb_menu"] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"main_gnb_menu_on"] forState:UIControlStateSelected];
        [btn addTarget:self action:@selector(didTouchHeaderButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    _btnLogo = ({
        UIButton *btn = UIButton.new;
        //btn.backgroundColor = [UIColor yellowColor];
        btn.tag = AKHeaderViewTypeLogo;
        [btn setImage:[UIImage imageNamed:@"main_gnb_logo"] forState:UIControlStateNormal];
        //[btn setImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
        [btn addTarget:self action:@selector(didTouchHeaderButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    _btnCart = ({
        UIButton *btn = UIButton.new;
        //btn.backgroundColor = [UIColor blueColor];
        btn.tag = AKHeaderViewTypeCart;
        [btn setImage:[UIImage imageNamed:@"main_gnb_cart"] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"main_gnb_cart_on"] forState:UIControlStateSelected];
        [btn addTarget:self action:@selector(didTouchHeaderButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    _btnSearchText = ({
        UIButton *btn = UIButton.new;
        //btn.backgroundColor = [UIColor whiteColor];
        btn.tag = AKHeaderViewTypeSearchText;
        btn.titleLabel.font = [UIFont systemFontOfSize:15.f];
        btn.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 24)];
        [btn setTitleColor:UIColorFromRGB(0xA7A7A7) forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(didTouchHeaderButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    _line = ({
        UIView *view = UIView.new;
        view.backgroundColor = UIColorFromRGB(0x121616);
        [_btnSearchText addSubview:view];
        view;
    });
    
    _btnSearch = ({
        UIButton *btn = UIButton.new;
       // btn.backgroundColor = [UIColor cyanColor];
        btn.tag = AKHeaderViewTypeSearchIcon;
        [btn setImage:[UIImage imageNamed:@"main_gnb_search"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(didTouchHeaderButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    _cartCountLabel = ({
        UILabel *label = UILabel.new;
        label.backgroundColor = [UIColor clearColor];
        label.textColor = UIColorFromRGB(0xFFFFFF);
        label.font = [UIFont systemFontOfSize:9.f];
        label.textAlignment = NSTextAlignmentCenter;
        [_btnCart addSubview:label];
        label;
    });
    
    [self showSearchText];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showSearchText) name:kLoadMktMessageNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setNewAlarm) name:kNewAlarmNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setCartCount) name:kCartCountNotification object:nil];
    
    return self;
}

- (void)layoutSubviews
{
    [_btnCategory updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(17);
        make.top.equalTo(15);
        make.width.equalTo(26);
        make.height.equalTo(20);
    }];
    
    [_btnLogo updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.btnCategory.right).offset(7);
        make.top.equalTo(16);
        make.width.equalTo(70);
        make.height.equalTo(20);
    }];
    
    [_btnCart updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.right).offset(-10);
        make.top.equalTo(9);
        make.width.equalTo(35);
        make.height.equalTo(28);
    }];
    
    [_btnSearchText updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.btnCart.left).offset(-12);
        make.top.equalTo(9);
        make.left.equalTo(self.btnLogo.right).offset(15);
        make.height.equalTo(35);
    }];
    
    [_line updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.btnSearchText.left);
        make.right.equalTo(self.btnSearchText.right);
        make.bottom.equalTo(self.btnSearchText.bottom);
        make.height.equalTo(1);
    }];
    
    [_btnSearch updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.btnSearchText.right);
        make.top.equalTo(self.btnSearchText.top);
        make.bottom.equalTo(self.btnSearchText.bottom);
        make.width.equalTo(23);
    }];
    
    [_cartCountLabel updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.btnCart.right);
        make.top.equalTo(self.btnCart.top);
        make.height.equalTo(17);
        make.width.equalTo(17);
    }];
    
    _cartCountLabel.text = [NSString stringWithFormat:@"%d", [appDelegate nCartCount]];
    _btnCategory.selected = [appDelegate isNewAlarm];
    _btnCart.selected = ([appDelegate nCartCount] > 0);
}

@end
