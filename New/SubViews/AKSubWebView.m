//
//  AKSubWebView.m
//  AKMall
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "AKSubWebView.h"
#import <WebKit/WebKit.h>
#import "AKWebCommon.h"

#import "AKSchemeManager.h"
#import "AKCommonUtil.h"
#import "AKSettingViewController.h"
#import "AKNewMainViewController.h"
#import "SubViewController.h"
#import "AKProperty.h"
#import <objc/runtime.h>

#import <IgaworksCommerce/IgaworksCommerce.h>
#import "finger/finger.h"

@interface AKSubWebView() <WKNavigationDelegate, WKUIDelegate, AKSchemeManagerDelegate, UIScrollViewDelegate>
{
    AKSchemeManager *_schemeManager;
    BOOL isCompleted;
}

@property (nonatomic, strong) UIProgressView *progressView;
@end

@implementation AKSubWebView

#pragma mark - Public Method

- (void)requestUrl:(NSString *)url
{
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    //[self.webView reload]
}

- (void)resizeWebView
{
    self.webView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

#pragma mark - Private Function

- (void)closePopup:(NSString *)type {
    
    if ((self.webViewType == AKSubWebViewTypeHistoryMenu) || (self.webViewType == AKSubWebViewTypePopupView)) {
        if (_delegate && [_delegate respondsToSelector:@selector(closePopupView:)]) {
            [_delegate closePopupView:type];
        }
    }
    else {
        [UIView animateWithDuration:0.2 animations:^{
            CGRect frame = self.frame;
            frame.origin.x = -1*self.superview.frame.size.width;
            self.frame = frame;
        } completion:^(BOOL finished) {
            if ([type isEqualToString:@"setting"]) {
                AKSettingViewController *setting = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"AKSettingViewController"];
                [[AKCommonUtil getVisibleController].navigationController pushViewController:setting animated:YES];
            }
            [self removeFromSuperview];
        }];
    }
}

- (BOOL)isContainCategoryURL:(NSString *)url {
    if ([AKCommonUtil containsUrlString:url checkString:CATEGORY_BIG_KEY]    ||
        [AKCommonUtil containsUrlString:url checkString:CATEGORY_BRAND1_KEY] ||
        [AKCommonUtil containsUrlString:url checkString:CATEGORY_BRAND1_KEY] ||
        [AKCommonUtil containsUrlString:url checkString:CATEGORY_ITEM1_KEY]  ||
        [AKCommonUtil containsUrlString:url checkString:CATEGORY_ITEM2_KEY]) {
        return YES;
    }
    return NO;
}

- (NSString *)callJavaScriptReturnValue:(NSString *)script
{
    __block NSString *resultString = nil;
    __block BOOL finished = NO;

    [self.webView evaluateJavaScript:script completionHandler:^(id result, NSError *error) {
        if (error == nil) {
            if (result != nil) {
                resultString = [NSString stringWithFormat:@"%@", result];
            }
        }
        finished = YES;
    }];

    while (!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }

    return resultString;
}


#pragma mark - AKSchemeManager Delegate

- (BOOL)canGoBack
{
    return [self.webView canGoBack];
}

- (void)goBack
{
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }
    else {
        // 이전 화면
        [[AKCommonUtil getVisibleController].navigationController popViewControllerAnimated:YES];
    }
}

- (void)callJavaScript:(NSString *)script
{
    [_webView evaluateJavaScript:script completionHandler:nil];
}

- (void)openWebViewUrl:(NSString *)aUrl
{
    if (self.webViewType == AKSubWebViewTypeHistoryMenu) {
        [appDelegate closeHistoryView];
        if ([[AKCommonUtil getVisibleController] isKindOfClass:[AKNewMainViewController class]]) {
            [AKCommonUtil pushWebViewController:aUrl];
        }
        else {
            [AKCommonUtil pushWebViewController:aUrl];
        }
    }
    else {
        [AKCommonUtil pushWebViewController:aUrl];
    }
}

- (void)callWishPopup:(NSString *)goodsId {
    
    NSString *sWishUrl = [NSString stringWithFormat:@"%@&goods_id=%@", URL_LIKE, goodsId];
    
    [AKCommonUtil showPopupWebView:self
                               url:[AKCommonUtil getRequestUrl:sWishUrl]
     ];
}

- (void)openBrowser:(NSString *)aUrl;
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(openBrowser:)]) {
        [self.delegate openBrowser:aUrl];
    }
}

- (void)closeSubBrowser
{
    [self removeFromSuperview];
}

- (void)moveMainTabIndex:(NSString *)nIndex
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(moveTabIndex:)]) {
        [self.delegate moveTabIndex:[nIndex intValue]];
    }
}

- (void)toggleTabbar:(BOOL)isHidden
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(hiddenTabbar:)]) {
        [self.delegate hiddenTabbar:isHidden];
    }
}

- (void)toggleHeader:(BOOL)isHidden
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(hiddenHeader:)]) {
        [self.delegate hiddenHeader:isHidden];
    }
}

- (void)callJavaScriptFromWeb:(NSString *)aScript
{
    [_webView evaluateJavaScript:aScript completionHandler:nil];
}

- (void)showBack:(BOOL)isShow {
    //_back.hidden = !isShow;
}

- (void)updateHeight:(NSDictionary *)dic {
    if (self.delegate && [self.delegate respondsToSelector:@selector(updateHeight:)]) {
        [self.delegate updateHeight:[dic[@"h"] floatValue]];
    }
}

- (void)closeAndOpenWebview:(NSString *)url {
    if (self.delegate && [self.delegate respondsToSelector:@selector(closeAndOpenWebview:)]) {
        [self.delegate closeAndOpenWebview:url];
    }
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([self isKindOfClass:[SubViewController class]]) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(scrollViewDidScroll:)]) {
            [self.delegate scrollViewDidScroll:scrollView];
        }
    }
}

#pragma mark - WKWebView WKUIDelegate

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        @try {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"AKMall" message:message preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                completionHandler();
            }]];
            [[AKCommonUtil getVisibleController] presentViewController:alertController animated:YES completion:nil];
        }
        @catch (NSException *exception) {
        }
        @finally {
        }
    });
}

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler {
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        // TODO We have to think message to confirm "YES"
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"AKMall" message:message preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            completionHandler(YES);
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"취소" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            completionHandler(NO);
        }]];
        [alertController.view setNeedsLayout];
        
        [[AKCommonUtil getVisibleController] presentViewController:alertController animated:YES completion:nil];
    });
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString *))completionHandler {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:prompt message:webView.URL.host preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = defaultText;
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSString *input = ((UITextField *)alertController.textFields.firstObject).text;
        completionHandler(input);
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        completionHandler(nil);
    }]];
    
    [[AKCommonUtil getVisibleController] presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - WKWebView delegate

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    [[AKCommonUtil getVisibleController] startIndicator];
    
    BOOL isHiddenTabbar = NO;
    BOOL isHiddenHeader = NO;
    
    NSString *sUrl = webView.URL.absoluteString;
    
    NSArray *arrHiddenTabbar = [IUserDataManager getHiddenTabbarUrl];
    for (NSString *url in arrHiddenTabbar) {
        if ([sUrl rangeOfString:url].location != NSNotFound) {
            isHiddenTabbar = YES;
            break;
        }
    }
    
    NSLog(@"sUrl is %@", sUrl);
    
    if ([sUrl rangeOfString:@".akmall.com"].location != NSNotFound) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(hiddenTabbar:)]) {
            [self.delegate hiddenTabbar:isHiddenTabbar];
        }
    }
    
    NSArray *arrHiddenHeader = [IUserDataManager getHiddenHeaderUrl];
    for (NSString *url in arrHiddenHeader) {
        if ([sUrl rangeOfString:url].location != NSNotFound) {
            isHiddenHeader = YES;
            break;
        }
    }
    
    if ([sUrl rangeOfString:@".akmall.com"].location != NSNotFound) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(hiddenHeader:)]) {
            [self.delegate hiddenHeader:isHiddenHeader];
        }
    }
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    NSURL *url = navigationAction.request.URL;

    if (self.webViewType == AKSubWebViewTypeOutView) {
        if ([url.absoluteString hasPrefix:@"akmall://"]) {
            NSDictionary *scheme = [AKCommonUtil parseScheme:url.absoluteString];
            if ([scheme[@"command"] isEqualToString:@"getTitle"]) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(updateTitle:)]) {
                    [self.delegate updateTitle:scheme[@"param"][@"t"]];
                }
            }
            decisionHandler(WKNavigationActionPolicyCancel);
        }
        else if ([url.absoluteString rangeOfString:@"//close?#"].location != NSNotFound) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(closePopupView:)]) {
                [self.delegate closePopupView:@""];
            }
            decisionHandler(WKNavigationActionPolicyCancel);
        }
        else {
            decisionHandler(WKNavigationActionPolicyAllow);
        }
    }
    else if (self.webViewType == AKSubWebViewTypeCategoryMenu) {
        
        NSDictionary *scheme = [AKCommonUtil parseScheme:url.absoluteString];
        
        if ([url.scheme isEqualToString:@"akmall"]) {
            NSString *sHost = url.host;
            if ([sHost isEqualToString:@"goBack"]) {
                [appDelegate hideLeftMenu:YES];
            }
            else if ([sHost isEqualToString:@"setup"]) {
                [appDelegate hideLeftMenu:YES];
                
                AKSettingViewController *setting = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"AKSettingViewController"];
                setting.webView = webView;
                [[AKCommonUtil getVisibleController].navigationController pushViewController:setting animated:YES];
            }
            else if ([scheme[@"command"] isEqualToString:@"callToken"]) {
                NSString *sScript = [NSString stringWithFormat:@"callToken('%@')",[[finger sharedData] getDeviceIdx]];
                [self callJavaScript:sScript];
            }
            else if ([scheme[@"command"] isEqualToString:@"callMyFeed"]) {
                NSLog(@"json : %@  error : ",@"callMyFeed");
                [[finger sharedData] requestPushListPageWithBlock:1 Cnt:50 :^(NSDictionary *posts, NSError *error) {
                    if(!error) {
                        if(posts) {
                            int total= 0;
                            NSMutableArray *objList = [NSMutableArray new];
                            for (NSDictionary *dic in posts[@"pushList"]) {
                                NSDictionary * obj = @{@"msgTag":dic[@"msgTag"], @"mode":dic[@"mode"]};
                                [objList insertObject:obj atIndex:total];
                                total++;
                            }
                            NSString *json = [AKCommonUtil getJsonStringFromObject : objList];
                            NSString *encodeString = [AKCommonUtil urlEncode : json];
                            
                            NSString *url = [NSString stringWithFormat:@"%@/app/PushList.do?idxList=%@", BASE_URL, encodeString];
                            [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
                        }
                    }
                    
                }];
            }
            else if ([scheme[@"command"] isEqualToString:@"callMyFeedCnt"]) {
                [[finger sharedData] requestPushListPageWithBlock:1 Cnt:50 :^(NSDictionary *posts, NSError *error) {
                    NSLog(@"posts : %@  error : %@",posts,error);
                    NSString *todayStr = [AKCommonUtil getTodayString:@"yyyyMMdd"];
                    if(!error) {
                        if(posts) {
                            int total= 0;
                            int cnt = 0;
                            for (NSDictionary *dic in posts[@"pushList"]) {
                                
                                if([dic[@"opened"] isEqual:@"N"] && [[dic[@"date"] substringToIndex:8] isEqual:todayStr] ) {
                                    cnt++;
                                }
                                total++;
                            }
                            NSLog(@"callMyFeedCnt(%d,%d)",cnt,total);
                            NSString *sScript = [NSString stringWithFormat:@"callMyFeedCnt(%d,%d)",cnt,total];
                            [self callJavaScriptFromWeb:sScript];
                        }
                    }
                }];
            }
            decisionHandler(WKNavigationActionPolicyCancel);
        }
        else {
            if (isCompleted) {
                [appDelegate hideLeftMenu:NO];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(gotoSubViewController:)]) {
                    [self.delegate gotoSubViewController:url.absoluteString];
                }
                
                decisionHandler(WKNavigationActionPolicyCancel);
            }
            else decisionHandler(WKNavigationActionPolicyAllow);
        }
    }
    else if (self.webViewType == AKSubWebViewTypeHistoryMenu) {
        if ([url.scheme isEqualToString:@"akmall"]) {
            
            NSDictionary *scheme = [AKCommonUtil parseScheme:url.absoluteString];
            
            if ([scheme[@"command"] isEqualToString:@"closePopup"]) {
                [appDelegate closeHistoryView];
            }
            else if ([scheme[@"command"] isEqualToString:@"callWishPopup"]) {
                [self callWishPopup:scheme[@"param"][@"goods_id"]];
            }
            else if ([scheme[@"command"] isEqualToString:@"openWebview"]) {
                NSString *sUrl = scheme[@"param"][@"url"];
                if (sUrl && ([sUrl isEqualToString:@""] == NO) ) {
                    [appDelegate closeHistoryView];
                    [AKCommonUtil pushWebViewController:sUrl];
                }
            }
            else if ([scheme[@"command"] isEqualToString:@"callToken"]) {
                NSString *sScript = [NSString stringWithFormat:@"callToken('%@')",[[finger sharedData] getDeviceIdx]];
                [self callJavaScript:sScript];
            }
            else if ([scheme[@"command"] isEqualToString:@"goBack"]) {
                [appDelegate closeHistoryView];
            }
            decisionHandler(WKNavigationActionPolicyCancel);
        }
        else {
            if (isCompleted) {
                [appDelegate closeHistoryView];
                [AKCommonUtil pushWebViewController:url.absoluteString];
                decisionHandler(WKNavigationActionPolicyCancel);
            }
            else decisionHandler(WKNavigationActionPolicyAllow);
        }
    }
    else if (self.webViewType == AKSubWebViewTypePopupView) {
        BOOL isResult = [_schemeManager processScheme:webView url:url.absoluteString];
        if (isResult) {
            decisionHandler(WKNavigationActionPolicyAllow);
        }
        else decisionHandler(WKNavigationActionPolicyCancel);
    }
    else {
        BOOL isResult = [_schemeManager processScheme:webView url:url.absoluteString];
        if (isResult) {
            
            if ([AKCommonUtil containsUrlString:url.absoluteString checkString:@"/main/Main.do"]) {
                if ([[AKCommonUtil getVisibleController] isKindOfClass:[AKNewMainViewController class]]) {
                    decisionHandler(WKNavigationActionPolicyAllow);
                }
                else {
                    // 홈 클릭으로 메인 첫 화면으로 이동
                    [AKCommonUtil goHome:url.absoluteString];
                    decisionHandler(WKNavigationActionPolicyCancel);
                }
            }
            else if (![[AKCommonUtil getVisibleController] isKindOfClass:[SubViewController class]] &&
                     [AKCommonUtil containsUrlString:url.absoluteString checkString:@"/order/ShoppingCart.do"]) {
                // ShopingCart.do의 경우 dummy=I가 붙은 경우 pass
                if (![url.absoluteString containsString:@"dummy=I"]) {
                    [AKCommonUtil pushWebViewController:url.absoluteString];
                    decisionHandler(WKNavigationActionPolicyCancel);
                }
                else {
                    decisionHandler(WKNavigationActionPolicyAllow);
                }
            }
            else if ([AKCommonUtil containsUrlString:url.absoluteString checkString:URL_LOGIN]) {
                if ([url.absoluteString containsString:@"token"]) {
                    if (![[AKCommonUtil getVisibleController] isKindOfClass:[SubViewController class]]) {
                        [AKCommonUtil pushWebViewController:url.absoluteString];
                        
                        decisionHandler(WKNavigationActionPolicyCancel);
                    }
                    else {
                        decisionHandler(WKNavigationActionPolicyAllow);
                    }
                }
                else {
                    NSString *aUrl = @"";
                    if ([url.absoluteString containsString:@"?"]) {
                        aUrl = [NSString stringWithFormat:@"%@&token=%@", url.absoluteString, [AKProperty getDeviceToken]];
                    }
                    else {
                        aUrl = [NSString stringWithFormat:@"%@?token=%@", url.absoluteString, [AKProperty getDeviceToken]];
                    }
                    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:aUrl]]];
                    decisionHandler(WKNavigationActionPolicyCancel);
                }
            }
            else if ([[AKCommonUtil getVisibleController] isKindOfClass:[AKNewMainViewController class]]) {
                // 메인 화면에서 통과해야 할 url 정의
                // 카카오 관련 공통 웹뷰로 이동하여 실행 시 오류 발생하여 메인웹뷰에서 처리하도록 수정
                // 앱스토어 이동 시 다시 앱으로 돌아왔을 때 흰 화면이 되어서 메인웹뷰에서 처리하도록 수정
                NSArray *passUrl = @[@"about:blank", @"accounts.google.com", @"content.googleapis.com", @"kakaolink://", @"storylink://", @"itunes.apple.com", @"youtube.com"];
                
                if (![AKCommonUtil containsStringList:passUrl inString:url.absoluteString]) {
                    if (self.webViewType == AKSubWebViewTypeHistoryMenu) {
                        decisionHandler(WKNavigationActionPolicyAllow);
                    } else {
                        // 메인 화면에서는 무조건 새로운 웹화면으로 이동해서 시작
                        [AKCommonUtil pushWebViewController:url.absoluteString];
                        decisionHandler(WKNavigationActionPolicyCancel);
                    }
                }
                else if ([url.absoluteString rangeOfString:@"youtube.com"].location != NSNotFound) {
                    decisionHandler(WKNavigationActionPolicyAllow);
                }
                else {
                    decisionHandler(WKNavigationActionPolicyCancel);
                }
            }
            else {
                decisionHandler(WKNavigationActionPolicyAllow);
            }
        }
        else {
            decisionHandler(WKNavigationActionPolicyCancel);
        }
    }
    
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{
//    NSHTTPURLResponse *response = (NSHTTPURLResponse *)navigationResponse.response;
//    NSArray *cookies =[NSHTTPCookie cookiesWithResponseHeaderFields:[response allHeaderFields] forURL:response.URL];
//
//    for (NSHTTPCookie *cookie in cookies) {
//        NSLog(@"cookie is %@", cookie);
//        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
//    }
    
    if (@available(iOS 11.0, *)) {  //available on iOS 11+
        WKHTTPCookieStore *cookieStore = webView.configuration.websiteDataStore.httpCookieStore;
            [cookieStore getAllCookies:^(NSArray* cookies) {
                if (cookies.count > 0) {
                    for (NSHTTPCookie *cookie in cookies) {
                        NSLog(@"cookie is %@", cookie);
                       [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
                    }
                }
            }];
    }
    
    decisionHandler(WKNavigationResponsePolicyAllow);

    //NSString *sUrl = navigationResponse.response.URL.absoluteString;
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [[AKCommonUtil getVisibleController] stopIndicator];
    
    if (self.webViewType == AKSubWebViewTypeOutView) {
        [AKCommonUtil getTitle:webView];
    }
    
    ///order/MobileOrderProc.do
    if ([webView.URL.absoluteString rangeOfString:@"/order/OrderFinish.do"].location != NSNotFound) {
        NSString *purchaseJsonString = nil;
        
        purchaseJsonString = [self callJavaScriptReturnValue:@"purchaseJson"];
        
        if (purchaseJsonString != nil) {
            NSString *lastPurchaseJson = [IUserDataManager getLastPurchaseJson];
            // 중복호출 방지
            if ([purchaseJsonString isEqualToString:lastPurchaseJson]) {
                return ;
            }
            if (purchaseJsonString && ![purchaseJsonString isEqualToString:@""]) {
                // IGAW연동
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"purchaseJsonString is %@", purchaseJsonString);
                    [IUserDataManager setLastPurchaseJson:purchaseJsonString];
                    [IgaworksCommerce purchase:purchaseJsonString];
                });
            }
            else {
                lastPurchaseJson = @"";
            }
        }
    }
    
    isCompleted = YES;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if ([keyPath isEqualToString:@"estimatedProgress"] && object == _webView) {
        [self.progressView setAlpha:1.0f];
        [self.progressView setProgress:((WKWebView *)_webView).estimatedProgress animated:YES];
        
        if(((WKWebView *)_webView).estimatedProgress >= 1.0f) {
            [UIView animateWithDuration:0.3 delay:0.3 options:UIViewAnimationOptionCurveEaseOut animations:^{
                [self.progressView setAlpha:0.0f];
            } completion:^(BOOL finished) {
                [self.progressView setProgress:0.0f animated:NO];
            }];
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - init

- (void)dealloc {
    self.webView.scrollView.delegate = nil;
    self.webView.UIDelegate = nil;
    self.webView.navigationDelegate = nil;
    self.delegate = nil;
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
}

- (void)initWebView
{
    WKPreferences *thisPref = [[WKPreferences alloc] init];
    thisPref.javaScriptCanOpenWindowsAutomatically = YES;
    thisPref.javaScriptEnabled = YES;
    
    WKWebViewConfiguration* configuration = WKWebViewConfiguration.new;
    configuration.processPool = [WKWebViewPoolHandler pool];
    configuration.allowsInlineMediaPlayback = YES;
    configuration.mediaTypesRequiringUserActionForPlayback = NO;
    configuration.allowsPictureInPictureMediaPlayback = YES;
    configuration.mediaTypesRequiringUserActionForPlayback = WKAudiovisualMediaTypeNone;
    configuration.preferences = thisPref;
    configuration.applicationNameForUserAgent = [NSString stringWithFormat:@" NEW-AKMALL;appv=%@;appid=%@;akpush", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], [AKCommonUtil getUUID]];
    
    _webView = [[WKWebView alloc] initWithFrame:self.bounds configuration:configuration];
    _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _webView.UIDelegate = self;
    _webView.navigationDelegate = self;
    _webView.multipleTouchEnabled = NO;
    _webView.scrollView.delegate = self;
    _webView.allowsBackForwardNavigationGestures = YES;
    [self addSubview:_webView];
    
    self.progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    self.progressView.trackTintColor = [UIColor whiteColor];
    self.progressView.tintColor = UIColorFromRGB(0xe70060);
    self.progressView.frame = CGRectMake(0, 0, self.frame.size.width, 10);
    [self addSubview:self.progressView];
    
    [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:NULL];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _schemeManager = [[AKSchemeManager alloc] init];
        _schemeManager.delegate = self;
        [self initWebView];
    }
    
    return self;
}

- (void)allowDisplayingKeyboardWithoutUserAction {
    Class class = NSClassFromString(@"WKContentView");
    NSOperatingSystemVersion iOS_11_3_0 = (NSOperatingSystemVersion){11, 3, 0};
    
    if ([[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion: iOS_11_3_0]) {
        SEL selector = sel_getUid("_startAssistingNode:userIsInteracting:blurPreviousNode:changingActivityState:userObject:");
        Method method = class_getInstanceMethod(class, selector);
        IMP original = method_getImplementation(method);
        IMP override = imp_implementationWithBlock(^void(id me, void* arg0, BOOL arg1, BOOL arg2, BOOL arg3, id arg4) {
            ((void (*)(id, SEL, void*, BOOL, BOOL, BOOL, id))original)(me, selector, arg0, TRUE, arg2, arg3, arg4);
        });
        method_setImplementation(method, override);
    } else {
        SEL selector = sel_getUid("_startAssistingNode:userIsInteracting:blurPreviousNode:userObject:");
        Method method = class_getInstanceMethod(class, selector);
        IMP original = method_getImplementation(method);
        IMP override = imp_implementationWithBlock(^void(id me, void* arg0, BOOL arg1, BOOL arg2, id arg3) {
            ((void (*)(id, SEL, void*, BOOL, BOOL, id))original)(me, selector, arg0, TRUE, arg2, arg3);
        });
        method_setImplementation(method, override);
    }
}

@end
