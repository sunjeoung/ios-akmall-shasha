//
//  AKWebView.m
//  AKMall
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "AKWebView.h"
#import "AKWebCommon.h"
#import "AKCommonUtil.h"
#import "AKSchemeManager.h"
#import "AKProperty.h"
#import "AKLoadingView.h"

#import <AdBrix/AdBrix.h>
#import <IgaworksCommerce/IgaworksCommerce.h>

@interface AKWebView() <WKNavigationDelegate, WKUIDelegate, UIScrollViewDelegate, AKSchemeManagerDelegate>
{
    AKSchemeManager *_schemeManager;
}

@property (assign) BOOL dontShowPopup;
@property (nonatomic, strong) NSString *currentUrl;

@end

@implementation AKWebView

/**
*  서버로 부터 전송된 URI 중에 호스트가 포함되어 있는지 확인 후
* 호스트 주소가 포함되어 있지 않다면 추가하여 웹을 로드하도록 한다.
* - Parameters:
*   - url : 연결할 웹뷰 주소
* - Returns:  void
*/
- (void)requestUrl:(NSString *)url
{
    [AKLoadingView showLoadingView];
    NSString *address = @"akmall.com";
    self.currentUrl = url;
    if([url rangeOfString: address].location == NSNotFound) {
        NSString *loadUrl = [AKCommonUtil getRequestUrl:url];
        self.currentUrl = loadUrl;
    }
    
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString: self.currentUrl]]];
}

- (void)reloadWebView
{
    [_webView.scrollView setContentOffset:CGPointMake(0, 0)];
    if (self.currentUrl && ([self.currentUrl isEqualToString:@""] == NO)) {
        [self requestUrl:self.currentUrl];
    }
}

- (void)callGATrackerJavaScript:(NSString *)script
{
    [_webView evaluateJavaScript:script completionHandler:nil];
}

#pragma mark - AKSchemeManager Delegate

- (void)canGoBack
{
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }
    else {
        // 이전 화면
        [[AKCommonUtil getVisibleController].navigationController popViewControllerAnimated:YES];
    }
}

- (void)callJavaScript:(NSString *)script
{
    NSLog(@"script is %@", script);
    [_webView evaluateJavaScript:script completionHandler:nil];
}

#pragma mark - AKSchemeManagerDelegate

- (void)sendMainPopup:(NSArray *)list {
    for (NSDictionary *dic in list) {
        // native popup
        if ([dic[@"popup_div_code"] intValue] == 110) {
            NSString *todayStr = [AKCommonUtil getTodayString:@"yyyyMMdd"];
            if (!_dontShowPopup && ![[AKProperty getPopupTodayDate:dic[@"mobile_popup_id"]] isEqualToString:todayStr]) {
                [AKCommonUtil showFullPopup:self data:dic];
                _dontShowPopup = YES;
            }
        }
    }
}

- (void)callWishPopup:(NSString *)goodsId {
    
    NSString *sWishUrl = [NSString stringWithFormat:@"%@&goods_id=%@", URL_LIKE, goodsId];
    [AKCommonUtil showPopupWebView:self url:[AKCommonUtil getRequestUrl:sWishUrl]];
}

- (void)callJavaScriptFromWeb:(NSString *)aScript
{
    NSLog(@"aScript is %@", aScript);
    [_webView evaluateJavaScript:aScript completionHandler:nil];
}

- (void)moveMainTabIndex:(NSString *)nIndex
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(moveTabIndex:)]) {
        [self.delegate moveTabIndex:[nIndex intValue]];
    }
}

#pragma mark - WKWebView WKUIDelegate

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        @try {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:webView.URL.host message:message preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                completionHandler();
            }]];
            [[AKCommonUtil getVisibleController] presentViewController:alertController animated:YES completion:nil];
        }
        @catch (NSException *exception) {
        }
        @finally {
        }
    });
}

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler {
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        // TODO We have to think message to confirm "YES"
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:webView.URL.host message:message preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            completionHandler(YES);
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"취소" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            completionHandler(NO);
        }]];
        [alertController.view setNeedsLayout];
        
        [[AKCommonUtil getVisibleController] presentViewController:alertController animated:YES completion:nil];
    });
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString *))completionHandler {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:prompt message:webView.URL.host preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = defaultText;
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSString *input = ((UITextField *)alertController.textFields.firstObject).text;
        completionHandler(input);
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        completionHandler(nil);
    }]];
    
    [[AKCommonUtil getVisibleController] presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - WKWebView Delegate

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    if(error) {
        NSLog(@"error is %@", error);
    }
    
    if ([AKCommonUtil checkWebViewError:error]) {
        [AKCommonUtil showAlert:nil
                            msg:@"네트워크에 접속할 수 없습니다. 네트워크 연결상태를 확인해 주세요."
                    cancelTitle:@"앱종료"
                    cancelBlock:^(UIAlertAction * _Nullable action) {
                        exit(0);
                    }
                   confirmTitle:@"재시도" confirmBlock:^(UIAlertAction * _Nullable action) {
                       [webView reload];
                   }
         ];
    }
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    NSString *url = navigationAction.request.URL.absoluteString;
    
    BOOL isResult = [_schemeManager processScheme:webView url: url categoryType:CATEGORY_MAIN];
    
    if (isResult) {
        NSArray *passUrl = @[@"about:blank", @"accounts.google.com", @"content.googleapis.com", @"kakaolink://", @"storylink://", @"itunes.apple.com", @"youtube.com"];
        
        if ([url rangeOfString:@"/main/Main.do"].location != NSNotFound) {
            decisionHandler(WKNavigationActionPolicyAllow);
        }
        else if (![AKCommonUtil containsStringList:passUrl inString:url]) {
            // 메인 화면에서는 무조건 새로운 웹화면으로 이동해서 시작
            [AKCommonUtil pushWebViewController:url];
            decisionHandler(WKNavigationActionPolicyCancel);
        }
        else if ([url rangeOfString:@"youtube.com"].location != NSNotFound) {
            decisionHandler(WKNavigationActionPolicyAllow);
        }
        else decisionHandler(WKNavigationActionPolicyAllow);
    }
    else {
        decisionHandler(WKNavigationActionPolicyCancel);
    }
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{
    NSHTTPURLResponse *response = (NSHTTPURLResponse *)navigationResponse.response;
    if ([response.URL.host rangeOfString:@".akmall.com"].location != NSNotFound) {
        NSArray *cookies =[NSHTTPCookie cookiesWithResponseHeaderFields:[response allHeaderFields] forURL:response.URL];
        
        for (NSHTTPCookie *cookie in cookies) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
        }
    }
    
    decisionHandler(WKNavigationResponsePolicyAllow);
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [AKLoadingView hideLoadingView];
    
    @try{
        if ([webView.URL.absoluteString rangeOfString:@"/main/Main.do"].location != NSNotFound) {
            NSLog(@"commerceViewHome inapp");
            [AdBrix commerceViewHome];
        }
    }@catch (NSException *exception) {
        NSLog(@"commerceViewHome error");
    }
    @finally {
    }
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(scrollViewDidScroll:)]) {
        [self.delegate scrollViewDidScroll:scrollView];
    }
}

#pragma mark - init

- (void)initWebView
{
    WKPreferences *thisPref = [[WKPreferences alloc] init];
    thisPref.javaScriptCanOpenWindowsAutomatically = YES;
    thisPref.javaScriptEnabled = YES;
    
    WKWebViewConfiguration* configuration = WKWebViewConfiguration.new;
    configuration.processPool = [WKWebViewPoolHandler pool];
    configuration.allowsInlineMediaPlayback = YES;
    configuration.mediaTypesRequiringUserActionForPlayback = NO;
    configuration.allowsPictureInPictureMediaPlayback = YES;
    configuration.mediaTypesRequiringUserActionForPlayback = WKAudiovisualMediaTypeNone;
    configuration.preferences = thisPref;
    configuration.applicationNameForUserAgent = [NSString stringWithFormat:@" NEW-AKMALL;appv=%@;appid=%@;akpush", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], [AKCommonUtil getUUID]];
    
    _webView = [[WKWebView alloc] initWithFrame:self.bounds configuration:configuration];
    _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _webView.UIDelegate = self;
    _webView.navigationDelegate = self;
    _webView.multipleTouchEnabled = NO;
    _webView.scrollView.delegate = self;
    _webView.allowsBackForwardNavigationGestures = YES;
    [self addSubview:_webView];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _schemeManager = [[AKSchemeManager alloc] init];
        _schemeManager.delegate = self;
        [self initWebView];
    }
    
    return self;
}

@end
