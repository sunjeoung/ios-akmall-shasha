//
//  CPTabMenuButton.m
//  11st
//
//  Created by Sanghong Han on 2015. 1. 20..
//  Copyright (c) 2015년 Commerce Planet. All rights reserved.
//

#import "CPTabMenuButton.h"

#import "NSDictionary+TabDatas.h"

#define nFontSize  15

@interface CPTabMenuButton()
{
    UILabel *titleLabel;
    UIView  *bottomLine;
}

@end

@implementation CPTabMenuButton

- (id)initWithFrameWithTitle:(CGRect)frame title:(NSDictionary *)aDict tag:(NSInteger)aTag {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.userInteractionEnabled = YES;
        
        self.backgroundColor = [UIColor clearColor];
        CGFloat nImgWidth = [aDict isNew] ? 12 : 0;
        
        titleLabel = [[UILabel alloc] init];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.text = [aDict tabName];
        titleLabel.textColor = UIColorFromRGB(0x191919);
        titleLabel.font = [UIFont boldSystemFontOfSize:nFontSize];
        [self addSubview:titleLabel];
        
        CGSize curSize = [titleLabel.text sizeWithAttributes:@{NSFontAttributeName:titleLabel.font}];
        CGFloat xPos = (self.frame.size.width - (curSize.width+nImgWidth)) / 2;
        titleLabel.frame = CGRectMake(xPos, 0, curSize.width+nImgWidth, self.frame.size.height);
        
        if ([aDict isNew]) {
            UIImageView *imgNew = [[UIImageView alloc] initWithFrame:CGRectMake(xPos+curSize.width+2, 13, 10, 10)];
            imgNew.image = [UIImage imageNamed:@"img_new"];
            [self addSubview:imgNew];
        }
        
        if ([aDict isHasFlagName]) {
            
            CGSize hotLabelSize = [[aDict description] sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:9.f]}];
            
            CGFloat nHotLabelWidth = frame.size.width;
            if ([aDict isNew]) {
                nHotLabelWidth = nHotLabelWidth - 12;
            }
            
            CGFloat yPos = ((frame.size.height - curSize.height)/2) - hotLabelSize.height;
            
            UILabel *lblHotLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, yPos, nHotLabelWidth, hotLabelSize.height)];
            lblHotLabel.font = [UIFont systemFontOfSize:9.f];
            lblHotLabel.textColor = UIColorFromRGB(0x4652a0);
            lblHotLabel.textAlignment = NSTextAlignmentCenter;
            lblHotLabel.text = [aDict description];
            [self addSubview:lblHotLabel];
        }
        
        bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height-3, frame.size.width, 3)];
        [bottomLine setBackgroundColor:UIColorFromRGB(0xE20265)];
        [bottomLine setHidden:YES];
        [self addSubview:bottomLine];
        
        self.tag = aTag;
    }
    
    return self;
}

- (void)setSelected:(BOOL)aBool {
    if (aBool) {
        [bottomLine setHidden:NO];
        [titleLabel setTextColor:UIColorFromRGB(0xE20265)];
        titleLabel.font = [UIFont boldSystemFontOfSize:(nFontSize)];
    }
    else {
        [bottomLine setHidden:YES];
        [titleLabel setTextColor:UIColorFromRGB(0x191919)];
        titleLabel.font = [UIFont boldSystemFontOfSize:(nFontSize)];
    }
}

#pragma mark - touch event

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [bottomLine setHidden:YES];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [bottomLine setHidden:YES];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    self.backgroundColor = [UIColor clearColor];
    if (self.delegate) {
        [self.delegate touchTabMenuButton:self];
    }
}

@end
