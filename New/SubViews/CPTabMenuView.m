//
//  CPTabMenuView.m
//  11st
//
//  Created by spearhead on 2014. 8. 28..
//  Copyright (c) 2014년 Commerce Planet. All rights reserved.
//

#import "CPTabMenuView.h"
#import "CPTabMenuButton.h"

#import "NSDictionary+TabDatas.h"

#define kGradationWidth     100

@interface CPTabMenuView() <CPTabMenuButtonDelegate>
{
    NSArray *menuTitles;
    NSArray *menuContents;
    NSInteger currentItemIndex;
    
    UIScrollView    *tabScrollView;
}
@end

@implementation CPTabMenuView

@synthesize arrMenus;

#pragma mark - Private

- (void)moveTabMenu:(CGRect)frame
{
    CGFloat nButtonCenter = frame.origin.x + frame.size.width/2;
    CGFloat nCenterPos = self.frame.size.width/2;
    CGFloat nMaxMoveWidth = self.contentSize.width - self.frame.size.width;
    CGFloat nMovePos = 10;
    
    if (nButtonCenter > nCenterPos) {
        nMovePos = nButtonCenter - nCenterPos;
        if ((nMovePos >= self.contentSize.width/2)) {
            nMovePos = self.contentSize.width - self.frame.size.width;
        }
        if (nMovePos > nMaxMoveWidth) {
            [self setContentOffset:CGPointMake(nMaxMoveWidth, 0) animated:YES];
        }
        else {
            [self setContentOffset:CGPointMake(nMovePos, 0) animated:YES];
        }
    }
    else {
        [self setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}

#pragma mark - CPTabMenuButtonDelegate

- (void)touchTabMenuButton:(id)sender {
    for (UIView *subView in [self subviews]) {
        if ([subView isKindOfClass:[CPTabMenuButton class]]) {
            CPTabMenuButton *button = (CPTabMenuButton *)subView;
            
            if (button.tag == ((CPTabMenuButton *)sender).tag) {
                [button setSelected:YES];
                
                [self moveTabMenu:button.frame];
                
                if ([self.tabdelegate respondsToSelector:@selector(didTouchTabMenuButton:)]) {
                    [self.tabdelegate didTouchTabMenuButton:button.tag-100];
                }
            }
            else {
                [button setSelected:NO];
            }
        }
    }
}

- (void)loadTabMenu
{
    for (UIView *v in [self subviews]) {
        if ([v isKindOfClass:[CPTabMenuButton class]]) {
            [v removeFromSuperview];
        }
    }
    
    CGFloat xGap = 15;
    CGFloat nMenuGap = 5;
    CGFloat nTabSpace = 10;
    CGFloat xPos = xGap;
    CGFloat nFontSize = 15;
    
    for (int i=0; i < [self.arrMenus count]; i++) {
        
        NSDictionary *dict = [self.arrMenus objectAtIndex:i];
        
        NSString *sTitle = [dict tabName];
        
        CGSize nSize = [sTitle sizeWithAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:nFontSize]}];
        CGFloat nWidth = nSize.width + nTabSpace;
        if ([dict isNew]) {
            nWidth = nWidth + 12;
        }
        
        CGRect menuButtonFrame = CGRectMake(xPos, 0, nWidth, kTabMenuHeight);
        
        CPTabMenuButton *menuButton = [[CPTabMenuButton alloc] initWithFrameWithTitle:menuButtonFrame title:dict tag:i+100];
        menuButton.delegate = self;
        (i == 0) ? [menuButton setSelected:YES] : [menuButton setSelected:NO];
        [self addSubview:menuButton];
        
        xPos = xPos + nSize.width+nTabSpace + nMenuGap;
    }

    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, kTabMenuHeight-1, xPos-nMenuGap+xGap, 1)];
    line.backgroundColor = UIColorFromRGB(0xE0E0E0);
    [self addSubview:line];
    
    [self setContentSize:CGSizeMake(xPos-nMenuGap+xGap, self.frame.size.height)];

}

#pragma mark -

- (id)initWithFrame:(CGRect)frame menuTitleItems:(NSArray *)menuTitleItems menuContentsItems:(NSArray *)menuContentsItems {
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        // backgroundColor
        [self setBackgroundColor:UIColorFromRGB(0xFFFFFF)];
        [self setShowsHorizontalScrollIndicator:NO];
    }
    
    return self;
}

#pragma mark - Public Methods

- (void)tabMenuCurrentItemIndexDidChange:(NSInteger)index
{
    currentItemIndex = index;
    
    for (UIView *subView in [self subviews]) {
        if ([subView isKindOfClass:[CPTabMenuButton class]]) {
            CPTabMenuButton *button = (CPTabMenuButton *)subView;
            
            if (button.tag == currentItemIndex+100) {
                [button setSelected:YES];
                
                [self moveTabMenu:button.frame];
            }
            else {
                [button setSelected:NO];
            }
        }
    }
}

@end
