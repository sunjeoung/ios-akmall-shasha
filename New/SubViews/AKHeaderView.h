//
//  AKHeaderView.h
//  AKMall
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, AKHeaderViewType) {
    AKHeaderViewTypeNone = -1,
    AKHeaderViewTypeCategory = 1000,
    AKHeaderViewTypeLogo,
    AKHeaderViewTypeCart,
    AKHeaderViewTypeSearchIcon,
    AKHeaderViewTypeSearchText
};

@protocol AKHeaderViewDelegate <NSObject>

- (void)didTouchHeaderView:(AKHeaderViewType)type;
@end

@interface AKHeaderView : UIView

@property (nonatomic, assign) id<AKHeaderViewDelegate> delegate;
@end
