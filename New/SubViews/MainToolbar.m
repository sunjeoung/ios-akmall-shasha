//
//  MainToolbar.m
//  AKMall
//
//  Created by seoyeon on 2018. 3. 27..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "MainToolbar.h"
#import "UIImageView+WebCache.h"

#define kImageWidth     18

#define MAS_SHORTHAND
#define MAS_SHORTHAND_GLOBALS
#import "Masonry.h"

@interface MainToolbar()
{
    
}

@property (nonatomic, strong) UIButton *btnLike;
@property (nonatomic, strong) UIButton *btnOrder;
@property (nonatomic, strong) UIButton *btnHome;
@property (nonatomic, strong) UIButton *btnMyAK;
@property (nonatomic, strong) UIButton *btnHistory;
@property (nonatomic, strong) UIView   *line;
@property (nonatomic, strong) UIImageView *imgHistory;

@end

@implementation MainToolbar

+ (CGFloat)height {
    return kMainToolbarHeight + [CommonFunc safeBottomY];
}

#pragma mark - Touch Event

- (void)didTouchMainToolbar:(UIButton *)btn
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTouchMainToolbar:)]) {
        [self.delegate didTouchMainToolbar:btn.tag];
    }
}

#pragma mark - NSNotification Function

- (void)setHistoryImage:(NSNotification *)noti
{
    NSString *sImgUrl = [noti object];
    _imgHistory.layer.cornerRadius = kImageWidth/2;
    _imgHistory.clipsToBounds = YES;
    [_imgHistory sd_setImageWithURL:[NSURL URLWithString:sImgUrl]];
}

#pragma mark - init

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) {
        return self;
    }
    
    _btnLike = ({
        UIButton *btn = UIButton.new;
        btn.tag = MainToolbarTypeLikeIt;
        btn.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [btn setImage:[UIImage imageNamed:@"img_likeit"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(didTouchMainToolbar:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    _btnOrder = ({
        UIButton *btn = UIButton.new;
        btn.tag = MainToolbarTypeOrder;
        btn.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [btn setImage:[UIImage imageNamed:@"img_order"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(didTouchMainToolbar:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    _btnHome = ({
        UIButton *btn = UIButton.new;
        btn.tag = MainToolbarTypeHome;
        btn.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [btn setImage:[UIImage imageNamed:@"img_home"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(didTouchMainToolbar:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    _btnMyAK = ({
        UIButton *btn = UIButton.new;
        btn.tag = MainToolbarTypeMyAK;
        btn.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [btn setImage:[UIImage imageNamed:@"img_myak"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(didTouchMainToolbar:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    _btnHistory = ({
        UIButton *btn = UIButton.new;
        btn.tag = MainToolbarTypeHistory;
        btn.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [btn setImage:[UIImage imageNamed:@"img_history"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(didTouchMainToolbar:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });

    _imgHistory = ({
        UIImageView *img = UIImageView.new;
        [_btnHistory addSubview:img];
        img;
    });
    
    _line = ({
        UIView *view = UIView.new;
        view.backgroundColor = UIColorFromRGB(0xc4c4c4);
        [self addSubview:view];
        view;
    });

    self.backgroundColor = UIColorFromRGB(0xF8F8F8);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setHistoryImage:) name:kDispplayHistoryImageNotification object:nil];
    
    return self;
}

- (void)layoutSubviews
{
    CGFloat nButtonWidth = kScreenBoundsWidth / 5.f;
 
    [_btnLike updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.top.equalTo(0);
        make.width.equalTo(nButtonWidth);
        make.height.equalTo(kMainToolbarHeight);
    }];
    
    [_btnOrder updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.btnLike.right);
        make.top.equalTo(0);
        make.width.equalTo(nButtonWidth);
        make.height.equalTo(kMainToolbarHeight);
    }];
    
    [_btnHome updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.btnOrder.right);
        make.top.equalTo(0);
        make.width.equalTo(nButtonWidth);
        make.height.equalTo(kMainToolbarHeight);
    }];
    
    [_btnMyAK updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.btnHome.right);
        make.top.equalTo(0);
        make.width.equalTo(nButtonWidth);
        make.height.equalTo(kMainToolbarHeight);
    }];
    
    [_btnHistory updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.btnMyAK.right);
        make.top.equalTo(0);
        make.width.equalTo(nButtonWidth);
        make.height.equalTo(kMainToolbarHeight);
    }];
    
    [_imgHistory updateConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.btnHistory);
        make.top.equalTo(self.top).offset(12);
        make.width.equalTo(kImageWidth);
        make.height.equalTo(kImageWidth);
    }];

    [_line updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.width.equalTo(kScreenBoundsWidth);
        make.height.equalTo(1);
    }];
    
    if ([[appDelegate imgHistoryUrl] isEqualToString:@""] == NO) {
        _imgHistory.layer.cornerRadius = kImageWidth/2;
        _imgHistory.clipsToBounds = YES;
        [_imgHistory sd_setImageWithURL:[NSURL URLWithString:[appDelegate imgHistoryUrl]]];
    }
}

@end
