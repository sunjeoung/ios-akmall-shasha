//
//  AKWebCommon.h
//  AKMall
//
//  Created by Sanghong Han on 2020/08/18.
//  Copyright © 2020 sjsofttech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WKProcessPool.h>

NS_ASSUME_NONNULL_BEGIN

@interface WKWebViewPoolHandler : NSObject
{
}

+ (WKProcessPool *)pool;
@end

@interface AKWebCommon : NSObject

@end

NS_ASSUME_NONNULL_END
