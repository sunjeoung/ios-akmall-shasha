//
//  FLWebViewProvider.h
//  FLWebView
//
//  Created by Steve Richey on 11/21/14.
//  Copyright (c) 2014 Float Mobile Learning. Shared under an MIT license. See license.md for details.
//

#import <Foundation/Foundation.h>

/*
 * This class defines methods that FLUIWebView and FLWKWebView should implement in
 * order to work within our ViewController.
*/
@protocol FLWebViewProvider <NSObject>

/*
 * Return the active NSURLRequest of this webview.
 * The methodology is a bit different between UIWebView and WKWebView.
 * Defining it here one way helps to ensure we'll implement it in the same way in our categories.
*/
@property (nonatomic, strong) NSURLRequest *request;

/*
 * Returns the active NSURL. Again, this is a bit different between the two web views.
*/
@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, assign) BOOL allowsBackForwardNavigationGestures;
@property (nonatomic, assign) CGFloat estimatedProgress;

/*
 * Assign a delegate view for this webview.
*/
- (void) setDelegateViews: (id) delegateView;

/*
 * Load an NSURLRequest in the active webview.
*/
- (void) loadRequest: (NSURLRequest *) request;

/*
 * Convenience method to load a request from a string.
*/
- (void) loadRequestFromString: (NSString *) urlNameAsString;

/*
 * Returns true if it is possible to go back, false otherwise.
*/
- (BOOL) canGoBack;
- (void) goBack;
- (NSString*)getPageTitle;
/*
 * UIWebView has stringByEvaluatingJavaScriptFromString, which is synchronous.
 * WKWebView has evaluateJavaScript, which is asynchronous.
 * Since it's far easier to implement the latter in UIWebView, we define it here and do that.
*/
- (void)evaluateJavaScript: (NSString *) javaScriptString completionHandler: (void (^)(id, NSError *)) completionHandler;
- (NSString *)stringByEvaluatingJavaScriptFromString2:(NSString *)aScript;
- (void)stringByEvaluatingJavaScriptFromStringVoid:(NSString *)script;

- (void) stopLoadingWebview;
- (void) setScrollContentsOffset:(CGPoint)offset;
- (void) setScrollContentsOffset:(CGPoint)offset animated:(BOOL)animated;
- (void) setScrollDelegate:(id)delegate;
- (void) setScalesPageToFit2:(BOOL)setPages;
- (void) setScrollDecelerationRate:(CGFloat)decelerationRate;

- (void) loadHTMLString:(NSString*)htmlString baseURL:(NSURL *)baseURL;
- (void) setAddViewInScrollView:(UIView *)aView;
- (void) reloadWebView;

- (void) setInlineMediaPlayback:(BOOL)allow;
- (void) setMediaPlaybackRequireUserAction:(BOOL)allow;
- (double) getEstimatedProgress;

@end
