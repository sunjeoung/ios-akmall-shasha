//
//  UIWebView+FLUIWebView.m
//  FLWebView
//
//  Created by Steve Richey on 11/21/14.
//  Copyright (c) 2014 Float Mobile Learning. Shared under an MIT license. See license.md for details.
//

#import "UIWebView+FLUIWebView.h"

@implementation UIWebView (FLWebView)

- (void) reloadWebView {
    [self reload];
}

/*
 * Set any delegate view that implements UIWebViewDelegate.
 * FLWKWebView has a comparable method that looks for its own delegates.
 * Since this method is defined in FLWebViewProvider, we can call it in our view controller
 * no matter which web view was used.
*/
- (void) setDelegateViews: (id <UIWebViewDelegate>) delegateView
{
    [self setDelegate: delegateView];
}

- (void) setScrollDecelerationRate:(CGFloat)decelerationRate {
    self.scrollView.decelerationRate = decelerationRate;
}

/*
 * Same implementation as FLWKWebView.
*/
- (void) loadRequestFromString: (NSString *) urlNameAsString
{
    [self loadRequest: [NSURLRequest requestWithURL:[NSURL URLWithString: urlNameAsString]]];
}

/*
 * The current URL is stored within the request property.
 * WKWebView has this available as a property, so we add it to UIWebView here.
*/
- (NSURL *) URL
{
    return [[self request] URL];
}

/*
 * Simple way to implement WKWebView's JavaScript handling in UIWebView.
 * Just evaluates the JavaScript and passes the result to completionHandler, if it exists.
 * Since this is defined in FLWebViewProvider, we can call this method regardless of the web view used.
*/
- (void) evaluateJavaScript: (NSString *) javaScriptString completionHandler: (void (^)(id, NSError *)) completionHandler
{
    // Since with UIWebView we have to call evaluateJavaScript from the main thread
    // so, this is to make sure we always call this method from the main thread.
    // Just a note, with WkWebView we can call evaluateJavaScript from any thread.
    if (![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self evaluateJavaScript: javaScriptString completionHandler:completionHandler];
        });
    }else{
        NSString *string = [self stringByEvaluatingJavaScriptFromString: javaScriptString];
        if (completionHandler) {
            completionHandler(string, nil);
        }
    }
}

- (NSString *)stringByEvaluatingJavaScriptFromString2:(NSString *)aScript
{
    __block NSString *res = nil;
    __block BOOL finish = NO;
    [self evaluateJavaScript:aScript completionHandler:^(NSString *result, NSError *error){
        res = result;
        finish = YES;
    }];
    
    while(!finish) {
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    return res;
}

- (void)stringByEvaluatingJavaScriptFromStringVoid:(NSString *)script
{
    [self evaluateJavaScript:script completionHandler:nil];
}

/*
 * WKWebView has nothing comparable to scalesPagesToFit, so we use this method instead.
 * Here, we just update scalesPagesToFit. In FLWKWebView, nothing happens.
*/
- (void) setScalesPageToFit2:(BOOL)setPages
{
    self.scalesPageToFit = setPages;
}


//from GhostWebView >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

- (void) setScrollContentsOffset:(CGPoint)offset {
    self.scrollView.contentOffset = offset;
}

- (void) setScrollContentsOffset:(CGPoint)offset animated:(BOOL)animated
{
    [self.scrollView setContentOffset:offset animated:animated];
}

- (void) setScrollDelegate:(id)delegate {
    self.scrollView.delegate = delegate;
}

- (void) stopLoadingWebview {
    [self stopLoading];
}

- (void) setAddViewInScrollView:(UIView *)aView
{
    self.scrollView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    [self.scrollView  addSubview:aView];
}

- (NSString*) getPageTitle
{
    return [self stringByEvaluatingJavaScriptFromString:@"document.title"];
}

- (void) setInlineMediaPlayback:(BOOL)allow
{
    self.allowsInlineMediaPlayback = allow;
}

- (void) setMediaPlaybackRequireUserAction:(BOOL)allow
{
    self.mediaPlaybackRequiresUserAction = allow;
}

- (double) getEstimatedProgress {
    return -1; //not support
}

@end
