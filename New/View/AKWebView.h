//
//  AKWebView.h
//  AKMall
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import <WebKit/WKProcessPool.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, AKWebViewType) {
    AKWebViewTypeNone = 1000,
    AKWebViewTypeMain,
    AKWebViewTypeWeb,
    AKWebViewTypePopup,
    AKWebViewTypeOutWeb,
    AKWebViewTypeIntro
};

@interface WKWebViewPoolHandler : NSObject
{
}
+ (WKProcessPool *)pool;
@end

@protocol AKWebViewDelegate <NSObject>

@optional
- (void)didGotoSubViewController:(NSString *)aUrl;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)moveTabIndex:(NSInteger)nIndex;
- (void)showBack:(BOOL)bShow;
- (void)gotoMainLater;
- (void)introErrorWebView;
- (void)igaTracking;
- (void)removePopupView;
- (void)updateViewHeight:(NSDictionary *)dict;
- (void)changePopupTitle:(NSString *)title;
@end

@interface AKWebView : UIView

@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, assign) id<AKWebViewDelegate> delegate;
@property (nonatomic, assign) AKWebViewType type;

- (void)loadRequest:(NSURLRequest *)request;
- (void)requestUrl:(NSString *)url;
- (void)reloadWebView;
- (void)callGATrackerJavaScript:(NSString *)script;
- (BOOL)canGoBack;
- (void)goBack;

@end

NS_ASSUME_NONNULL_END
