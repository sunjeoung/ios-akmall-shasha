//
//  AKPopupView.m
//  AKMall
//
//  Created by 김상현 on 2018. 4. 12..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "AKPopupView.h"
#import "MainToolbar.h"

#define MAS_SHORTHAND
#define MAS_SHORTHAND_GLOBALS
#import "Masonry.h"

#define kPopupViewWidth    303
#define kPopupViewHeight   492

#define xGap                15
#define kButtonSize         30

@interface AKPopupView() <UIScrollViewDelegate>
{
    int nPopupCount;
}

@property (nonatomic, strong) UIButton *btnPrev;
@property (nonatomic, strong) UIButton *btnNext;

@end

@implementation AKPopupView

- (instancetype)initWithFrame:(CGRect)frame {

    self = [super initWithFrame:frame];

    if (self) {
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4f];

        _popupView = ({
            UIView *popupView = UIView.new;
            popupView.backgroundColor = UIColorFromRGB(0xda6699);
            [self addSubview:popupView];
            popupView;
        });
        
        _scrollView = ({
            UIScrollView *scrView = UIScrollView.new;
            scrView.showsVerticalScrollIndicator = NO;
            scrView.showsHorizontalScrollIndicator = NO;
            scrView.pagingEnabled = YES;
            scrView.delegate = self;
            //scrView.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTouchPopup)];
            [scrView addGestureRecognizer:singleTap];

            [_popupView addSubview:scrView];
            scrView;
        });

        _pageControl = ({
            UIPageControl *pageControl = UIPageControl.new;
            pageControl.userInteractionEnabled = NO;
            pageControl.currentPage = 0;
            pageControl.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            pageControl.numberOfPages = [[IUserDataManager getMobilePopup] count];
            pageControl.currentPageIndicatorTintColor = UIColorFromRGB(0xffffff);
            pageControl.pageIndicatorTintColor = [UIColorFromRGB(0xffffff) colorWithAlphaComponent:0.4f];
            [_popupView addSubview:pageControl];
            pageControl;
        });

        nPopupCount = (int)[[IUserDataManager getMobilePopup] count];
        
        _btnNext = ({
            UIButton *btn = UIButton.new;
            btn.titleLabel.font = [UIFont boldSystemFontOfSize:14.f];
            [btn setTitle:@"다음" forState:UIControlStateNormal];
            btn.alpha = (nPopupCount == 1) ? 0.4f : 1.0f;
            btn.enabled = !(nPopupCount == 1);
            [btn setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(didTouchNext) forControlEvents:UIControlEventTouchUpInside];
            [_popupView addSubview:btn];
            btn;
        });

        _btnPrev = ({
            UIButton *btn = UIButton.new;
            btn.titleLabel.font = [UIFont boldSystemFontOfSize:14.f];
            [btn setTitle:@"이전" forState:UIControlStateNormal];
            [btn setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
            btn.alpha = 0.4f;
            btn.enabled = NO;
            [btn addTarget:self action:@selector(didTouchPrev) forControlEvents:UIControlEventTouchUpInside];
            [_popupView addSubview:btn];
            btn;
        });

        _btnClose = ({
            UIButton *btn = UIButton.new;
            btn.backgroundColor = [UIColor clearColor];
            btn.titleLabel.font = [UIFont systemFontOfSize:14.f];
            btn.layer.borderColor = UIColorFromRGB(0xFFFFFF).CGColor;
            btn.layer.borderWidth = 1;
            [btn setTitle:@"닫기" forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(didTouchPopupClose) forControlEvents:UIControlEventTouchUpInside];
            [_popupView addSubview:btn];
            btn;
        });

        _btnDontShow = ({
            UIButton *btn = UIButton.new;
            btn.backgroundColor = [UIColor clearColor];
            btn.titleLabel.font = [UIFont systemFontOfSize:14.f];
            btn.layer.borderColor = UIColorFromRGB(0xFFFFFF).CGColor;
            btn.layer.borderWidth = 1;
            [btn setTitle:@"오늘 하루 보지 않기" forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(didTouchDontShowToday) forControlEvents:UIControlEventTouchUpInside];
            [_popupView addSubview:btn];
            btn;
        });
    }

    return self;
}

- (void)layoutSubviews
{
    CGFloat xPos = (kScreenBoundsWidth - kPopupViewWidth)/2;
    CGFloat yPos = (kScreenBoundsHeight - kPopupViewHeight)/2; //(kScreenBoundsHeight - kPopupViewHeight - [CommonFunc safeBottomY])/2;

    [_popupView updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.left).offset(xPos);
        make.top.equalTo(self.top).offset(yPos);
        make.width.equalTo(kPopupViewWidth);
        make.height.equalTo(kPopupViewHeight);
    }];
    
    [_scrollView updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.popupView.left);
        make.top.equalTo(self.popupView.top);
        make.width.equalTo(kPopupViewWidth);
        make.height.equalTo(381);
    }];
    
    _scrollView.contentSize = CGSizeMake(kPopupViewWidth * nPopupCount, 381);
    
    NSArray *arrMobiePopup = [IUserDataManager getMobilePopup];
    [arrMobiePopup enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *dict = obj;
        NSString *popup_img_url = dict[@"popup_img_url"];
        
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:popup_img_url]]];
        if (image) {
            UIImageView *ivPopup = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.popupView.frame), CGRectGetHeight(self.scrollView.frame))];
            ivPopup.image = image;
            ivPopup.frame = CGRectMake(idx * kPopupViewWidth, 0, kPopupViewWidth, 381);
            [self.scrollView addSubview:ivPopup];
        }
    }];
    
    [_btnPrev updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.popupView.left).offset(15);
        make.top.equalTo(self.scrollView.bottom).offset(9);
        make.width.equalTo(kButtonSize);
        make.height.equalTo(kButtonSize);
    }];
    
    [_btnNext updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.popupView.right).offset(-15);
        make.top.equalTo(self.scrollView.bottom).offset(9);
        make.width.equalTo(kButtonSize);
        make.height.equalTo(kButtonSize);
    }];
    
    CGFloat nHeight = _pageControl.frame.size.height;
    CGFloat nCenterY = (kButtonSize - nHeight) / 2;
    
    [_pageControl updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.btnPrev.right).offset(10);
        make.right.equalTo(self.btnNext.left).offset(-10);
        make.top.equalTo(self.btnNext.top).offset(nCenterY);
    }];
    
    [_btnDontShow updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.popupView.left).offset(12);
        make.top.equalTo(self.btnPrev.bottom).offset(13);
        make.width.equalTo(136);
        make.height.equalTo(44);
    }];
    
    [_btnClose updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.btnDontShow.right).offset(7);
        make.top.equalTo(self.btnPrev.bottom).offset(13);
        make.width.equalTo(136);
        make.height.equalTo(44);
    }];
}

- (void)layoutSubviews2 {

    CGFloat xPos = 0;
    CGFloat yPos = 0;

    // 팝업 뷰
    xPos = (kScreenBoundsWidth - kPopupViewWidth)/2;
    yPos = (kScreenBoundsHeight - kPopupViewHeight - [MainToolbar height])/2;
    _popupView.frame = CGRectMake(xPos, yPos, kPopupViewWidth, kPopupViewHeight);


    // 스크롤 뷰
    _scrollView.frame = CGRectMake(0, 0, CGRectGetWidth(self.popupView.frame), 381);
    _scrollView.contentSize = CGSizeMake(CGRectGetWidth(_popupView.frame) * _pageControl.numberOfPages, CGRectGetHeight(_scrollView.frame));

    if ([IUserDataManager getMobilePopup]) {
        NSArray *arrMobiePopup = [IUserDataManager getMobilePopup];
        [arrMobiePopup enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *dict = obj;
            NSString *popup_img_url = dict[@"popup_img_url"];

            UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:popup_img_url]]];
            if (image) {
                UIImageView *ivPopup = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.popupView.frame), CGRectGetHeight(self.scrollView.frame))];
                ivPopup.image = image;
                ivPopup.frame = CGRectMake(idx * CGRectGetWidth(self.popupView.frame),
                                           0,
                                           CGRectGetWidth(self.popupView.frame),
                                           CGRectGetHeight(self.scrollView.frame));
                [self.scrollView addSubview:ivPopup];
            }
        }];
    }


    [_popupView addSubview:_scrollView];


    // 페이지 컨트롤
    [_pageControl sizeToFit];
    xPos = (CGRectGetWidth(_popupView.frame) - CGRectGetWidth(_pageControl.frame))/2;
    yPos = CGRectGetMaxY(_scrollView.frame) + 20;
    _pageControl.frame = CGRectMake(xPos, yPos, CGRectGetWidth(_pageControl.frame), CGRectGetHeight(_pageControl.frame));
    [_popupView addSubview:_pageControl];


    // 다음
    [_btnNext sizeToFit];
    CGFloat width = 30;
    CGFloat height = 30;
    xPos = CGRectGetWidth(_popupView.frame) - width - xGap;
    yPos = CGRectGetMaxY(_scrollView.frame) + 9;
    _btnNext.frame = CGRectMake(xPos, yPos, width, height);
    [_popupView addSubview:_btnNext];


    // 이전
    [_btnPrev sizeToFit];
    xPos = xGap;
    yPos = CGRectGetMaxY(_scrollView.frame) + 9;
    _btnPrev.frame = CGRectMake(xPos, yPos, width, height);
    [_popupView addSubview:_btnPrev];


    // 닫기
    [_btnClose sizeToFit];
    width = 136;
    height = 44;
    xPos = CGRectGetWidth(_popupView.frame) - width - 12;
    yPos = kPopupViewHeight - height - xGap;
    _btnClose.frame = CGRectMake(xPos, yPos, width, height);
    [_popupView addSubview:_btnClose];


    // 오늘 하루 보기 않기
    [_btnDontShow sizeToFit];
    xPos = 12;
    _btnDontShow.frame = CGRectMake(xPos, yPos, width, height);
    [_popupView addSubview:_btnDontShow];


}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    NSInteger currentOffsetX = scrollView.contentOffset.x;

    if (currentOffsetX < 0 || currentOffsetX > (scrollView.contentSize.width/_pageControl.numberOfPages) * (_pageControl.numberOfPages - 1)) {
        scrollView.scrollEnabled = NO;
        return;
    }

    scrollView.scrollEnabled = YES;
    NSInteger index = currentOffsetX / self.popupView.frame.size.width;

    //페이징 스크롤이 완전히 끝나야 페이지 인덱스가 바뀜
    if (currentOffsetX % (NSInteger)self.popupView.frame.size.width == 0) {
        _pageControl.currentPage = index;

        [self setAlphaPrevAndNextButton];
    }
}


#pragma mark - Touch Event

- (void)didTouchPopupClose {
    [self removeFromSuperview];
}

- (void)didTouchDontShowToday {
    [IUserDataManager setDidTouchDontShowTodayDate:[NSDate date]];
    [self removeFromSuperview];
}


- (void)didTouchPopup {
    NSInteger selectedPage = _pageControl.currentPage;

    NSArray *arrMobilePopup = [IUserDataManager getMobilePopup];
    NSString *popup_link_url = [arrMobilePopup objectAtIndex:selectedPage][@"popup_link_url"];

    if (_delegate && [_delegate respondsToSelector:@selector(didTouchPopupView:code:)]) {
        [_delegate didTouchPopupView:popup_link_url code:[arrMobilePopup objectAtIndex:selectedPage][@"gaCode"]];
    }
}

- (void)didTouchNext {
    CGFloat curOffsetX = _scrollView.contentOffset.x;
    if (curOffsetX < (_scrollView.contentSize.width/_pageControl.numberOfPages) * (_pageControl.numberOfPages - 1)) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear
                             animations:^{
                                 self.scrollView.contentOffset = CGPointMake(curOffsetX + CGRectGetWidth(self.popupView.frame), 0);
                             }
                             completion:nil];
        });
        [self setAlphaPrevAndNextButton];
    } else {
        _btnNext.alpha = 0.4f;
    }
}

- (void)didTouchPrev {
    CGFloat curOffsetX = _scrollView.contentOffset.x;
    if (curOffsetX > 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear
                             animations:^{
                                 self.scrollView.contentOffset = CGPointMake(curOffsetX - CGRectGetWidth(self.popupView.frame), 0);
                             }
                             completion:nil];
        });
    }
    [self setAlphaPrevAndNextButton];
}

#pragma mark - Private Func

- (void)setAlphaPrevAndNextButton {
    if (_pageControl.currentPage + 1 == _pageControl.numberOfPages) {
        _btnNext.alpha = 0.4f;
        _btnNext.enabled = NO;

        _btnPrev.alpha = 1.0f;
        _btnPrev.enabled = YES;
    } else if (_pageControl.currentPage == 0){
        _btnNext.alpha = 1.0f;
        _btnNext.enabled = YES;

        _btnPrev.alpha = 0.4f;
        _btnPrev.enabled = NO;
    } else {
        _btnNext.alpha = 1.0f;
        _btnNext.enabled = YES;

        _btnPrev.alpha = 1.0f;
        _btnPrev.enabled = YES;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
