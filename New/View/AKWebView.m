//
//  AKWebView.m
//  AKMall
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "AKWebView.h"
#import "AKSchemeManager.h"
#import "AKProperty.h"
#import "AKCommonUtil.h"

@interface AKWebView() <WKNavigationDelegate, WKUIDelegate, UIScrollViewDelegate, AKSchemeManagerDelegate>
{
    AKSchemeManager *_schemeManager;
    BOOL isFinished;
}

@property (assign) BOOL dontShowPopup;
@property (nonatomic, strong) NSString *currentUrl;

@end

@implementation WKWebViewPoolHandler

+ (WKProcessPool *) pool
{
    static dispatch_once_t onceToken;
    static WKProcessPool *_pool;
    dispatch_once(&onceToken, ^{
        _pool = [[WKProcessPool alloc] init];
    });
    return _pool;
}

@end

@implementation AKWebView

- (void)loadRequest:(NSURLRequest *)request
{
    [_webView loadRequest:request];
}

/**
*  서버로 부터 전송된 URI 중에 호스트가 포함되어 있는지 확인 후
* 호스트 주소가 포함되어 있지 않다면 추가하여 웹을 로드하도록 한다.
* - Parameters:
*   - url : 연결할 웹뷰 주소
* - Returns:  void
*/
- (void)requestUrl:(NSString *)url
{
    self.currentUrl = url;
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString: self.currentUrl]]];
}

- (void)reloadWebView
{
    [_webView.scrollView setContentOffset:CGPointMake(0, 0)];
    
    if (self.currentUrl && ([self.currentUrl isEqualToString:@""] == NO)) {
        [self requestUrl:self.currentUrl];
    }
}

- (void)callGATrackerJavaScript:(NSString *)script
{
    [_webView evaluateJavaScript:script completionHandler:nil];
}

- (NSString *)removeParameter:(NSString *)aUrl
{
    NSRange range = [aUrl rangeOfString:@"?"];
    if (range.location != NSNotFound) {
        aUrl = [aUrl substringToIndex:range.location];
    }
    
    return aUrl;
}

#pragma mark - AKSchemeManager Delegate

- (BOOL)canGoBack
{
    return [_webView canGoBack];
}

- (void)goBack
{
    [_webView goBack];
}

- (void)closePopup:(NSString *)type
{
    if (self.delegate  && [self.delegate respondsToSelector:@selector(removePopupView)]) {
        [self.delegate removePopupView];
    }
}

- (void)updateHeight:(NSDictionary *)dic {
    if (self.delegate && [self.delegate respondsToSelector:@selector(updateViewHeight:)]) {
        [self.delegate updateViewHeight:dic];
        //_webViewHeight.constant = [dic[@"h"] floatValue];
    }
    
}

#pragma mark - WKWebView WKUIDelegate

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        @try {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:webView.URL.host message:message preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                completionHandler();
            }]];
            [[AKCommonUtil getVisibleController] presentViewController:alertController animated:YES completion:nil];
        }
        @catch (NSException *exception) {
        }
        @finally {
        }
    });
}

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler {
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        // TODO We have to think message to confirm "YES"
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:webView.URL.host message:message preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            completionHandler(YES);
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"취소" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            completionHandler(NO);
        }]];
        [alertController.view setNeedsLayout];
        
        [[AKCommonUtil getVisibleController] presentViewController:alertController animated:YES completion:nil];
    });
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString *))completionHandler {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:prompt message:webView.URL.host preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = defaultText;
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSString *input = ((UITextField *)alertController.textFields.firstObject).text;
        completionHandler(input);
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        completionHandler(nil);
    }]];
    
    [[AKCommonUtil getVisibleController] presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - WKWebView Delegate

//팝업 기능 사용을 원할 시 고려해볼 부분
- (WKWebView *)webView:(WKWebView *)webView
createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration
   forNavigationAction:(WKNavigationAction *)navigationAction
        windowFeatures:(WKWindowFeatures *)windowFeatures{
    WKWebView *newWebView = [[WKWebView alloc] initWithFrame:_webView.bounds configuration:configuration];
    newWebView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    newWebView.navigationDelegate = self;
    newWebView.UIDelegate = self;
    [self addSubview:newWebView];

    return newWebView;
}

- (void)webViewDidClose:(WKWebView *)webView
{
    [webView removeFromSuperview];
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    if (self.type == AKWebViewTypeMain) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(showBack:)]) {
            [self.delegate showBack:NO];
        }
    }
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"webView error is %@", [error debugDescription]);
    
    if ([AKCommonUtil checkWebViewError:error]) {
        [AKCommonUtil showAlert:nil msg:@"네트워크에 접속할 수 없습니다. 네트워크 연결상태를 확인해 주세요." cancelTitle:@"앱종료" cancelBlock:^(UIAlertAction * _Nullable action) {
            exit(0);
        } confirmTitle:@"재시도" confirmBlock:^(UIAlertAction * _Nullable action) {
            [webView reload];
        }];
    }
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    NSString *url = navigationAction.request.URL.absoluteString;
    NSString *scheme = navigationAction.request.URL.scheme;
    
    NSLog(@"url is %@", url);
    
    if ([url rangeOfString:@"itunes.apple.com"].location != NSNotFound) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{} completionHandler:nil];
        decisionHandler(WKNavigationActionPolicyCancel);
        
        return ;
    }
    

    if (self.type == AKWebViewTypeMain) {
        
//        if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"]) {
//
//            if ([url rangeOfString:@"/login"].location != NSNotFound) {
//
//            }
//            else if ([url rangeOfString:@"/login/LoginProcSSL.do"].location != NSNotFound) {
//
//            }
//            else if ([url rangeOfString:@"/sslControl.do"].location != NSNotFound) {
//
//            }
//            else if ([url rangeOfString:@"/main/Main.do"].location == NSNotFound) {
//
//                if ([url hasPrefix:BASE_URL] || [url hasPrefix:BASE_URL_SSL] ) {
//                    if (self.delegate && [self.delegate respondsToSelector:@selector(didGotoSubViewController:)]) {
//                        [self.delegate didGotoSubViewController:url];
//                    }
//                    decisionHandler(WKNavigationActionPolicyCancel);
//                    return;
//                }
//            }
//        }
//
//        BOOL isResult = [_schemeManager processScheme:webView url:url];
//        if (isResult) {
//            decisionHandler(WKNavigationActionPolicyAllow);
//        }
//        else {
//            decisionHandler(WKNavigationActionPolicyCancel);
//        }
    }
    else if (self.type == AKWebViewTypeWeb) {
        
//        if (isFinished) {
//            for (NSString *checkUrl in [AKProperty getNewWindow]) {
//                if ([[self removeParameter:url] rangeOfString:checkUrl].location != NSNotFound) {
//                    if (self.delegate && [self.delegate respondsToSelector:@selector(didGotoSubViewController:)]) {
//                        [self.delegate didGotoSubViewController:url];
//
//                        decisionHandler(WKNavigationActionPolicyCancel);
//                        return;
//                    }
//                }
//            }
//        }
//
//        if ([url hasPrefix:@"akfamily://"]) {
//            NSDictionary *scheme = [AKCommonUtil parseScheme:url];
//            if ([scheme[@"command"] isEqualToString:@"goBack"]) {
//                isFinished = NO;
//            }
//        }
//
//        if ([url rangeOfString:@"/Logout"].location != NSNotFound) {
//            [AKCommonUtil goUrl:url];
//            decisionHandler(WKNavigationActionPolicyCancel);
//            return;
//        }
//
//        BOOL isResult = [_schemeManager processScheme:webView url:url];
//        if (isResult) {
//            decisionHandler(WKNavigationActionPolicyAllow);
//        }
//        else {
//            decisionHandler(WKNavigationActionPolicyCancel);
//        }
    }
    else if (self.type == AKWebViewTypeOutWeb) {
//        if ([url hasPrefix:@"akfamily://"]) {
//            NSString *title = @"";
//            NSDictionary *scheme = [AKCommonUtil parseScheme:url];
//            if ([scheme[@"command"] isEqualToString:@"getTitle"]) {
//                title = [scheme[@"t"] stringByRemovingPercentEncoding];
//
//                if (self.delegate && [self.delegate respondsToSelector:@selector(changePopupTitle:)]) {
//                    [self.delegate changePopupTitle:@""];
//                }
//            }
//            decisionHandler(WKNavigationActionPolicyCancel);
//            return;
//        }

        decisionHandler(WKNavigationActionPolicyAllow);
    }
    else {
        decisionHandler(WKNavigationActionPolicyAllow);
    }

}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{
    NSHTTPURLResponse *response = (NSHTTPURLResponse *)navigationResponse.response;
    NSArray *cookies =[NSHTTPCookie cookiesWithResponseHeaderFields:[response allHeaderFields] forURL:response.URL];
    
    for (NSHTTPCookie *cookie in cookies) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
    }
    
    decisionHandler(WKNavigationResponsePolicyAllow);
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    if (self.type == AKWebViewTypeOutWeb) {
        [AKCommonUtil getTitle:webView];
    }
    else if (self.type == AKWebViewTypeIntro) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(gotoMainLater)]) {
            [self.delegate gotoMainLater];
        }
    }
    else if (self.type == AKWebViewTypeMain) {
        [AKCommonUtil checkBack:webView];
    }
    else if (self.type == AKWebViewTypeWeb) {
        [AKCommonUtil checkBack:webView];
        isFinished = YES;
    }
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(scrollViewDidScroll:)]) {
        [self.delegate scrollViewDidScroll:scrollView];
    }
}

#pragma mark - init

- (void)initWebView
{
    WKPreferences *thisPref = [[WKPreferences alloc] init];
    thisPref.javaScriptCanOpenWindowsAutomatically = YES;
    thisPref.javaScriptEnabled = YES;
    
    WKWebViewConfiguration* configuration = WKWebViewConfiguration.new;
    configuration.processPool = [WKWebViewPoolHandler pool];
    configuration.allowsInlineMediaPlayback = YES;
    configuration.mediaTypesRequiringUserActionForPlayback = NO;
    configuration.allowsPictureInPictureMediaPlayback = YES;
    configuration.mediaTypesRequiringUserActionForPlayback = WKAudiovisualMediaTypeNone;
    configuration.preferences = thisPref;
    configuration.applicationNameForUserAgent = [NSString stringWithFormat:@" FDSMALL;appv=%@;appid=%@;akpush",
                     [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], [AKCommonUtil getUUID]];
    
    _webView = [[WKWebView alloc] initWithFrame:self.bounds configuration:configuration];
    _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _webView.UIDelegate = self;
    _webView.navigationDelegate = self;
    _webView.multipleTouchEnabled = NO;
    _webView.allowsBackForwardNavigationGestures = YES;
    [self addSubview:_webView];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _schemeManager = [[AKSchemeManager alloc] init];
        _schemeManager.delegate = self;
        [self initWebView];
    }
    
    return self;
}

@end
