//
//  AKPopupView.h
//  AKMall
//
//  Created by 김상현 on 2018. 4. 12..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AKPopupViewDelegate <NSObject>
- (void)didTouchPopupView:(NSString *)url code:(NSString *)gaCode;
@end

@interface AKPopupView : UIView

@property (nonatomic, weak) id<AKPopupViewDelegate> delegate;

@property (nonatomic, strong) UIView *popupView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) UIButton *btnClose;
@property (nonatomic, strong) UIButton *btnDontShow;

@end
