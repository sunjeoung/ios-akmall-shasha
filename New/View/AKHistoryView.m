//
//  AKHistoryView.m
//  AKMall
//
//  Created by Sanghong Han on 2018. 5. 12..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "AKHistoryView.h"
#import "AKSubWebView.h"

@interface AKHistoryView() <AKSubWebViewDelegate>
{
    
}

@property (nonatomic, strong) AKSubWebView *webView;
@end

@implementation AKHistoryView

#pragma mark - AKSubWebView Delegate

- (void)closePopupView:(NSString *)type
{
    [appDelegate closeHistoryView];
}

#pragma mark - init

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(!self) {
        return self;
    }
    
    self.webView = [[AKSubWebView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    self.webView.webViewType = AKSubWebViewTypeHistoryMenu;
    self.webView.delegate = self;
    [self addSubview:self.webView];
    
    NSString *sHitoryURL = [NSString stringWithFormat:@"%@%@", BASE_URL, URL_HISTORY];
    [self.webView requestUrl:sHitoryURL];
    
    return self;
}

@end
