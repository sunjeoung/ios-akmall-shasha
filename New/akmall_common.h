//
//  akmall_common.h
//  AKMall
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#ifndef akmall_common_h
#define akmall_common_h

#import "AppDelegate.h"


#define     appDelegate                     (AppDelegate *)[[UIApplication sharedApplication] delegate]
#define     START_Y                         [[UIApplication sharedApplication] statusBarFrame].size.height

#define     kScreenBoundsWidth              ([UIScreen mainScreen].bounds.size).width
#define     kScreenBoundsHeight             ([UIScreen mainScreen].bounds.size).height

#define UIColorFromRGB(rgbValue)            [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0  \
                                                            green:((float)((rgbValue & 0xFF00) >> 8))/255.0     \
                                                            blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

// System
#define     IOS_VERSION                     [[[UIDevice currentDevice] systemVersion] floatValue]
#define     APP_VERSION                     [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]



#define     kHeaderHeight                   50
#define     kMainToolbarHeight              61
#define     kScrollOffset                   50

// Notification Define

#define     kLoadMktMessageNotification         @"LoadMktMessageNotification"
#define     kNewAlarmNotification               @"NewAlarmNotification"
#define     kCartCountNotification              @"CartCountNotification"
#define     kDispplayHistoryImageNotification   @"DispplayHistoryImageNotification"

#endif /* akmall_common_h */
