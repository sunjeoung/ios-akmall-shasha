//
//  SubViewController.h
//  AKMall
//
//  Created by seoyeon on 2018. 3. 14..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "AKBaseViewController.h"

@interface SubViewController : AKBaseViewController

@property (nonatomic, assign) BOOL isHiddenHeader;
@property (nonatomic, strong) NSString *url;
@property (strong, nonatomic) NSString *speechResult;

- (void)loadUrl:(NSString *)aUrl;

@end
