//
//  SubViewController.m
//  AKMall
//
//  Created by seoyeon on 2018. 3. 14..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "SubViewController.h"
#import "AKNewMainViewController.h"

#import "AKSubHeaderView.h"
#import "AKSubWebView.h"
#import "SubToolbar.h"
#import "UIScrollView+Customs.h"


#import <SafariServices/SafariServices.h>

@interface SubViewController () <
                                AKSubHeaderViewDelegate,
                                SubToolbarDelegate,
                                AKSubWebViewDelegate,
                                UIGestureRecognizerDelegate
                                >
{
    CGFloat _nPrev;
    CGFloat _nGap;
    BOOL _bUp;
    BOOL _isHiddenTabbar;
    BOOL _isHiddenHeader;
    
    SubToolbar *_subToolbar;
}

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@property (nonatomic, strong) AKSubWebView *webView;
@property (nonatomic, strong) AKSubHeaderView *header;

@end

@implementation SubViewController

#pragma mark - IBAction Method

- (IBAction)exitFromSpeech:(UIStoryboardSegue *)segue {
    if (_speechResult) {
        NSString *jsonStr = [AKCommonUtil getJsonStringFromObject:self.speechResult];
        jsonStr = [AKCommonUtil urlEncode:jsonStr];
        jsonStr = [NSString stringWithFormat:@"%@(\"%@\");", @"voiceSearch", [AKCommonUtil base64Encode:jsonStr]];
        [self.webView callJavaScript:jsonStr];
    }
}

- (IBAction)clickBack:(id)sender {
//    if ([_webView canGoBack]) {
//        [_webView goBack];
//    } else {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
}

#pragma mark - Private Function

- (void)showTabbar
{
    if (_isHiddenTabbar) {
        return ;
    }
    
    CGFloat nToolbarHeight = [SubToolbar height];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.3f
                         animations:^{
                             CGFloat yPos = kScreenBoundsHeight - nToolbarHeight;
                             self->_subToolbar.frame = CGRectMake(0, yPos, kScreenBoundsWidth, nToolbarHeight);
                         }
                         completion:^(BOOL finished) {
                             //bShowTabbar = YES;
                         }
         ];
    });
}

- (void)hideTabbar
{    
    if (_isHiddenTabbar) {
        return ;
    }
    
    CGFloat yPos = kScreenBoundsHeight;
    CGFloat nToolbarHeight = [SubToolbar height];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.3f
                         animations:^{
                             self->_subToolbar.frame = CGRectMake(0, yPos, kScreenBoundsWidth, nToolbarHeight);
                         }
                         completion:^(BOOL finished) {
                         }
         ];
    });
}

- (void)showHeader
{
    CGFloat yPos = START_Y;
    _header.frame = CGRectMake(0, yPos, kScreenBoundsWidth, kHeaderHeight);
    _header.hidden = NO;
    
    yPos = CGRectGetMaxY(_header.frame);
    _webView.frame = CGRectMake(0, yPos, kScreenBoundsWidth, kScreenBoundsHeight-yPos-[CommonFunc safeBottomY]);
    [_webView resizeWebView];
}

- (void)hideHeader
{
    CGFloat yPos = START_Y;
    _header.frame = CGRectMake(0, yPos, kScreenBoundsWidth, 0);
    _header.hidden = YES;
    
    yPos = CGRectGetMaxY(_header.frame);
    _webView.frame = CGRectMake(0, yPos, kScreenBoundsWidth, kScreenBoundsHeight-yPos-[CommonFunc safeBottomY]);
    [_webView resizeWebView];
}

- (void)callGATracker:(NSString *)code
{
    NSString *script = [NSString stringWithFormat:@"gaHandler('%@');", code];
    [_webView callJavaScript:script];
}

#pragma mark - AKSubHeaderView Delegate

- (void)didTouchSubHeaderView:(AKSubHeaderViewType)type
{
    switch (type) {
        case AKSubHeaderViewTypeCategory:
        {
            [appDelegate showLeftMenu];
            [self callGATracker:@"GMA_00_02"];
        }
            break;
        case AKSubHeaderViewTypeLogo:
        {
            [self callGATracker:@"GMA_00_01"];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
            break;
        case AKSubHeaderViewTypeSearchIcon:
        {
            [self callGATracker:@"GMA_00_03"];
            
            NSString *sSearchUrl = URL_SEARCH;
            NSString *sUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, sSearchUrl];
            [_webView requestUrl:sUrl];
        }
            break;
        case AKSubHeaderViewTypeCart:
        {
            [self callGATracker:@"GMA_00_04"];
            
            NSString *sUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, URL_BAG];
            [_webView requestUrl:sUrl];
            
        }
            break;
        default:
            break;
    }
}

- (void)didTouchSubToolbar:(SubToolbarType)type
{
    NSString *sUrl = @"";
    
    if (type == SubToolbarTypeBack) {
        [self callGATracker:@"GMA_01_07"];
        [self.webView goBack];
    }
    else if (type == SubToolbarTypeHome) {
        [self callGATracker:@"GMA_01_03"];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else if (type == SubToolbarTypeHistory) {
        [self callGATracker:@"GMA_01_06"];
        [appDelegate showHistoryView];
    }
    else {
        switch (type) {
            case SubToolbarTypeLikeIt:
                sUrl = URL_LIKEIT;
                [self callGATracker:@"GMA_01_02"];
                break;
            case SubToolbarTypeOrder:
                sUrl = URL_CAR;
                [self callGATracker:@"GMA_01_04"];
                break;
            case SubToolbarTypeMyAK:
                sUrl = URL_MYAK;
                [self callGATracker:@"GMA_01_05"];
                break;
            default:
                break;
        }
        sUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, sUrl];
        [_webView requestUrl:sUrl];
    }
}

#pragma mark - AKSubWebView Delegate

- (void)hiddenTabbar:(BOOL)isHidden
{
    _isHiddenTabbar = isHidden;
    if (_isHiddenTabbar) {
        CGFloat yPos = CGRectGetMaxY(_header.frame);
        _webView.frame = CGRectMake(0, yPos, kScreenBoundsWidth, kScreenBoundsHeight-yPos-[CommonFunc safeBottomY]);
        _subToolbar.hidden = YES;
        [_webView resizeWebView];
    }
    else {
        CGFloat nToolbarHeight = [SubToolbar height];
        CGFloat yPos = CGRectGetMaxY(_header.frame);
        _webView.frame = CGRectMake(0, yPos, kScreenBoundsWidth, kScreenBoundsHeight-yPos-[CommonFunc safeBottomY]);
        _subToolbar.frame = CGRectMake(0, kScreenBoundsHeight-nToolbarHeight, kScreenBoundsWidth, nToolbarHeight);
        _subToolbar.hidden = NO;
        [_webView resizeWebView];
    }
}

- (void)hiddenHeader:(BOOL)isHidden
{
    self.isHiddenHeader = isHidden;
    
    if (isHidden) {
        [self hideHeader];
    }
    else [self showHeader];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y < 0 ) {
        return ;
    }
    
    if (scrollView.contentOffset.y > (scrollView.contentSize.height - scrollView.frame.size.height - [SubToolbar height])) {
        return ;
    }
    
    if ([scrollView isAtbottom]) {
        [self hideTabbar];
    }
    
    if (_nPrev < scrollView.contentOffset.y) {
        if (!_bUp) { _nGap = 0; }
        _nGap = _nGap + (scrollView.contentOffset.y - _nPrev);
        _bUp = YES;
    }
    else {
        if (_bUp) { _nGap = 0; }
        _nGap = _nGap + (_nPrev - scrollView.contentOffset.y);
        _bUp = NO;
    }
    _nPrev = scrollView.contentOffset.y;
    
    if (_nGap >= kScrollOffset) {
        if (_bUp) {
            [self hideTabbar];
        }
        else {
            [self showTabbar];
        }
    }
}

- (void)openBrowser:(NSString *)aUrl {
    
    [AKCommonUtil openOutWebView:aUrl];
}

- (void)moveTabIndex:(NSInteger)nIndex
{
    [(AKNewMainViewController *)([[appDelegate window] rootViewController]) moveTabIndex:nIndex];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Public Method

- (void)loadUrl:(NSString *)aUrl
{
    [self.webView requestUrl:aUrl];
}

#pragma mark - init

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if ([self.webView canGoBack]) {
        return NO;
    }
    else return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = UIColorFromRGB(0xFFFFFF);
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    CGFloat yPos = START_Y;
    
    if (self.isHiddenHeader == NO) {
        _header = [[AKSubHeaderView alloc] initWithFrame:CGRectMake(0, yPos, kScreenBoundsWidth, kHeaderHeight)];
        _header.delegate = self;
        [self.mainView addSubview:_header];

        yPos = yPos + kHeaderHeight;
    }
    else {
        _header = [[AKSubHeaderView alloc] initWithFrame:CGRectMake(0, yPos, kScreenBoundsWidth, 0)];
        _header.delegate = self;
        [self.mainView addSubview:_header];
    }
    
    _header.hidden = self.isHiddenHeader;
    
    _webView = [[AKSubWebView alloc] initWithFrame:CGRectMake(0, yPos, kScreenBoundsWidth, kScreenBoundsHeight-yPos-[CommonFunc safeBottomY])];
    _webView.delegate = self;
    _webView.webViewType = AKSubWebViewTypeNormal;
    [self.mainView addSubview:_webView];

    [_webView requestUrl:self.url];

    
    CGFloat nToolbarHeight = [SubToolbar height];
    
    _subToolbar = [[SubToolbar alloc] initWithFrame:CGRectMake(0, kScreenBoundsHeight, kScreenBoundsWidth, nToolbarHeight)];
    _subToolbar.delegate = self;
    [self.mainView addSubview:_subToolbar];
}


@end
