//
//  IntroPopupController.h
//  AKMall
//
//  Created by seoyeon on 2018. 3. 21..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AKBaseViewController.h"

@interface IntroPopupController : AKBaseViewController

@property (nonatomic, strong) NSString *type;

@end
