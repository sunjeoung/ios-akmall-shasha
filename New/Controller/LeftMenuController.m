//
//  LeftMenuController.m
//  AKMall
//
//  Created by Sanghong Han on 2018. 4. 24..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "LeftMenuController.h"
#import "SubViewController.h"

#import "AKSubWebView.h"
#import "GlobalHeader.h"

@interface LeftMenuController () <AKSubWebViewDelegate>
{
    
}
@property (nonatomic, strong) AKSubWebView *webView;
@end

@implementation LeftMenuController


#pragma mark - AKSubWebView Delegate

- (void)gotoSubViewController:(NSString *)aUrl
{
    if ([[AKCommonUtil getVisibleController] isKindOfClass:[SubViewController class]]) {
        SubViewController *controller =(SubViewController *)[AKCommonUtil getVisibleController];
        [controller loadUrl:aUrl];
    }
    else {
        SubViewController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"SubViewController"];
        controller.isHiddenHeader = YES;
        controller.url = aUrl;
        [[AKCommonUtil getVisibleController].navigationController pushViewController:controller animated:YES];
    }
}

- (void)callJavaScriptFromWeb:(NSString *)aScript
{
    NSLog(@"aScript is %@", aScript);
    [self.webView callJavaScript:aScript];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

- (void)hiddenTabbar:(BOOL)isHidden
{
    
}

- (void)hiddenHeader:(BOOL)isHidden
{
    
}

- (void)closePopupView:(NSString *)type
{
    
}

- (void)openBrowser:(NSString *)aUrl
{
    
}

- (void)moveTabIndex:(NSInteger)nIndex
{
    
}

#pragma mark - init

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) {
        return self;
    }
    
    self.backgroundColor = UIColorFromRGB(0xFFFFFF);
    
    CGFloat yPos = START_Y;
    CGFloat xPos = 0;
    NSString *sUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, URL_CATEGORY];
    
    self.webView = [[AKSubWebView alloc] initWithFrame:CGRectMake(xPos, yPos, kScreenBoundsWidth, kScreenBoundsHeight-yPos-[CommonFunc safeBottomY])];
    self.webView.webViewType = AKSubWebViewTypeCategoryMenu;
    self.webView.delegate = self;
    [self addSubview:self.webView];
    [self.webView requestUrl:sUrl];
    
    return self;
}


@end
