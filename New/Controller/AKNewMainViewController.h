//
//  AKNewMainViewController.h
//  AKMallToday
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "AKBaseViewController.h"

@interface AKNewMainViewController : AKBaseViewController

- (void)refreshWeb:(NSString *)aUrl;
- (void)moveTabIndex:(NSInteger)nIndex;

@end
