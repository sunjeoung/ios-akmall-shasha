//
//  AKNewMainViewController.m
//  AKMallToday
//
//  Created by Sanghong Han on 2018. 3. 13..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "AKNewMainViewController.h"
#import "SubViewController.h"

#import "AKPopupView.h"
#import "AKHeaderView.h"
#import "CPTabMenuView.h"
#import "iCarousel.h"
#import "AKWebView.h"
#import "MainToolbar.h"
#import "AKProperty.h"

#import "UIScrollView+Customs.h"
#import "NSDictionary+TabDatas.h"
#import "NSString+Customs.h"

@interface AKNewMainViewController ()   <
                                        CPTabMenuViewDelegate,
                                        AKHeaderViewDelegate,
                                        iCarouselDelegate,
                                        iCarouselDataSource,
                                        AKWebViewDelegate,
                                        MainToolbarDelegate,
                                        AKPopupViewDelegate
                                        >
{
    AKHeaderView *_headerView;
    
    iCarousel *_contentsView;
    NSMutableArray *_tabViewArray;
    CGFloat _nPrev;
    CGFloat _nGap;
    BOOL _bUp;
}
@property (nonatomic, strong) CPTabMenuView *tabMenuView;
@property (nonatomic, strong) MainToolbar *mainToolbar;
@property (nonatomic, strong) NSArray *arrTabs;
@end

@implementation AKNewMainViewController

#pragma mark - Request API

- (void)requestTabMenuList
{
    [AKNetworkUtil requestTabMenuListWithSuccess:^(NSDictionary * _Nullable response) {
        self.arrTabs = response[@"data"];

        dispatch_async(dispatch_get_main_queue(), ^{
            self.tabMenuView.arrMenus = self.arrTabs;
            [self.tabMenuView loadTabMenu];
            [self loadContentView];
        });
    } failure:^(NSError * _Nonnull error) {

    }];
}

#pragma mark - Private Function

- (void)currentReload
{
    for (UIView *v in _contentsView.visibleItemViews) {
        for (UIView *sub in [v subviews]) {
            if ([sub isKindOfClass:[AKWebView class]]) {
                [(AKWebView *)sub reloadWebView];
            }
        }
    }
}


- (BOOL)compareSavedDayAndCurDayForMobilePopup {

    NSDate *curDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd"];

    NSString *curDay = [formatter stringFromDate:curDate];
    NSString *savedDay = @"";
    if ([IUserDataManager getDidTouchDontShowTodayDate]) {
        savedDay = [formatter stringFromDate:[IUserDataManager getDidTouchDontShowTodayDate]];
    }

    NSLog(@"cur Day = %@, saved Day = %@", curDay, savedDay);
    if ([curDay isEqualToString:savedDay]) {
        return YES;
    }
    return NO;
}


- (void)showPopupView {
    
    NSArray *arrMobiePopup = [IUserDataManager getMobilePopup];
    if (arrMobiePopup && ([arrMobiePopup count] > 0)) {
        AKPopupView *popupView = [[AKPopupView alloc] initWithFrame:CGRectMake(0, 0, kScreenBoundsWidth, kScreenBoundsHeight)];
        popupView.delegate = self;
        [self.view addSubview:popupView];
    }
}

- (void)showHistoryView {
    for (UIView *v in _contentsView.visibleItemViews) {
        for (UIView *sub in [v subviews]) {
            if ([sub isKindOfClass:[AKWebView class]]) {
                //hans
                //[((AKWebView *)sub).webView stringByEvaluatingJavaScriptFromStringVoid:@"javascript:historyFun.toggle()"];
                break;
            }
        }
    }
}

#pragma mark - AKHeaderView Delegate

- (void)didTouchHeaderView:(AKHeaderViewType)type
{
    switch (type) {
        case AKHeaderViewTypeCategory:
        {
//            [AKCommonUtil showCategoryView:self.view];
            [appDelegate showLeftMenu];
            [self callGATracker:@"GMA_00_02"];
        }
            break;
        case AKHeaderViewTypeLogo:
        {
            //홈으로 이동 후 reload
            [_contentsView setCurrentItemIndex:0];
            [self currentReload];
            [self callGATracker:@"GMA_00_01"];
        }
            break;
            
        case AKHeaderViewTypeSearchText:
        {
            [self callGATracker:@"GMA_00_07"];
            
            SubViewController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"SubViewController"];
            controller.isHiddenHeader = YES;
            controller.url = [NSString stringWithFormat:@"%@%@", BASE_URL, URL_SEARCH];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
            
        case AKHeaderViewTypeSearchIcon:
        {
            [self callGATracker:@"GMA_00_03"];
            
            NSString *sSearchUrl = URL_SEARCH;
            
            NSDictionary *dictInfo = [appDelegate dictSearchInfo];
            if (dictInfo && dictInfo[@"search_link"]) {
                sSearchUrl = dictInfo[@"search_link"];
            }
            
            SubViewController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"SubViewController"];
            controller.isHiddenHeader = YES;
            controller.url = [NSString stringWithFormat:@"%@%@", BASE_URL, sSearchUrl];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
            
        case AKHeaderViewTypeCart:
        {
            [self callGATracker:@"GMA_00_04"];
            
            SubViewController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"SubViewController"];
            controller.isHiddenHeader = NO;
            controller.url = [NSString stringWithFormat:@"%@%@", BASE_URL, URL_BAG];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        default:
            break;
    }
}

#pragma mark - CPTabMenuView Delegate

- (void)didTouchTabMenuButton:(NSInteger)index
{
    NSDictionary *dict = _arrTabs[index];
    NSString *code = [NSString stringWithFormat:@"MTM_%@", dict[@"tabid"]];
    [self callGATracker:code];
    
    [_contentsView setCurrentItemIndex:index];
}

#pragma mark - iCarousel Delegate

- (id)findTabViewItem:(int)tag
{
    for (UIView *item in _tabViewArray) {
        if (item.tag == tag+100) {
            return item;
        }
    }
    
    return nil;
}

- (id)createMainView:(int)nIndex
{
    if ([self.arrTabs count] > nIndex) {
        NSDictionary *dict = [self.arrTabs objectAtIndex:nIndex];
        if ([dict isWebView]) {
            AKWebView *webView = [[AKWebView alloc] initWithFrame:_contentsView.bounds];
            webView.delegate = self;
            webView.tag = nIndex+100;
            return webView;
        }
        else {
            return nil;
        }
    }
    else {
        // Exception
        return nil;
    }
}

- (void)carouselUpdateChildFrame:(UIView *)currentView frame:(CGRect)bounds
{
    
}

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [self.arrTabs count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBoundsWidth, CGRectGetHeight(_contentsView.frame))];
    [view setBackgroundColor:[UIColor whiteColor]];
    
    id mainView;
    NSDictionary *dict = [self.arrTabs objectAtIndex:index];
    
    if (_tabViewArray.count > 0) {
        mainView = [self findTabViewItem:(int)index];
        if (mainView) {
            [view addSubview:mainView];
        }
        else {
            mainView = [self createMainView:(int)index];
            [mainView requestUrl:[dict url]];
            [view addSubview:mainView];
            
            @synchronized(_tabViewArray) {
                [_tabViewArray addObject:mainView];
            }
        }
    }
    else {
        mainView = [self createMainView:(int)index];
        [mainView requestUrl:[dict url]];
        [view addSubview:mainView];
        
        @synchronized(_tabViewArray) {
            [_tabViewArray addObject:mainView];
        }
    }
    
    return view;
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel
{
    return kScreenBoundsWidth;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option) {
        case iCarouselOptionWrap:
            return YES;
            break;
            
        case iCarouselOptionVisibleItems:
            // 기본 값은 3
            return 3;
            break;
            
        case iCarouselOptionSpacing:
            return value;
            break;
        default:
            break;
    }
    
    return value;
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel
{
    NSLog(@"carousel.currentItemIndex is %d", (int)carousel.currentItemIndex);
    
    NSDictionary *dict = _arrTabs[carousel.currentItemIndex];
    NSString *code = [NSString stringWithFormat:@"MTP_%@", dict[@"tabid"]];
    [self callGATracker:code];
    
    [self.tabMenuView tabMenuCurrentItemIndexDidChange:carousel.currentItemIndex];
}

#pragma mark - MainToolbar Delegate

- (void)didTouchMainToolbar:(MainToolbarType)type
{
    NSString *sUrl = @"";
    
    if (type == MainToolbarTypeHome) {
        //홈으로 이동 후 reload
        [_contentsView setCurrentItemIndex:0];
        [self currentReload];
        [self callGATracker:@"GMA_01_3"];
    }
    else if (type == MainToolbarTypeHistory) {
        //[self showHistoryView];
        [self callGATracker:@"GMA_01_06"];
        //[self showHistoryView];
        [appDelegate showHistoryView];
    }
    else {
        switch (type) {
            case MainToolbarTypeLikeIt:
                sUrl = URL_LIKEIT;
                [self callGATracker:@"GMA_01_02"];
                break;
            case MainToolbarTypeOrder:
                sUrl = URL_CAR;
                [self callGATracker:@"GMA_01_04"];
                break;
            case MainToolbarTypeMyAK:
                sUrl = URL_MYAK;
                [self callGATracker:@"GMA_01_05"];
                break;
            default:
                break;
        }
        
        SubViewController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"SubViewController"];
        controller.url = [NSString stringWithFormat:@"%@%@", BASE_URL, sUrl];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - AKWebView Delegate

- (void)showTabbar
{
    CGFloat nToolbarHeight = [MainToolbar height];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.3f
                         animations:^{
                             CGFloat yPos = kScreenBoundsHeight - nToolbarHeight;
                             self.mainToolbar.frame = CGRectMake(0, yPos, kScreenBoundsWidth, nToolbarHeight);
                         }
                         completion:^(BOOL finished) {
                             //bShowTabbar = YES;
                         }
         ];
    });
}

- (void)hideTabbar
{
    CGFloat yPos = kScreenBoundsHeight;
    CGFloat nToolbarHeight = [MainToolbar height];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.3f
                         animations:^{
                             self.mainToolbar.frame = CGRectMake(0, yPos, kScreenBoundsWidth, nToolbarHeight);
                         }
                         completion:^(BOOL finished) {
                         }
         ];
    });
}

//- (CGFloat)verticalOffsetForBottom:(UIScrollView *)scrollView {
//    CGFloat scrollViewHeight = scrollView.bounds.size.height;
//    CGFloat scrollContentSizeHeight = scrollView.contentSize.height;
//    CGFloat bottomInset = scrollView.contentInset.bottom;
//    CGFloat scrollViewBottomOffset = scrollContentSizeHeight + bottomInset - scrollViewHeight;
//    return scrollViewBottomOffset;
//}
//
//- (BOOL)isAtBottom:(UIScrollView *)scrollView {
//    return (scrollView.contentOffset.y >= [self verticalOffsetForBottom:scrollView]);
//}

- (void)didGotoSubViewController:(NSString *)aUrl
{
    BOOL isHiddenHeader = NO;
    
    NSArray *arrURL = [IUserDataManager getHiddenHeaderUrl];
    for (NSString *url in arrURL) {
        if ([aUrl rangeOfString:url].location != NSNotFound) {
            isHiddenHeader = YES;
            break;
        }
    }
    
    SubViewController *controller = [[SubViewController alloc] init];
    controller.isHiddenHeader = isHiddenHeader;
    controller.url = aUrl;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y < 0 ) {
        return ;
    }
    
    if (scrollView.contentOffset.y > (scrollView.contentSize.height - scrollView.frame.size.height - [MainToolbar height])) {
        return ;
    }
    
    if ([scrollView isAtbottom]) {
        [self hideTabbar];
    }
    
    if (_nPrev < scrollView.contentOffset.y) {
        if (!_bUp) { _nGap = 0; }
        _nGap = _nGap + (scrollView.contentOffset.y - _nPrev);
        _bUp = YES;
    }
    else {
        if (_bUp) { _nGap = 0; }
        _nGap = _nGap + (_nPrev - scrollView.contentOffset.y);
        _bUp = NO;
    }
    _nPrev = scrollView.contentOffset.y;
    
    if (_nGap >= kScrollOffset) {
        if (_bUp) {
            [self hideTabbar];
        }
        else {
            [self showTabbar];
        }
    }
}

- (void)moveTabIndex:(NSInteger)nIndex
{
    [self didTouchTabMenuButton:nIndex];
}

#pragma mark - AKPopupView Delegate

- (void)didTouchPopupView:(NSString *)url code:(NSString *)gaCode {
    
    if ((url==nil) || ([url isEqualToString:@""])) {
        return ;
    }

    [self callGATracker:gaCode];
    
    SubViewController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"SubViewController"];
    controller.url = [NSString stringWithFormat:@"%@%@", BASE_URL, url];;
    [self.navigationController pushViewController:controller animated:YES];
}


#pragma mark - Public Method

- (void)refreshWeb:(NSString *)aUrl
{
    NSURL *mainUrl = [NSURL URLWithString:aUrl];
    NSString *query = mainUrl.query;
    NSDictionary *dictParams = query ? [query parseURLParams] : nil;
    
    NSString *sTabIndex = dictParams ? dictParams[@"mainTabIndex"] : @"";
    int nTabIndex = -1;
    
    if (sTabIndex && ([sTabIndex isEqualToString:@""] == NO)) {
        nTabIndex = [sTabIndex intValue];
        [_contentsView setCurrentItemIndex:nTabIndex];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            NSDictionary *dict = self.arrTabs[nTabIndex];
            NSString *sUrl = [dict url];
            
            for (NSString *key in [dictParams allKeys]) {
                if ([key isEqualToString:@"mainTabIndex"] == NO) {
                    sUrl = [NSString stringWithFormat:@"%@&%@=%@", sUrl, key, [dictParams[key] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
                }
            }

            for (UIView *v in self->_contentsView.visibleItemViews) {
                for (UIView *sub in [v subviews]) {
                    if ([sub isKindOfClass:[AKWebView class]]) {
                        [(AKWebView *)sub requestUrl:sUrl];
                    }
                }
            }
        });
    }
    else {
        [_contentsView setCurrentItemIndex:0];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
 
            NSDictionary *dict = self.arrTabs[0];
            NSString *sUrl = [dict url];
            
            for (UIView *v in self->_contentsView.visibleItemViews) {
                for (UIView *sub in [v subviews]) {
                    if ([sub isKindOfClass:[AKWebView class]]) {
                        [(AKWebView *)sub requestUrl:sUrl];
                    }
                }
            }
        });
    }
//
//    [self performSelector:@selector(currentReload) withObject:nil afterDelay:0.2f];
}

- (void)callGATracker:(NSString *)code
{
    for (UIView *v in _contentsView.currentItemView.subviews) {
        if ([v isKindOfClass:[AKWebView class]]) {
            NSString *script = [NSString stringWithFormat:@"gaHandler('%@');", code];
            [(AKWebView *)v callGATrackerJavaScript:script];
        }
    }
}

#pragma mark - init

- (void)makeTabView
{
    CGFloat yPos = CGRectGetMaxY(_headerView.frame);
    
    self.tabMenuView = [[CPTabMenuView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 50)];
    self.tabMenuView.tabdelegate = self;
    [self.view addSubview:self.tabMenuView];
}

- (void)makeHeaderView
{
    CGFloat yPos = START_Y;
    _headerView = [[AKHeaderView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, kHeaderHeight)];
    _headerView.delegate = self;
    [self.view addSubview:_headerView];
}

- (void)loadContentView
{
    _contentsView = nil;
    
    CGFloat yPos = CGRectGetMaxY(self.tabMenuView.frame);
    
    _contentsView = [[iCarousel alloc] initWithFrame:CGRectMake(0, yPos, kScreenBoundsWidth, kScreenBoundsHeight-yPos)]; // note: 아이폰 X에서 fullscreen으로 보여주려면 START_Y를 제거합니다.
    [_contentsView setBackgroundColor:UIColorFromRGB(0xFFFFFF)];
    [_contentsView setDelegate:self];
    [_contentsView setDataSource:self];
    [_contentsView setType:iCarouselTypeLinear];
    [_contentsView setDecelerationRate:0.6f];
    [_contentsView setScrollSpeed:1.f];
    [_contentsView setBounceDistance:0.5f];
    [_contentsView setCenterItemWhenSelected:YES];
    [_contentsView setPagingEnabled:YES];
    //[self.view addSubview:_contentsView];
    [self.view insertSubview:_contentsView belowSubview:self.mainToolbar];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *pushId = [AKProperty getPushId];
    NSString *deepLinkUrl = [AKProperty getDeepLinkUrl];
    
    if (pushId && ![pushId isEqualToString:@""]) {
        NSString *url = [[NSString stringWithFormat:@"%@%@", PUSH_DETAIL_URL, pushId] stringByRemovingPercentEncoding];
        
        [AKCommonUtil pushWebViewController:url];
        [AKProperty setPushId:@""];
        
    } else if (deepLinkUrl && ![deepLinkUrl isEqualToString:@""]) {
        [AKCommonUtil pushWebViewController:deepLinkUrl];
        [AKProperty setDeepLinkUrl:@""];
    } else if ([[appDelegate returnUrl]  containsString:@"GoodsDetail.do"]) {
        NSString *loadUrl = [appDelegate returnUrl];
        [appDelegate setReturnUrl:@""];
        [AKCommonUtil pushWebViewController:loadUrl];
        
        //add 20210203 p65458 앱 최초 실행시 모바일웹 상품 상세페이지에서 앱으로보기 실행시  앱 초기화 완료시 저장된 값으로 상품페이지 이동
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = UIColorFromRGB(0xFFFFFF);
    
    [self makeHeaderView];
    [self makeTabView];
    [self requestTabMenuList];
    
    CGFloat nToolbarHeight = [MainToolbar height];
    
    self.mainToolbar = [[MainToolbar alloc] initWithFrame:CGRectMake(0, kScreenBoundsHeight-nToolbarHeight, kScreenBoundsWidth, nToolbarHeight)];
    self.mainToolbar.delegate = self;
    [self.view addSubview:self.mainToolbar];
    
    _tabViewArray = [NSMutableArray array];

    if ([IUserDataManager getMobilePopup] && ![self compareSavedDayAndCurDayForMobilePopup]) {
        //[self performSelector:@selector(showPopupView) withObject:nil afterDelay:1.5];
        [self showPopupView];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
