//
//  IntroPopupController.m
//  AKMall
//
//  Created by seoyeon on 2018. 3. 21..
//  Copyright © 2018년 sjsofttech. All rights reserved.
//

#import "IntroPopupController.h"
#import "AKNewMainViewController.h"
#import "finger/finger.h"

#define kButtonSizeH    60

@interface IntroPopupController ()
{
    UIScrollView *_scrContents;
    UIImageView *_imgView;
    UIButton *_btnPermissionOK;

    // 푸시알림화면 관련
    UIScrollView *_scrPushContents;
    UIImageView *_imgPushView;
    UIButton *_btnOK;
    UIButton *_btnNext;

}
@end

@implementation IntroPopupController

#pragma mark - IBAction Method

- (void)didTouchPermissionOK:(UIButton *)aBtn
{
    [IUserDataManager setShowRights:YES];

    [_scrContents removeFromSuperview];
    [self initPushAlarmView];
}

- (void)didTouchPushNext:(UIButton *)aBtn
{
    [self gotoMainWithRequesthPushAlarm:NO];
}

- (void)didTouchPushOK:(UIButton *)aBtn
{
    [self gotoMainWithRequesthPushAlarm:YES];
}

#pragma mark - Private Func

- (void)gotoMainWithRequesthPushAlarm:(BOOL)isOn {

    [IUserDataManager setDidTouchPushAlarmDate:[NSDate date]];
    [IUserDataManager setFingerPushShow:YES];
    [[finger sharedData] setEnable:isOn :^(NSString *posts, NSError *error) {
        if(!error) {
            NSLog(@"finger Push State : %@",isOn?@"동의":@"거부");
        }
    }];
    [[finger sharedData] requestSetAdPushEnable:isOn :^(NSString *posts, NSError *error) {
        if(!error) {
            NSLog(@"finger Ad Push State : %@",isOn?@"동의":@"거부");
        }
    }];
    
    [AKNetworkUtil requestPushAlarmWithSuccess:isOn success:^(NSDictionary * _Nullable response) {
        [AKCommonUtil showAlertWithMsg:[NSString stringWithFormat:@"[AK몰] 마케팅(광고성) 앱 알림 수신 %@가 %@ 정상적으로 처리 되었습니다.", isOn ? @"동의" : @"거부", [AKCommonUtil getTodayString:@"yyyy년 MM월 dd일"]]];
    } failure:^(NSError * _Nonnull error) {
    }];
    
    AKNewMainViewController *controller = [[AKNewMainViewController alloc] init];
    [self.navigationController setViewControllers:@[controller] animated:YES];

}

#pragma mark - init

- (void)initRightView
{
    _scrContents = ({
        UIScrollView *scr = UIScrollView.new;
        scr.showsVerticalScrollIndicator = NO;
        scr.showsHorizontalScrollIndicator = NO;
        [self.view addSubview:scr];
        scr;
    });
    
    _imgView = ({
        UIImageView *img = UIImageView.new;
        img.contentMode = UIViewContentModeScaleAspectFill;
        img.image = [UIImage imageNamed:@"pop_permission"];
        [_scrContents addSubview:img];
        img;
    });
    
    _btnPermissionOK = ({
        UIButton *btn = UIButton.new;
//        [btn setImage:[UIImage imageNamed:@"pop_permission_btn"] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:@"pop_permission_btn"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(didTouchPermissionOK:) forControlEvents:UIControlEventTouchUpInside];
        btn.imageView.contentMode = UIViewContentModeScaleAspectFill;
        btn.subviews.firstObject.contentMode = UIViewContentModeScaleAspectFill;

        [self.view addSubview:btn];
        btn;
    });
    
    CGFloat yPos = START_Y;
    CGFloat nButtonHeight = kButtonSizeH;
    CGFloat nImageHeight = kScreenBoundsWidth * 2.21333;

    _btnPermissionOK.frame = CGRectMake(0, self.view.frame.size.height - nButtonHeight - [CommonFunc safeBottomY], kScreenBoundsWidth, nButtonHeight);
    _scrContents.frame = CGRectMake(0, yPos, kScreenBoundsWidth, kScreenBoundsHeight-nButtonHeight-yPos);
    _imgView.frame = CGRectMake(0, 0, kScreenBoundsWidth, nImageHeight);
    [_scrContents setContentSize:CGSizeMake(kScreenBoundsWidth, nImageHeight + [CommonFunc safeBottomY])];
}

- (void)initPushAlarmView {

    _scrPushContents = ({
        UIScrollView *scr = UIScrollView.new;
        scr.showsVerticalScrollIndicator = NO;
        scr.showsHorizontalScrollIndicator = NO;
        scr.scrollEnabled = NO;
        [self.view addSubview:scr];
        scr;
    });

    _imgPushView = ({
        UIImageView *img = UIImageView.new;
        img.contentMode = UIViewContentModeScaleAspectFit;
        img.image = [UIImage imageNamed:@"pop_apppush"];
        [_scrPushContents addSubview:img];
        img;

    });

    _btnNext = ({
        UIButton *btn = UIButton.new;
        [btn addTarget:self action:@selector(didTouchPushNext:) forControlEvents:UIControlEventTouchUpInside];

        [btn setBackgroundImage:[UIImage imageNamed:@"pop_apppush_btn_n"] forState:UIControlStateNormal];
        btn.imageView.contentMode = UIViewContentModeScaleAspectFill;
        btn.subviews.firstObject.contentMode = UIViewContentModeScaleAspectFill;
        [self.view addSubview:btn];
        btn;
    });

    _btnOK = ({
        UIButton *btn = UIButton.new;
        [btn addTarget:self action:@selector(didTouchPushOK:) forControlEvents:UIControlEventTouchUpInside];
        [btn setBackgroundImage:[UIImage imageNamed:@"pop_apppush_btn_y"] forState:UIControlStateNormal];
        btn.imageView.contentMode = UIViewContentModeScaleAspectFill;
        btn.subviews.firstObject.contentMode = UIViewContentModeScaleAspectFill;
        [self.view addSubview:btn];
        btn;
    });


    CGFloat yPos = START_Y;
    CGFloat nButtonHeight = kButtonSizeH;
    CGFloat nImageHeight = kScreenBoundsHeight - nButtonHeight;
    CGFloat nNextBtnWidth = kScreenBoundsWidth * 0.31466;

    _btnNext.frame = CGRectMake(0, self.view.frame.size.height - nButtonHeight - [CommonFunc safeBottomY], nNextBtnWidth, nButtonHeight);
    _btnOK.frame = CGRectMake(CGRectGetMaxX(_btnNext.frame), CGRectGetMinY(_btnNext.frame),
                              kScreenBoundsWidth - CGRectGetWidth(_btnNext.frame) , nButtonHeight);
    _scrPushContents.frame = CGRectMake(0, yPos, kScreenBoundsWidth, kScreenBoundsHeight-nButtonHeight-yPos);
    _imgPushView.frame = CGRectMake(0, 0, kScreenBoundsWidth, nImageHeight);
    [_scrPushContents setContentSize:CGSizeMake(kScreenBoundsWidth, nImageHeight + [CommonFunc safeBottomY])];
    _scrPushContents.contentOffset = CGPointMake(0, (_scrPushContents.contentSize.height-_scrPushContents.frame.size.height)/2);
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //CGFloat yPos = START_Y;
    
    [self.navigationController setNavigationBarHidden:YES];
    self.view.backgroundColor = UIColorFromRGB(0xFFFFFF);

    if ([self.type isEqualToString:@"rights"]) {
        [self initRightView];
    }
    else if ([self.type isEqualToString:@"push"]) {
        [self initPushAlarmView];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
