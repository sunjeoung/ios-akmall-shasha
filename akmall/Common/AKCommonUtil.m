//
//  AKCommonUtil.m
//  akmall
//
//  Created by KimJinoug on 2016. 10. 20..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <SafariServices/SafariServices.h>
#import "AKCommonUtil.h"
#import "AKOutWebView.h"
#import "AKPopupWebView.h"
#import "AKFullPopupView.h"
#import "KeychainItemWrapper.h"
#import "AKNewMainViewController.h"
#import "AKSubWebView.h"
#import <Contacts/Contacts.h>

#import "SubViewController.h"

@implementation AKCommonUtil

// 현재 보여지고 있는 ViewController 리턴
+ (AKBaseViewController *)getVisibleController {
    UINavigationController *nav = (UINavigationController *)appDelegate.window.rootViewController;
    return (AKBaseViewController *)nav.viewControllers.lastObject;
}

#pragma mark - Select Popup

// select 팝업 호출
+ (void)showSelect:(UIView *)parent delegate:(id<AKSelectViewDelegate>)delegate dataList:(NSArray *)dataList valueList:(NSArray *)valueList selectedIndex:(int)index {
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    AKSelectView *view = [[[NSBundle mainBundle] loadNibNamed:@"AKSelectView" owner:parent options:nil] objectAtIndex:0];
    
    view.selectedIndex = index;
    view.dataList = dataList;
    view.valueList = valueList;
    view.delegate = delegate;
    [view reloadData];
    [parent addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(parent).with.insets(padding);
    }];
}

#pragma mark - Popup WebView

// 공유하기 웹뷰 팝업 호출
+ (void)showSnsShare:(UIView *)parent url:(NSString *)url {
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    AKPopupWebView *view = [[[NSBundle mainBundle] loadNibNamed:@"AKPopupWebView" owner:parent options:nil] objectAtIndex:0];
    
    [view loadUrl:url];
    [view updateHeight:268];
    [parent addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(parent).with.insets(padding);
    }];
}

// 웹뷰 팝업 호출
+ (void)showPopupWebView:(UIView *)parent url:(NSString *)url {
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    AKPopupWebView *view = [[[NSBundle mainBundle] loadNibNamed:@"AKPopupWebView" owner:parent options:nil] objectAtIndex:0];
    
    [view loadUrl:url];
    [parent addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(parent).with.insets(padding);
    }];
}

#pragma mark - Full Popup

// Full 팝업 호출
+ (void)showFullPopup:(UIView *)parent data:(NSDictionary *)data {
    UIEdgeInsets padding = UIEdgeInsetsMake(20, 0, 0, 0);
    AKFullPopupView *view = [[[NSBundle mainBundle] loadNibNamed:@"AKFullPopupView" owner:parent options:nil] objectAtIndex:0];
    
    view.data = data;
    [view loadImage];
    [parent addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(parent).with.insets(padding);
    }];
}

#pragma mark - Version Popup

+ (void)showVersionPopup:(UIView *)parent delegate:(id<AKVersionPopupViewDelegate>)delegate type:(NSString *)type {
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    AKVersionPopupView *view = [[[NSBundle mainBundle] loadNibNamed:@"AKVersionPopupView" owner:parent options:nil] objectAtIndex:0];
    
    view.delegate = delegate;
    view.popupType = type;
    [view reloadPopupView];
    [parent addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(parent).with.insets(padding);
    }];
}

#pragma mark - History Webview

+ (void)showCategoryView:(UIView *)parent {

    CGFloat xPos = -1*kScreenBoundsWidth;
    CGFloat yPos = START_Y;
    
    AKSubWebView *category = [[AKSubWebView alloc] initWithFrame:CGRectMake(xPos, yPos, kScreenBoundsWidth, kScreenBoundsHeight-yPos)];
    category.webViewType = AKSubWebViewTypeCategoryMenu;
    [parent addSubview:category];
    [category requestUrl:[NSString stringWithFormat:@"%@%@", BASE_URL, URL_CATEGORY]];
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = category.frame;
        frame.origin.x = 0;
        category.frame = frame;
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - image from color

// 컬러를 이미지로 변환
+ (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark - UIButton Border

// View에 border 추가
+ (void)setBorder:(UIView *)btn width:(CGFloat)width color:(UIColor *)color {
    btn.layer.borderWidth = width;
    btn.layer.borderColor = color.CGColor;
}

#pragma mark - Currency

// 숫자를 원화로 표시
+ (NSString *)getCurrencyFormat:(int)number {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    NSString *numberAsString = @"";
    
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setCurrencySymbol:@""];
    numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithInt:number]];
    
    return numberAsString;
}

#pragma mark - Date

+ (NSString *)getDateString:(NSDate *)date format:(NSString *)format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:format];
    
    return [formatter stringFromDate:date];
}

+ (NSString *)getTodayString:(NSString *)format {
    return [self getDateString:[NSDate date] format:format];
}

#pragma mark - Alert

// 메시지, 확인 버튼 알럿
+ (void)showAlertWithMsg:(NSString *)msg {
    [self showAlert:nil msg:msg];
}

// 타이틀, 메시지, 확인 버튼 알럿
+ (void)showAlert:(NSString *)title msg:(NSString *)msg {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title ? title : @"터치 AK" message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:confirm];
    [[self getVisibleController] presentViewController:alert animated:YES completion:nil];
}

// 타이틀, 메시지, 확인, 취소 버튼 알럿
+ (void)showAlert:(NSString *)title msg:(NSString *)msg cancelBlock:(void (^ __nullable)(UIAlertAction *action))cancelHandler confirmBlock:(void (^ __nullable)(UIAlertAction *action))confirmHandler {
    [self showAlert:title msg:msg cancelTitle:@"취소" cancelBlock:cancelHandler confirmTitle:@"확인" confirmBlock:confirmHandler];
}

// 타이틀, 메시지, 취소 버튼 알럿
+ (void)showAlert:(NSString *)title msg:(NSString *)msg cancelTitle:(NSString *)cancelTitle cancelBlock:(void (^ __nullable)(UIAlertAction *action))cancelHandler {
    [self showAlert:title msg:msg cancelTitle:cancelTitle cancelBlock:cancelHandler confirmTitle:nil confirmBlock:nil];
}

// 타이틀, 메시지, 확인, 취소 버튼 알럿
+ (void)showAlert:(NSString *)title msg:(NSString *)msg cancelTitle:(NSString *)cancelTitle cancelBlock:(void (^ __nullable)(UIAlertAction *action))cancelHandler confirmTitle:(NSString *)confirmTitle confirmBlock:(void (^ __nullable)(UIAlertAction *action))confirmHandler {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title ? title : @"터치 AK" message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    if (cancelTitle) {
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            cancelHandler(action);
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:cancel];
    }
    
    if (confirmTitle) {
        UIAlertAction *confirm = [UIAlertAction actionWithTitle:confirmTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            confirmHandler(action);
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:confirm];
    }
    
    [[self getVisibleController] presentViewController:alert animated:YES completion:nil];
}

#pragma mark - push webview

// 내부링크의 경우 전체 웹뷰, 외부링크의 경우 모달 웹뷰
//+ (void)pushWebViewController:(NSString *)url {
//    if (![url hasPrefix:@"http"] && [url hasPrefix:@"/"]) {
//        url = [self getRequestUrl:url];
//    }
//
//    if ([[AKCommonUtil getVisibleController] isKindOfClass:[AKWebViewController class]]) {
//        AKWebViewController *vc = (AKWebViewController *)[AKCommonUtil getVisibleController];
//
//        [vc loadUrl:url];
//    } else {
//        AKWebViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"AKWebViewController"];
//
//        vc.url = url;
//        [[self getVisibleController].navigationController pushViewController:vc animated:YES];
//    }
//}

+ (void)pushWebViewController:(NSString *)url {
    if (![url hasPrefix:@"http"] && [url hasPrefix:@"/"]) {
        url = [self getRequestUrl:url];
    }
    
    if ([[AKCommonUtil getVisibleController] isKindOfClass:[SubViewController class]]) {
        SubViewController *vc = (SubViewController *)[AKCommonUtil getVisibleController];
        [vc loadUrl:url];
    }
    else {
        //20210202 add p65458 https://bid.g.doubleclick.net/xbbe/pixel?d=KAE 하단 툴바 홈 버튼 클릭시 간헐적으로 해당 url로 화면 상세페이지로 이동 이슈 -> 해당 도메인 포함시 화면 이동 하지 않음
        NSString* tempUrl = @"bid.g.doubleclick.net";
        if ([url containsString:tempUrl]) {
            return;
        }else{
            //if (![[appDelegate returnUrl]  containsString:@"GoodsDetail.do"]) {        //add 20210203 p65458 앱 최초 실행시 모바일웹 상품 상세페이지에서 앱으로보기 실행시  앱 초기화 완료시 저장된 값으로 상품페이지 이동하도록 이전 상품 상세 페이지 이이동 하지 않도록 추가 -> 필요 없어서 블럭 
                SubViewController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"SubViewController"];
                controller.url = url;
                [[self getVisibleController].navigationController pushViewController:controller animated:YES];
            //}
        }
    }
}

// target=_blank의 경우 외부용 웹뷰로 호출
+ (void)openOutWebView:(NSString *)url {
    AKBaseViewController *vc = [self getVisibleController];
    AKOutWebView *view = [[[NSBundle mainBundle] loadNibNamed:@"AKOutWebView" owner:vc options:nil] objectAtIndex:0];
    
    [view loadUrl:url];
    [vc.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(vc.view.mas_left);
        make.right.equalTo(vc.view.mas_right);
        make.top.equalTo(vc.mas_topLayoutGuideBottom);
        make.bottom.equalTo(vc.view.mas_bottom);
    }];
    [view openAnimation:vc.view];
}

// 플레이어용 웹뷰 호출
+ (void)openPlayerWebView:(NSString *)url {
    AKBaseViewController *vc = [self getVisibleController];
    AKOutWebView *view = [[[NSBundle mainBundle] loadNibNamed:@"AKPlayerWebView" owner:vc options:nil] objectAtIndex:0];
    
    [view loadUrl:url];
    [vc.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(vc.view.mas_left);
        make.right.equalTo(vc.view.mas_right);
        make.top.equalTo(vc.mas_topLayoutGuideBottom);
        make.bottom.equalTo(vc.view.mas_bottom);
    }];
    [view openAnimation:vc.view];
}

// 웹브라우저 및 앱 호출
+ (void)openWeb:(NSString *)url {
    if ([SFSafariViewController class]) {
        SFSafariViewController *sf = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:url]];
        [[self getVisibleController] presentViewController:sf animated:YES completion:nil];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}

+ (void)openURL:(NSString *)url {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

#pragma mark - webview scheme

// url을 dictionary 형태로 변환
+ (NSDictionary *)parseScheme:(NSString *)url {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    NSRange range = [url rangeOfString:@"://"];
    
    if (range.length > 0) {
        NSString *remain = [url substringFromIndex:range.location + range.length];
        NSString *query = [NSURL URLWithString:url].query;
        NSArray *remains = [remain componentsSeparatedByString:@"?"];
        
        dic[@"scheme"] = [url substringToIndex:range.location];
        dic[@"command"] = remains[0];
        
        if (query && ([query isEqualToString:@""] == NO)) {
#if 1
            NSString *dec = [query stringByRemovingPercentEncoding];
            
            dic[@"param"] = [NSJSONSerialization JSONObjectWithData:[dec dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];
#else
            NSString *param = remains[1];
            NSArray *params = [param componentsSeparatedByString:@"&"];
            
            for (NSString *p in params) {
                NSArray *keys = [p componentsSeparatedByString:@"="];
                
                if (keys.count > 1) {
                    dic[keys[0]] = keys[1];
                }
            }
#endif
        }
    }
    
    return dic;
}

#pragma mark - URL

// 기본 url 리턴
+ (NSString *)getBaseUrl {
    return BASE_URL;
}

// 기본 url에 추가 url 추가하여 리턴
+ (NSString *)getRequestUrl:(NSString *)url {
    if (url) {
        return [NSString stringWithFormat:@"%@%@", [self getBaseUrl], url];
    } else {
        return [self getBaseUrl];
    }
}

#pragma mark - check nil

// 스트링이 nil인 경우 빈 스트링으로 리턴
+ (NSString *)checkNil:(NSString *)str {
    if ([str isKindOfClass:[NSNull class]]) {
        return @"";
    }
    
    if (str) {
        return str;
    }
    
    return @"";
}

#pragma mark - check url

// url에 특정 스트링이 포함되어 있는지 체크(파라미터에 포함된 경우 제외)
+ (BOOL)containsUrlString:(NSString *)urlString checkString:(NSString *)checkString {
    NSArray *comp = [urlString componentsSeparatedByString:@"?"];
    
    return [comp[0] containsString:checkString];
}

// str에 목록에 있는 스트링이 있는지 체크
+ (BOOL)containsStringList:(NSArray *)stringList inString:(NSString *)str {
    for (NSString *s in stringList) {
        if ([str containsString:s]) {
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - Javascript

// 웹뷰 자바스크립트 호출(파라미터 url encoding 후 base64 encode)
+ (void)callJavascript:(WKWebView *)webView function:(NSString *)function object:(id)object {
    NSString *jsonStr = [AKCommonUtil getJsonStringFromObject:object];
    
    jsonStr = [self urlEncode:jsonStr];
    jsonStr = [NSString stringWithFormat:@"%@(\"%@\");", function, [self base64Encode:jsonStr]];
    [webView evaluateJavaScript:jsonStr completionHandler:nil];
}

// 웹뷰 자바스크립트 호출
+ (void)callJavascriptNoEncode:(WKWebView *)webView function:(NSString *)function object:(id)object {
    NSString *jsonStr = [AKCommonUtil getJsonStringFromObject:object];
    jsonStr = [NSString stringWithFormat:@"%@(\"%@\");", function, jsonStr];
    [webView evaluateJavaScript:jsonStr completionHandler:nil];
}

// target=_blank 체크하는 자바스크립트 추가
+ (void)checkTargetBlank:(WKWebView *)webView {
    NSString *JSInjection = @"javascript: var allLinks = document.getElementsByTagName('a'); if (allLinks) {var i;for (i=0; i<allLinks.length; i++) {var link = allLinks[i];var target = link.getAttribute('target'); if (target && target == '_blank') {link.setAttribute('target','_self');link.href = 'newtab:'+link.href;}}}";
    [webView evaluateJavaScript:JSInjection completionHandler:nil];
}

// callGoBack 자바스크립트 체크
+ (void)checkBack:(WKWebView *)webView {
    NSString *JSInjection = @"setTimeout(function(){if(location.origin.indexOf('akmall.com') == -1)location.href='akmall://callCheck?'+encodeURIComponent(JSON.stringify({'r' : 0}));},100);";
    [webView evaluateJavaScript:JSInjection completionHandler:nil];
}

// 타이틀 얻어오는 자바스크립트 추가
+ (void)getTitle:(WKWebView *)webView {
    NSString *JSInjection = @"javascript:setTimeout(function(){var title = document.getElementsByTagName('title')[0].text; location.href = 'akmall://getTitle?' + encodeURIComponent(JSON.stringify({'t' : title}));},100)";
    [webView evaluateJavaScript:JSInjection completionHandler:nil];
}

#pragma mark - JSON

// 객체를 json 스트링으로 변환
+ (NSString *)getJsonStringFromObject:(id)object {
    NSString *jsonStr = @"";
    
    if (object) {
        if ([object isKindOfClass:[NSString class]]) {
            jsonStr = (NSString *)object;
        } else {
            NSData *json = [NSJSONSerialization dataWithJSONObject:object options:0 error:nil];
            
            jsonStr = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        }
    }
    
    return jsonStr;
}

+ (id)getObjectFromJsonString:(NSString *)jsonStr {
    id ret = nil;
    
    if (jsonStr) {
        ret = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
    }
    
    return ret;
}

#pragma mark - url encode/decode

// url encode
+ (NSString *)urlEncode:(NSString *)str {
    return [str stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
}

// url decode
+ (NSString *)urlDecode:(NSString *)str {
    return [str stringByRemovingPercentEncoding];
}

#pragma mark - BASE64

// base64 encode
+ (NSString *)base64Encode:(NSString *)str {
    return [[str dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];
}

#pragma mark - Cookie

+ (NSString *)getCookieValue:(NSString *)key {
#ifdef PRIVATE_SERVER
    NSString *url = [[self getBaseUrl] substringFromIndex:@"http://".length];
#else
    NSString *url = @".akmall.com";
#endif
    NSString *value = nil;
    
    for (NSHTTPCookie *cookie in [NSHTTPCookieStorage sharedHTTPCookieStorage].cookies) {
        NSLog(@"cookie is %@", cookie);
        if ([cookie.properties[NSHTTPCookieName] isEqualToString:key] && [cookie.properties[NSHTTPCookieDomain] isEqualToString:url]) {
            value = cookie.properties[NSHTTPCookieValue];
            break;
        }
    }
    
    return value;
}

+ (NSString *)getCookieValue:(NSString *)key web:(WKWebView *)webView {
#ifdef PRIVATE_SERVER
    NSString *url = [[self getBaseUrl] substringFromIndex:@"http://".length];
#else
    NSString *url = @".akmall.com";
#endif
    
    __block NSString *resultString = nil;
    __block BOOL finished = NO;

    if (@available(iOS 11.0, *)) {
        [webView.configuration.websiteDataStore.httpCookieStore
        getAllCookies:^(NSArray<NSHTTPCookie *> *_Nonnull cookies) {
          for(NSHTTPCookie *cookie in cookies){
              if ([cookie.properties[NSHTTPCookieName] isEqualToString:key] && [cookie.properties[NSHTTPCookieDomain] isEqualToString:url]) {
                  resultString = cookie.properties[NSHTTPCookieValue];
                  finished = YES;
                  break;
              }
          }
            finished = YES;
        }];

        while (!finished)
        {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    }

    return resultString;
}

+ (void)setCookie:(NSString *)key value:(NSString *)value {
#ifdef PRIVATE_SERVER
    NSString *url = [[self getBaseUrl] substringFromIndex:@"http://".length];
#else
    NSString *url = @".akmall.com";
#endif
    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:@{NSHTTPCookieDomain: url, NSHTTPCookiePath: @"/", NSHTTPCookieName: key, NSHTTPCookieValue: value, NSHTTPCookieExpires: [NSDate dateWithTimeIntervalSinceNow:3600 * 24 * 365]}];
    
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
}

#pragma mark - UUID

+ (NSString *)getUUID {
    // initialize keychaing item for saving UUID.
    KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:@"UUID" accessGroup:nil];
    NSString *uuid = [wrapper objectForKey:(__bridge id)(kSecAttrAccount)];
    
    if (!uuid || uuid.length == 0) {
        // if there is not UUID in keychain, make UUID and save it.
        CFUUIDRef uuidRef = CFUUIDCreate(NULL);
        CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
        
        CFRelease(uuidRef);
        uuid = [NSString stringWithString:(__bridge NSString *) uuidStringRef];
        CFRelease(uuidStringRef);
        
        // save UUID in keychain
        [wrapper setObject:uuid forKey:(__bridge id)(kSecAttrAccount)];
    }
    
    return uuid;
}

#pragma mark - Go Home

+ (void)goHome:(NSString *)url {
    if ([[AKCommonUtil getVisibleController] isKindOfClass:[AKNewMainViewController class]]) {
        [(AKNewMainViewController *)[AKCommonUtil getVisibleController] refreshWeb:url];
    }
    else {
        AKNewMainViewController *vc = (AKNewMainViewController *)[AKCommonUtil getVisibleController].navigationController.viewControllers[0];
        [[AKCommonUtil getVisibleController].navigationController popToRootViewControllerAnimated:NO];
        // url 호출이 여러번 반복되어 약간 딜레이를 줌.
        [vc performSelector:@selector(refreshWeb:) withObject:url afterDelay:0.5];
    }
}

#pragma mark - Check WebView Error

// 웹뷰 로딩 실패 팝업 띄우는 조건 체크
+ (BOOL)checkWebViewError:(NSError *)error {
    if (!error) {
        return NO;
    }
    
    NSLog(@"checkWebViewError = %@", error);
    
    return [error.userInfo[NSURLErrorFailingURLStringErrorKey] hasPrefix:BASE_URL] && ![error.userInfo[NSURLErrorFailingURLStringErrorKey] containsString:@"/search/PowerLink.do"] && error.code != -999 && ((error.code >= 400 && error.code < 600) || error.code < 0);
}

#pragma mark - get device addrList
/*
// add 20210111 선물하기에서 메뉴에서 주소록 목록을 전달함
+ (void)getContactlist:(NSError *)error {
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];

    if( status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusRestricted)

    {

        NSLog(@"access denied");

    }

    else

    {

        //Create repository objects contacts

        CNContactStore *contactStore = [[CNContactStore alloc] init];

        

        NSArray *keys = [[NSArray alloc]initWithObjects:CNContactIdentifierKey, CNContactEmailAddressesKey, CNContactBirthdayKey, CNContactImageDataKey, CNContactPhoneNumbersKey, CNContactViewController.descriptorForRequiredKeys, nil];

        

        // Create a request object

        CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];

        request.predicate = nil;

        

        [contactStore enumerateContactsWithFetchRequest:request

                                                  error:nil

                                             usingBlock:^(CNContact* __nonnull contact, BOOL* __nonnull stop)

         {

             // Contact one each function block is executed whenever you get

             NSString *phoneNumber = @"";

             if( contact.phoneNumbers)

                 phoneNumber = [[[contact.phoneNumbers firstObject] value] stringValue];

             

             NSLog(@"phoneNumber = %@", phoneNumber);

             NSLog(@"givenName = %@", contact.givenName);

             NSLog(@"familyName = %@", contact.familyName);

             NSLog(@"email = %@", contact.emailAddresses);

             

             

             //[contactList addObject:contact];

         }];

        

        //[contactTableView reloadData];

    }
    */

+ (void)getContactlist {
        if ([CNContactStore class]) {
            //ios9 or later
            CNEntityType entityType = CNEntityTypeContacts;
            if( [CNContactStore authorizationStatusForEntityType:entityType] == CNAuthorizationStatusNotDetermined)
             {
                 CNContactStore * contactStore = [[CNContactStore alloc] init];
                 [contactStore requestAccessForEntityType:entityType completionHandler:^(BOOL granted, NSError * _Nullable error) {
                     if(granted){
                         [self getAllContact];
                         //[self openContact];//add 20210121 p65458
                     }
                 }];
             }
            else if( [CNContactStore authorizationStatusForEntityType:entityType]== CNAuthorizationStatusAuthorized)
            {
                [self getAllContact];
                //[self openContact];//add 20210121 p65458
            }
        }
    }


+ (void)getAllContact
    {
        if([CNContactStore class])
        {
            //iOS 9 or later
            NSError* contactError;
            NSMutableArray* contactArray = [[NSMutableArray alloc] init];
            CNContactStore* addressBook = [[CNContactStore alloc]init];
            [addressBook containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[addressBook.defaultContainerIdentifier]] error:&contactError];
            NSArray * keysToFetch =@[CNContactEmailAddressesKey, CNContactPhoneNumbersKey, CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPostalAddressesKey];
            CNContactFetchRequest * request = [[CNContactFetchRequest alloc]initWithKeysToFetch:keysToFetch];
            BOOL success = [addressBook enumerateContactsWithFetchRequest:request error:&contactError usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop){
                //[self parseContactWithContact:contact];
                    NSString * firstName =  contact.givenName;
                    NSString * lastName =  contact.familyName;
                    //NSString * phone = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
                    NSString * phone = [[contact.phoneNumbers[0] valueForKey:@"value"] valueForKey:@"digits"];//대표번호하나만 사용
                    //NSString * email = [contact.emailAddresses valueForKey:@"value"];
                    //NSArray * addrArr = [self parseAddressWithContact:contact];
                    NSMutableDictionary* contactMutableDictionary = [NSMutableDictionary dictionaryWithCapacity:1];
                    //[contactMutableDictionary setObject:firstName forKey:@"firstName"];
                    //[contactMutableDictionary setObject:lastName forKey:@"lastName"];
                    [contactMutableDictionary setObject:[lastName stringByAppendingString:firstName] forKey:@"fullName"];
                    
                    [contactMutableDictionary setObject:phone forKey:@"phone"];
                    //[contactMutableDictionary setObject:addrArr forKey:@"addrArr"];
                
                    NSDictionary* contactDictionary = [NSDictionary dictionaryWithObject:contactMutableDictionary forKey:@"contact"];
                
                
                    //[contactArray addObject:contactDictionary];
                    [contactArray addObject:contactMutableDictionary];
                    
                    [self parseContactWithContactArray:contactArray];

                    
                  
            }];
            
        }
    }

//20210112 주소록 가져오기
/*
+ (void)parseContactWithContact1 :(CNContact* )contact
{
    NSString * firstName =  contact.givenName;
    NSString * lastName =  contact.familyName;
    NSString * phone = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
    NSString * email = [contact.emailAddresses valueForKey:@"value"];
    NSArray * addrArr = [self parseAddressWithContact:contact];

    NSError *error = nil;
    NSError* jsonSerializationError = nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:addrArr options:0 error:&error];

    if (error) {
        NSLog(@"Failed to encode JSON with object: %@", addrArr);
        return;
    }
    
    if (!jsonSerializationError){
        NSString* serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"serialized json: %@", serJSON);
    }else{
        NSLog(@"JSON Encoding failed: %@", [jsonSerializationError localizedDescription]);
    }
    
    NSString* server_url = @"";
    //데이터 전송
    NSURL* messageURL = [NSURL URLWithString:server_url];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:messageURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPBody:jsonData];
  
    NSMutableURLRequest *requst = [[NSMutableURLRequest alloc] init];

    NSURLConnection* jsonConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (jsonConnection){
        NSMutableData* connData = [[NSMutableData alloc] init];
        //[self setConnectionData:connData];
        
    }else{
    
        NSLog(@"Connection failed");
    }
    
    
}
 */

+ (void)parseContactWithContact: (CNContact *)contact
{
    NSMutableArray* contactArray = [[NSMutableArray alloc] init];
    NSString * firstName =  contact.givenName;
    NSString * lastName =  contact.familyName;
    //NSString * phone = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
    NSString * phone = [[contact.phoneNumbers[0] valueForKey:@"value"] valueForKey:@"digits"];//대표번호하나만 사용

 
    //NSString * email = [contact.emailAddresses valueForKey:@"value"];
    //NSArray * addrArr = [self parseAddressWithContact:contact];
    NSMutableDictionary* contactMutableDictionary = [NSMutableDictionary dictionaryWithCapacity:1];
    //[contactMutableDictionary setObject:firstName forKey:@"firstName"];
    //[contactMutableDictionary setObject:lastName forKey:@"lastName"];
    [contactMutableDictionary setObject:[lastName stringByAppendingString:firstName] forKey:@"fullName"];

    [contactMutableDictionary setObject:phone forKey:@"phone"];
    //[contactMutableDictionary setObject:addrArr forKey:@"addrArr"];

    NSDictionary* contactDictionary = [NSDictionary dictionaryWithObject:contactMutableDictionary forKey:@"contact"];


    //[contactArray addObject:contactDictionary];
    [contactArray addObject:contactMutableDictionary];

    [self parseContactWithContactArray:contactArray];

}


//20210112 주소록 전송
+ (void)parseContactWithContactArray :(NSMutableArray* )contactArray
{
    NSString* serJSON = nil;
    NSError *error = nil;
    NSError* jsonSerializationError = nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:contactArray options:0 error:&error];

    if (error) {
        NSLog(@"Failed to encode JSON with object: %@", contactArray);
        return;
    }
    
    if (!jsonSerializationError){
        serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"serialized json: %@", serJSON);
    }else{
        NSLog(@"JSON Encoding failed: %@", [jsonSerializationError localizedDescription]);
    }
    
    //NSString* server_url = [BASE_URL stringByAppendingString:@"contact/getContact.do"];//@"";
    //데이터 전송
    //NSURL* messageURL = [NSURL URLWithString:server_url];
    
    //NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:messageURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc]init];

    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[request setValue:[NSString stringWithFormat:@"%d", [serJSON length]] forHTTPHeaderField:@"Content-Length"];
    
    //[request setHTTPBody:jsonData];
    
    
    NSMutableURLRequest *requst = [[NSMutableURLRequest alloc] init];

    //NSString *param1 = @"contacts";

    //post 데이터 만들기
    NSString *post = [NSString stringWithFormat:@"contacts=%@",serJSON]; //보낼 내용을 작성합니다.
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    //Request 요청 설정하기

    [request setURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:@"contact/getContact.do"]]];
    [request setHTTPMethod:@"POST"]; // POST 방식으로 보냄
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"Mozilla/4.0 (compatible;)" forHTTPHeaderField:@"User-Agent"];
    [request setHTTPBody:postData];
    [request setTimeoutInterval:30.0]; // 60초 후 타임아
    
    // 커넥션 에러를 다룰 객체를 생성
    NSError *conError = nil; // NSURLConnection 객체를 이용해 동기적으로 보냄 (Response객체는 사용하지 않음) // 돌아온 값은 NSData형으로 받는다
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:Nil error:&error];
    if(data == NULL){ // 통신 실패 !
        NSLog(@"통신 실패 ! : %@",conError);
    } else{ // 통신 성공 // 받아온 정보가 스트링인 경우
        NSString *returnStr = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]; NSLog(@"Return String : %@",returnStr);
    }
/*
    NSURLConnection* jsonConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (jsonConnection){
        //NSMutableData* connData = [[NSMutableData alloc] init];
        //[self setConnectionData:connData];
        NSLog(@"Connection suecssed");
    }else{
    
        NSLog(@"Connection failed");
    }
    */
    
}

+ (NSMutableArray *)parseAddressWithContact: (CNContact *)contact
{
    NSMutableArray * addrArr = [[NSMutableArray alloc]init];
    CNPostalAddressFormatter * formatter = [[CNPostalAddressFormatter alloc]init];
    NSArray * addresses = (NSArray*)[contact.postalAddresses valueForKey:@"value"];
   

    //NBSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithCapacity:addresses.count];
    if (addresses.count > 0) {
        for (CNPostalAddress* address in addresses) {
            [addrArr addObject:[formatter stringFromPostalAddress:address]];

            /*
            NSString * firstName =  contact.givenName;
            NSString * lastName =  contact.familyName;
            
            NSString * phone = [[contact.phoneNumbers valueForKey:@"value"]
            [dict setObject:[NSNumber numberWithInt:42] forKey:@"A cool number"];
            [dict setObject:[NSNumber numberWithInt:42] forKey:@"A cool number"];
            [dict setObject:[NSNumber numberWithInt:42] forKey:@"A cool number"];
              */

        }
    }

    return addrArr;
}


@end
