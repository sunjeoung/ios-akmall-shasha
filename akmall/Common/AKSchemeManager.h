//
//  AKSchemeManager.h
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 18..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@protocol AKSchemeManagerDelegate <NSObject>

@optional
- (void)closePopup:(NSString *)type;
- (void)updateHeight:(NSDictionary *)dic;
- (void)getProdList:(NSDictionary *)dic;
- (void)changeTab:(NSDictionary *)tab;
- (void)callJavascriptAll:(NSString *)function;
- (void)reloadCategory:(NSString *)url;
- (void)showSelectPopup:(NSDictionary *)dic;
- (void)changeParam:(NSDictionary *)param;
- (void)showNative;
- (void)hideNative;
- (void)drawList;
- (void)callLayerFilter:(NSString *)param;
- (void)sendFilter:(NSString *)param;
- (void)openWebView:(NSString *)tp;
- (void)scrollTop;
- (void)callWishPopup:(NSString *)goodsId;
- (void)sendMainPopup:(NSArray *)list;

- (void)showGNB;
- (void)hideFloat;
- (void)showFloat;
- (void)readPositionX:(int)x;
- (void)callShareLayer:(NSString *)goodsId;
- (void)callpWishInpt:(NSString *)goodsId;
- (void)callShoopingCart:(NSString *)goodsId;
- (void)closeAndOpenWebview:(NSString *)url;
- (void)showBack:(BOOL)isShow;
- (void)callJavaScriptFromWeb:(NSString *)aScript;
- (void)openWebViewUrl:(NSString *)aUrl;
- (void)openBrowser:(NSString *)aUrl;
- (void)closeSubBrowser;

- (void)moveMainTabIndex:(NSString *)nIndex;
- (void)toggleTabbar:(BOOL)isHidden;
- (void)toggleHeader:(BOOL)isHidden;

@end

@interface AKSchemeManager : NSObject

@property (assign) id<AKSchemeManagerDelegate> delegate;

- (BOOL)processScheme:(WKWebView *)webView url:(NSString *)url;
- (BOOL)processScheme:(WKWebView *)webView url:(NSString *)url categoryType:(int)categoryType;


- (void)getAllContact;
- (void)openContact;
@end
