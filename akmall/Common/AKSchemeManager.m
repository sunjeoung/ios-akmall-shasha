//
//  AKSchemeManager.m
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 18..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKSchemeManager.h"
#import "AKCommonUtil.h"
#import "AKProperty.h"
#import "SubViewController.h"
#import "AKNewMainViewController.h"
#import "AKSpeechViewController.h"
#import <AdSupport/AdSupport.h>
#import "finger/finger.h"
#import <AdBrix/AdBrix.h>
#import <IgaworksCore/IgaworksCore.h>
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>

#import "NSString+Customs.h"

@implementation AKSchemeManager

- (BOOL)processScheme:(WKWebView *)webView url:(NSString *)url {
    return [self processScheme:webView url:url categoryType:CATEGORY_NONE];
}

- (BOOL)processScheme:(WKWebView *)webView url:(NSString *)url categoryType:(int)categoryType {
    NSLog(@"processScheme url = %@", url);
    
    NSString *scheme = [NSURL URLWithString:url].scheme;
    scheme = [scheme lowercaseString];
    if ([scheme isEqualToString:@"akmall"]) {
        [self doAkMallScheme:webView url:url categoryType:categoryType];
        return NO;
    }
    else if ([scheme isEqualToString:@"newtab"]) {
        NSString *newtabUrl = [url substringFromIndex:7];
        
        if ([newtabUrl containsString:@"akmembers.com"]) {
            [AKCommonUtil openWeb:newtabUrl];
        }
        else {
            [AKCommonUtil openOutWebView:newtabUrl];
        }
        return NO;
    }
    else if ([scheme isEqualToString:@"kakaolink"]) {
        BOOL isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"kakaolink://"]];
        if (isInstalled) {
            [AKCommonUtil openURL:url];
        } else {
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"https://itunes.apple.com/app/id362057947"]];
        }
        
        return NO;
    }
    else if ([scheme isEqualToString:@"storylink"]) {
        BOOL isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"storylink://"]];
        if (isInstalled) {
            [AKCommonUtil openURL:url];
        } else {
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"https://itunes.apple.com/app/id486244601"]];
        }
        
        return NO;
    }
    else if ([scheme isEqualToString:@"newplay"]) {
        // av play 시 플레이 웹뷰를 이용
        [AKCommonUtil openPlayerWebView:[url substringFromIndex:8]];
        return NO;
    }
    else if ([scheme isEqualToString:@"about"]) {
        return YES;
    }
    else if (([scheme isEqualToString:@"http"] == NO) && ([scheme isEqualToString:@"https"] == NO)) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        return NO;
    }
    else {
        return YES;
        //[self doOtherUrl:webView url:url categoryType:categoryType];
    }
}

//- (BOOL)doOtherUrl:(id)webView url:(NSString *)url categoryType:(int)categoryType
//{
//    if ([AKCommonUtil containsUrlString:url checkString:@"/main/Main.do"]) {
//        if ([[AKCommonUtil getVisibleController] isKindOfClass:[AKNewMainViewController class]]) {
//            return YES;
//        }
//        else {
//            // 홈 클릭으로 메인 첫 화면으로 이동
//            [AKCommonUtil goHome:url];
//
//            return NO;
//        }
//    }
//    else if (![[AKCommonUtil getVisibleController] isKindOfClass:[SubViewController class]] &&
//               [AKCommonUtil containsUrlString:url checkString:@"/order/ShoppingCart.do"]) {
//        // ShopingCart.do의 경우 dummy=I가 붙은 경우 pass
//        if (![url containsString:@"dummy=I"]) {
//            [AKCommonUtil pushWebViewController:url];
//
//            return NO;
//        }
//    }
//    else if ([AKCommonUtil containsUrlString:url checkString:URL_LOGIN]) {
//        if ([url containsString:@"token"]) {
//           if (![[AKCommonUtil getVisibleController] isKindOfClass:[SubViewController class]]) {
//               [AKCommonUtil pushWebViewController:url];
//
//               return NO;
//           }
//        }
//        else {
//           if ([url containsString:@"?"]) {
//               url = [NSString stringWithFormat:@"%@&token=%@", url, [AKProperty getDeviceToken]];
//           }
//           else {
//               url = [NSString stringWithFormat:@"%@?token=%@", url, [AKProperty getDeviceToken]];
//           }
//           [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
//
//           return NO;
//        }
//    }
//    else if (categoryType == CATEGORY_MAIN) {
//       // 메인 화면에서 통과해야 할 url 정의
//       // 카카오 관련 공통 웹뷰로 이동하여 실행 시 오류 발생하여 메인웹뷰에서 처리하도록 수정
//       // 앱스토어 이동 시 다시 앱으로 돌아왔을 때 흰 화면이 되어서 메인웹뷰에서 처리하도록 수정
//        NSArray *passUrl = @[@"about:blank", @"accounts.google.com", @"content.googleapis.com", @"kakaolink://", @"storylink://", @"itunes.apple.com", @"youtube.com"];
//
//        if (![AKCommonUtil containsStringList:passUrl inString:url]) {
//            // 메인 화면에서는 무조건 새로운 웹화면으로 이동해서 시작
//            [AKCommonUtil pushWebViewController:url];
//        }
//        else if ([url rangeOfString:@"youtube.com"].location != NSNotFound) {
//            return YES;
//        }
//
//        return NO;
//    }
//
//    return YES;
//}

- (void)doAkMallScheme:(WKWebView *)webView url:(NSString *)url categoryType:(int)categoryType
{
    //NSString *jsonStr = @"";
                //jsonStr = [AKCommonUtil urlEncode:jsonStr];
                //[AKCommonUtil getContactlist:jsonStr];//20210113 주소록 가져오기 todo
    //[self openContact];//20210113 연동 가져오기 todo

    NSString *host = [NSURL URLWithString:url].host;
    if ([host isEqualToString:@"appinterface"]) {
        NSString *sQuery = [NSURL URLWithString:url].query;
        NSDictionary *dictParams = [sQuery parseURLParams];
        if ([dictParams[@"func"] isEqualToString:@"newAlarm"]) {
            [appDelegate setIsNewAlarm:[dictParams[@"params"] isEqualToString:@"Y"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNewAlarmNotification object:nil];
        }
        else if ([dictParams[@"func"] isEqualToString:@"cartCount"]) {
            [appDelegate setNCartCount:[dictParams[@"params"] intValue]];
            [[NSNotificationCenter defaultCenter] postNotificationName:kCartCountNotification object:nil];
        }
        else if ([dictParams[@"func"] isEqualToString:@"historyImg"]) {
            
            NSString *sImgurl = dictParams[@"params"];
            if ([sImgurl isEqualToString:@""] == NO) {
                sImgurl = [NSString stringWithFormat:@"http:%@", sImgurl];
            }
            [appDelegate setImgHistoryUrl:sImgurl];
            [[NSNotificationCenter defaultCenter] postNotificationName:kDispplayHistoryImageNotification object:sImgurl];
        }
        else if ([dictParams[@"func"] isEqualToString:@"changeMainTab"]) {
            NSString *sIndex = dictParams[@"params"];
            if(sIndex && ([sIndex isEqualToString:@""] == NO)) {
                sIndex = [sIndex stringByReplacingOccurrencesOfString:@"'" withString:@""];
                if (_delegate && [_delegate respondsToSelector:@selector(moveMainTabIndex:)]) {
                    [_delegate moveMainTabIndex:sIndex];
                }
            }
        }
        else if ([dictParams[@"func"] isEqualToString:@"toggleTabbar"]) {
            NSString *sHiddenYN = dictParams[@"params"];
            if (_delegate && [_delegate respondsToSelector:@selector(toggleTabbar:)]) {
                [_delegate toggleTabbar:[sHiddenYN isEqualToString:@"N"]];
            }
        }
        else if ([dictParams[@"func"] isEqualToString:@"toggleHeader"]) {
            NSString *sHiddenYN = dictParams[@"params"];
            if (_delegate && [_delegate respondsToSelector:@selector(toggleHeader:)]) {
                [_delegate toggleHeader:[sHiddenYN isEqualToString:@"N"]];
            }
        }
        //adbrix
        //categoryView
        else if ([dictParams[@"func"] isEqualToString:@"categoryView"]) {
            @try {
                NSString *sParams = [NSString stringWithFormat:@"?%@", dictParams[@"params"]];
            
                NSString * escapeParams = [[AKCommonUtil urlDecode:sParams] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];

                
                NSString *eQuery = [NSURL URLWithString:escapeParams].query;
                NSDictionary *finalParams = [eQuery parseURLParams];
                
                NSLog(@"categoryView sParams = %@, | %@", eQuery, finalParams);
                NSLog(@"categoryView decodeQuery = %@, cat1 = %@, cat2 = %@, cat3 = %@, cat4 = %@", finalParams ,
                  [AKCommonUtil urlDecode:finalParams[@"cat1"]],
                  [AKCommonUtil urlDecode:finalParams[@"cat2"]],
                  [AKCommonUtil urlDecode:finalParams[@"cat3"]],
                  [AKCommonUtil urlDecode:finalParams[@"cat4"]]);
                NSString *cat1 = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"cat1"]]]];
                NSString *cat2 = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"cat2"]]]];
                NSString *cat3 = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"cat3"]]]];
                NSString *cat4 = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"cat4"]]]];
                [AdBrix commerceCategoryView:[AdBrixCommerceProductCategoryModel create:cat1 category2:cat2 category3:cat3 category4:cat4 ]] ;
                
            }@catch (NSException *exception) {
                NSLog(@"categoryView error");
            }
            @finally {
            }
        } //categoryView
        //productView
        else if ([dictParams[@"func"] isEqualToString:@"productView"]) {
            @try {
                NSString *sParams = [NSString stringWithFormat:@"?%@", dictParams[@"params"]];
                NSString * escapeParams = [[AKCommonUtil urlDecode:sParams] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
                NSDictionary *finalParams = [[NSURL URLWithString:escapeParams].query parseURLParams];
                
                NSString *goods_name = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_name"]]]];
                NSString *goods_id = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_id"]]]];
                NSString *goods_price = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_price"]]]];
                NSString *goods_final_price = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_final_price"]]]];
                NSLog(@"adbrix productview %@ | %@ | %@ | %@" , goods_name,goods_id,goods_price,goods_final_price);
                [AdBrix commerceProductView:
                 [AdBrix createCommerceProductModel:goods_id productName:goods_name price:[goods_price integerValue]                                        discount:[goods_final_price integerValue] quantity:1 currencyString:[AdBrix currencyName:AdBrixCurrencyKRW] category:[AdBrixCommerceProductCategoryModel create:@""]
                                      extraAttrsMap:[AdBrixCommerceProductAttrModel create:@{}]]];
            }@catch (NSException *exception) {
                NSLog(@"productview error");
            }
            @finally {
            }
        }
        //addToWishList
        else if ([dictParams[@"func"] isEqualToString:@"addToWishList"]) {
            @try {
                NSString *sParams = [NSString stringWithFormat:@"?%@", dictParams[@"params"]];
                NSString * escapeParams = [[AKCommonUtil urlDecode:sParams] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
                NSDictionary *finalParams = [[NSURL URLWithString:escapeParams].query parseURLParams];
                
                NSString *goods_name = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_name"]]]];
                NSString *goods_id = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_id"]]]];
                NSString *goods_price = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_price"]]]];
                NSString *goods_final_price = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_final_price"]]]];
                NSLog(@"adbrix commerceAddToWishList %@ | %@ | %@ | %@" , goods_name,goods_id,goods_price,goods_final_price);
                [AdBrix commerceAddToWishList:
                 [AdBrix createCommerceProductModel:goods_id productName:goods_name price:[goods_price integerValue]                                        discount:[goods_final_price integerValue] quantity:1 currencyString:[AdBrix currencyName:AdBrixCurrencyKRW] category:[AdBrixCommerceProductCategoryModel create:@""]
                                      extraAttrsMap:[AdBrixCommerceProductAttrModel create:@{}]]];
                
            }@catch (NSException *exception) {
                NSLog(@"productview error");
            }
            @finally {
            }
        }
        //addToCart
        else if ([dictParams[@"func"] isEqualToString:@"addToCart"]) {
            @try {
                NSString *sParams = [NSString stringWithFormat:@"?%@", dictParams[@"params"]];
                NSString * escapeParams = [[AKCommonUtil urlDecode:sParams] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
                NSDictionary *finalParams = [[NSURL URLWithString:escapeParams].query parseURLParams];
                
                NSString *goods_name = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_name"]]]];
                NSString *goods_id = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_id"]]]];
                NSString *goods_price = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_price"]]]];
                NSString *goods_final_price = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_final_price"]]]];
                NSString *goods_opt_qty = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_opt_qty"]]]];
                NSString *goods_opt_name = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_opt_name"]]]];
                NSLog(@"adbrix addToCart %@ | %@ | %@ | %@ | %@ | %@" , goods_name,goods_id,goods_price,goods_final_price,goods_opt_qty,goods_opt_name);
                [AdBrix commerceAddToCart:
                 [AdBrix createCommerceProductModel:goods_id productName:goods_name price:[goods_price integerValue]                                        discount:[goods_final_price integerValue] quantity:[goods_opt_qty integerValue] currencyString:[AdBrix currencyName:AdBrixCurrencyKRW] category:[AdBrixCommerceProductCategoryModel create:@""]
                                      extraAttrsMap:[AdBrixCommerceProductAttrModel create:@{@"opt":goods_opt_name}]]];
                
            }@catch (NSException *exception) {
                NSLog(@"addToCart error");
            }
            @finally {
            }
        }
        //commerceShare
        else if ([dictParams[@"func"] isEqualToString:@"commerceShare"]) {
            @try {
                NSString *sParams = [NSString stringWithFormat:@"?%@", dictParams[@"params"]];
                NSString * escapeParams = [[AKCommonUtil urlDecode:sParams] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
                NSDictionary *finalParams = [[NSURL URLWithString:escapeParams].query parseURLParams];
                
                NSString *goods_name = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_name"]]]];
                NSString *goods_id = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_id"]]]];
                NSString *goods_price = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_price"]]]];
                NSString *goods_final_price = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_final_price"]]]];
                NSString *channel = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"channel"]]]];
                
                NSLog(@"adbrix commerceShare %@ | %@ | %@ | %@ | %@" , goods_name,goods_id,goods_price,goods_final_price,channel);

                
                int method = 11; //ETC
                if ([channel isEqualToString:@"Facebook"]) {
                    method = 1;
                }else if ([channel isEqualToString:@"KakaoTalk"]) {
                    method = 2;
                }else if ([channel isEqualToString:@"KakaoStory"]) {
                    method = 3;
                }else if ([channel isEqualToString:@"Line"]) {
                    method = 4;
                }else if ([channel isEqualToString:@"WhatsApp"]) {
                    method = 5;
                }else if ([channel isEqualToString:@"QQ"]) {
                    method = 6;
                }else if ([channel isEqualToString:@"WeChat"]) {
                    method = 7;
                }else if ([channel isEqualToString:@"SMS"]) {
                    method = 8;
                }else if ([channel isEqualToString:@"Email"]) {
                    method = 9;
                }else if ([channel isEqualToString:@"CopyUrl"]) {
                    method = 10;
                }
                
                [AdBrix commerceShare:[AdBrix sharingChannel:method]
                              product:[AdBrix createCommerceProductModel:goods_id productName:goods_name price:[goods_price integerValue]                                        discount:[goods_final_price integerValue] quantity:1 currencyString:[AdBrix currencyName:AdBrixCurrencyKRW] category:[AdBrixCommerceProductCategoryModel create:@""]
                                                           extraAttrsMap:[AdBrixCommerceProductAttrModel create:@{}]]];
            }@catch (NSException *exception) {
                NSLog(@"commerceShare error");
            }
            @finally {
            }
        }
        //commercePurchase
        else if ([dictParams[@"func"] isEqualToString:@"commercePurchase"]) {
            @try {
                NSString *sParams = [NSString stringWithFormat:@"?%@", dictParams[@"params"]];
                NSString * escapeParams = [[AKCommonUtil urlDecode:sParams] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
                NSDictionary *finalParams = [[NSURL URLWithString:escapeParams].query parseURLParams];
                
                NSString *goods_name = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_name"]]]];
                NSString *goods_id = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_id"]]]];
                NSString *goods_price = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_price"]]]];
                NSString *goods_final_price = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_final_price"]]]];
                NSString *goods_opt_qty = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_opt_qty"]]]];
                NSString *goods_opt_name = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"goods_opt_name"]]]];
                NSString *channel = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"channel"]]]];
                NSString *deliv = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"deliv"]]]];
                NSString *ord_id = [NSString stringWithFormat:@"%@", [AKCommonUtil urlDecode:[AKCommonUtil urlDecode:finalParams[@"ord_id"]]]];
                double discount = [goods_price doubleValue] - [goods_final_price doubleValue];
                
                NSLog(@"adbrix commercePurchase %@ | %@ | %@ | %@ | %@ | %@ | %@ | %@ |%@ |%f" ,ord_id, goods_name,goods_id,goods_price,goods_final_price,goods_opt_qty,goods_opt_name,channel,deliv,discount);
                NSMutableArray *productArr = [NSMutableArray array];
                
                [productArr addObject:
                 [AdBrix createCommerceProductModel:goods_id
                                        productName:goods_name
                                              price:[goods_price integerValue]
                                           discount:discount
                                           quantity:[goods_opt_qty integerValue]
                                     currencyString:[AdBrix currencyName:AdBrixCurrencyKRW]
                                           category:[AdBrixCommerceProductCategoryModel create:@""]
                                      extraAttrsMap:[AdBrixCommerceProductAttrModel create:@{ @"opt" : goods_opt_name}]
                  ]
                 ];
                int method = 4; //ETC
                if ([channel isEqualToString:@"CreditCard"]) {
                    method = AdBrixPaymentCreditCard;
                }else if ([channel isEqualToString:@"BankTransfer"]) {
                    method = AdBrixPaymentBankTransfer;
                }else if ([channel isEqualToString:@"MobilePayment"]) {
                    method = AdBrixPaymentMobilePayment;
                }
                
                [AdBrix commercePurchase:ord_id
                           productsInfos:productArr
                                discount:discount
                          deliveryCharge:[deliv integerValue]
                           paymentMethod:[AdBrix paymentMethod:method]
                 ];
            }@catch (NSException *exception) {
                NSLog(@"commerceShare error");
            }
            @finally {
            }
        }
    }
    // 기존방식
    else {
        NSDictionary *scheme = [AKCommonUtil parseScheme:url];
        
        NSString *sQuery = [NSURL URLWithString:url].query;
        NSDictionary *dictParams = [sQuery parseURLParams];
        
        NSLog(@"scheme = %s", [[NSString stringWithFormat:@"%@", scheme] UTF8String]);
        
        if ([scheme[@"command"] isEqualToString:@"sms"]) {
            // SMS 문자 전송
            NSString *body = [scheme[@"param"][@"t"] stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
            NSString *smsUrl = [NSString stringWithFormat:@"sms:&body=%@", body];
            [AKCommonUtil openURL:smsUrl];
        }
        else if ([scheme[@"command"] isEqualToString:@"openWebview"]) {
            NSString *subUrl = scheme[@"param"][@"url"];
            NSString *tp = scheme[@"param"][@"tp"];
            
            if (tp && ([tp isEqualToString:@"Z"] || [tp isEqualToString:@"A"])) {
                // 같은 depth의 카테고리는 같은 화면에서 리로딩
                if ([AKCommonUtil containsUrlString:subUrl checkString:CATEGORY_BIG_KEY]) {
                    if (categoryType == CATEGORY_BIG) {
                        if ([_delegate respondsToSelector:@selector(reloadCategory:)]) {
                            [_delegate reloadCategory:subUrl];
                        }
                    }
                    else {
                        
                    }
                }
                else if ([AKCommonUtil containsUrlString:subUrl checkString:CATEGORY_BRAND1_KEY] ||
                         [AKCommonUtil containsUrlString:subUrl checkString:CATEGORY_BRAND2_KEY]) {
                    if (categoryType == CATEGORY_BRAND) {
                        if ([_delegate respondsToSelector:@selector(reloadCategory:)]) {
                            [_delegate reloadCategory:subUrl];
                        }
                    }
                    else {
                    }
                }
                else if ([AKCommonUtil containsUrlString:subUrl checkString:CATEGORY_ITEM1_KEY] ||
                         [AKCommonUtil containsUrlString:subUrl checkString:CATEGORY_ITEM2_KEY]) {
                    if (categoryType == CATEGORY_ITEM) {
                        if ([_delegate respondsToSelector:@selector(reloadCategory:)]) {
                            [_delegate reloadCategory:subUrl];
                        }
                    }
                    else {
                        
                    }
                }
                else {
                    // 앱 내 새 web 화면
                    if ([_delegate respondsToSelector:@selector(openWebViewUrl:)]) {
                        [_delegate openWebViewUrl:subUrl];
                    }
                }
            }
            else {
                // webview 열기
                if ([_delegate respondsToSelector:@selector(openWebView:)]) {
                    [_delegate openWebView:tp];
                }
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"openBrowser"]) {
            // 브라우저 앱 호출하여 url 열기
            NSString *paramUrl = scheme[@"param"][@"url"];
            //[AKCommonUtil openWeb:paramUrl];
            
            if ([_delegate respondsToSelector:@selector(openBrowser:)]) {
                [_delegate openBrowser:paramUrl];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"clipboard"]) {
            // 클립보드 복사
            //akmall://clipboard?%7B%22t%22:72738445%7D
            //akmall://clipboard?%7B%22t%22%3A%22%E3%85%97%E3%85%97%E3%85%97%E3%85%97%22%7D
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];

            //TODO: param 값이 전달되지 않음 !
            if (scheme[@"param"][@"t"]) {
                pasteboard.string = scheme[@"param"][@"t"];
                [AKCommonUtil showAlertWithMsg:@"클립보드에 복사되었습니다."];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"closePopup"]) {
            // webview 팝업 닫기
            if ([_delegate respondsToSelector:@selector(closePopup:)]) {
                [_delegate closePopup:scheme[@"param"][@"tp"]];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"checkHeight"]) {
            // webview 높이 조절
            if ([_delegate respondsToSelector:@selector(updateHeight:)]) {
                [_delegate updateHeight:scheme[@"param"]];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"showLoading"]) {
            // 로딩 뷰 보여주기
            [[AKCommonUtil getVisibleController] startIndicator];
        }
        else if ([scheme[@"command"] isEqualToString:@"hideLoading"]) {
            // 로딩 뷰 가리기
            [[AKCommonUtil getVisibleController] stopIndicator];
        }
        else if ([scheme[@"command"] isEqualToString:@"prodList"]) {
            // 카테고리 상품 목록
            if ([_delegate respondsToSelector:@selector(getProdList:)]) {
                [_delegate getProdList:scheme[@"param"]];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"changeTab"]) {
            // 카테고리 탭 변경
            if ([_delegate respondsToSelector:@selector(changeTab:)]) {
                [_delegate changeTab:scheme[@"param"]];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"goBack"]) {
             if ([webView canGoBack]) {
                [webView goBack];
            }
            else {
                // 이전 화면
                [[AKCommonUtil getVisibleController].navigationController popViewControllerAnimated:YES];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"voiceSearch"]) {
            // 음성검색 화면
            AKSpeechViewController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"AKSpeechViewController"];
            [[AKCommonUtil getVisibleController].navigationController pushViewController:controller animated:YES];
        }
        else if ([scheme[@"command"] isEqualToString:@"setup"]) {
            // 설정 화면
            [[AKCommonUtil getVisibleController] performSegueWithIdentifier:@"Setting" sender:[AKCommonUtil getVisibleController]];
        }
        else if ([scheme[@"command"] isEqualToString:@"callJavascriptAll"]) {
            // 카테고리 모든 웹뷰에 자바스크립트 함수 호출
            if ([_delegate respondsToSelector:@selector(callJavascriptAll:)]) {
                [_delegate callJavascriptAll:scheme[@"param"][@"tp"]];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"callSelectPop"]) {
            // 셀렉트 팝업 호출
            if ([_delegate respondsToSelector:@selector(showSelectPopup:)]) {
                [_delegate showSelectPopup:scheme[@"param"]];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"changeParam"]) {
            // 파라미터 변경
            if ([_delegate respondsToSelector:@selector(changeParam:)]) {
                [_delegate changeParam:scheme[@"param"]];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"showNative"]) {
            // 목록 보이기
            if ([_delegate respondsToSelector:@selector(showNative)]) {
                [_delegate showNative];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"hideNative"]) {
            // 목록 감추기
            if ([_delegate respondsToSelector:@selector(hideNative)]) {
                [_delegate hideNative];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"drawList"]) {
            // 목록 다시 그리기
            if ([_delegate respondsToSelector:@selector(drawList)]) {
                [_delegate drawList];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"logStatef"]) {
            // 로그인 상태 수신
            NSLog(@"cn : %@ ", scheme[@"param"][@"cn"]);
            
            if(scheme[@"param"][@"cn"] > 0) {
                [[finger sharedData] requestRegIdWithBlock:scheme[@"param"][@"cn"] :^(NSString *posts, NSError *error) {
                    NSLog(@"posts : %@  error : %@",posts,error);
                }];
                //Adbrix login
                [AdBrix commerceLogin:[[scheme[@"param"][@"cn"] dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0]];
            }
            
            NSString *isLoginYn = [AKCommonUtil getCookieValue:@"loginf" web:webView];
            NSString *loginCooke = [AKCommonUtil getCookieValue:@"loginToken" web:webView];
            NSString *keepId = [AKCommonUtil getCookieValue:@"keep_id" web:webView];
            
            [AKProperty setLogin:[isLoginYn hasSuffix:@"Y"]];
            [AKProperty setLoginToken:loginCooke];
            [AKProperty setKeepId:keepId];
            
            //푸시 인입시 자동로그인되면서 일로 들어오면 메뉴로 강제 이동되는 현상 수정
            //메뉴 들어갈때마다 새로고침으로 변경하여 아래 부분은 필요없어짐 20190103 민석
            //[appDelegate leftMenuRefresh];
        }
        else if ([scheme[@"command"] isEqualToString:@"callLayerFilter"]) {
            // 필터 오픈 후 필터 화면 업데이트
            if ([_delegate respondsToSelector:@selector(callLayerFilter:)]) {
                NSArray *temp = [url componentsSeparatedByString:@"?"];
                
                if (temp.count > 1) {
                    [_delegate callLayerFilter:temp[1]];
                }
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"sendFilter"]) {
            // 메인 화면 업데이트
            if ([_delegate respondsToSelector:@selector(sendFilter:)]) {
                NSArray *temp = [url componentsSeparatedByString:@"?"];
                if (temp.count > 1) {
                    [_delegate sendFilter:temp[1]];
                }
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"scrollTop"]) {
            NSLog(@"scrollTop....");
            // 최상단으로 스크롤
//            if ([_delegate respondsToSelector:@selector(scrollTop)]) {
//                [_delegate scrollTop];
//            }
        }
        else if ([scheme[@"command"] isEqualToString:@"callWishPopup"]) {
            // 찜하기
            if ([_delegate respondsToSelector:@selector(callWishPopup:)]) {
                [_delegate callWishPopup:scheme[@"param"][@"goods_id"]];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"sendMainPopup"]) {
            NSLog(@"sendMainPopup....");
            // 메인 팝업
//            if ([_delegate respondsToSelector:@selector(sendMainPopup:)]) {
//                NSRange range = [url rangeOfString:@"?"];
//                NSString *temp = [url substringFromIndex:range.location + 1];
//                NSString *param = [AKCommonUtil urlDecode:temp];
//
//                [_delegate sendMainPopup:[AKCommonUtil getObjectFromJsonString:param]];
//            }
        }
        else if ([scheme[@"command"] isEqualToString:@"hideGNB"]) {
            NSLog(@"hideGNB....");
//            if ([_delegate respondsToSelector:@selector(hideGNB)]) {
//                [_delegate hideGNB];
//            }
        }
        else if ([scheme[@"command"] isEqualToString:@"showGNB"]) {
            NSLog(@"showGNB....");
//            if ([_delegate respondsToSelector:@selector(showGNB)]) {
//                [_delegate showGNB];
//            }
        }
        else if ([scheme[@"command"] isEqualToString:@"hideFloat"]) {
            NSLog(@"hideFloat....");
            // float menu 숨기기
//            if ([_delegate respondsToSelector:@selector(hideFloat)]) {
//                [_delegate hideFloat];
//            }
        }
        else if ([scheme[@"command"] isEqualToString:@"showFloat"]) {
            NSLog(@"showFloat....");
            // float menu 보이기
//            if ([_delegate respondsToSelector:@selector(showFloat)]) {
//                [_delegate showFloat];
//            }
        }
        else if ([scheme[@"command"] isEqualToString:@"readPositionX"]) {
            NSLog(@"readPositionX....");
            // 카테고리 상단 x 위치
//            if ([_delegate respondsToSelector:@selector(readPositionX:)]) {
//                [_delegate readPositionX:[scheme[@"param"][@"x"] intValue]];
//            }
        }
        else if ([scheme[@"command"] isEqualToString:@"callShareLayer"]) {
            NSLog(@"callShareLayer....");
            // 공유하기 호출
//            if ([_delegate respondsToSelector:@selector(callShareLayer:)]) {
//                [_delegate callShareLayer:scheme[@"param"][@"goodsId"]];
//            }
        }
        else if ([scheme[@"command"] isEqualToString:@"callpWishInpt"]) {
            // 찜하기 호출
            if ([_delegate respondsToSelector:@selector(callpWishInpt:)]) {
                [_delegate callpWishInpt:scheme[@"param"][@"goodsId"]];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"callShoopingCart"]) {
            // 장바구니 호출
            if ([_delegate respondsToSelector:@selector(callShoopingCart:)]) {
                [_delegate callShoopingCart:scheme[@"param"][@"goodsId"]];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"closeAndOpenWebview"]) {
            // 현재 팝업을 닫고 url 이동
            if ([_delegate respondsToSelector:@selector(closeAndOpenWebview:)]) {
                [_delegate closeAndOpenWebview:scheme[@"param"][@"url"]];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"callCheck"]) {
            if ([_delegate respondsToSelector:@selector(showBack:)]) {
                [_delegate showBack:[scheme[@"param"][@"r"] intValue] == 0];
            }
        }
        else if ([scheme[@"command"] isEqualToString:@"callToken"]) {
            NSString *sScript = [NSString stringWithFormat:@"callToken('%@')",[[finger sharedData] getDeviceIdx]];
            if (_delegate && [_delegate respondsToSelector:@selector(callJavaScriptFromWeb:)]) {
                [_delegate callJavaScriptFromWeb:sScript];
            }
        } else if ([scheme[@"command"] isEqualToString:@"pushStatus"]) {
            //푸쉬 상태 리턴
            [self returnPushStatus];
            
        } else if ([scheme[@"command"] isEqualToString:@"activity"]) {
            BOOL activity = [dictParams[@"activity"] isEqualToString:@"true"];
            NSLog(@"activity : %@  ",dictParams[@"activity"]);
            [[finger sharedData] setEnable:activity :^(NSString *posts, NSError *error) {
                [self returnPushStatus];
                
                if(!error) {
                    //[AKCommonUtil showAlertWithMsg:[NSString stringWithFormat:@"앱 알림 수신 %@가 %@ 정상적으로 처리 되었습니다.", activity ? @"동의" : @"거부", [AKCommonUtil getTodayString:@"yyyy년 MM월 dd일"]]];
                }
            }];
        } else if ([scheme[@"command"] isEqualToString:@"ad_activity"]) {
            BOOL ad_activity = [dictParams[@"ad_activity"] isEqualToString:@"true"];
            [[finger sharedData] requestSetAdPushEnable:ad_activity :^(NSString *posts, NSError *error) {
                [self returnPushStatus];
                if(!error) {
                    //[AKCommonUtil showAlertWithMsg:[NSString stringWithFormat:@"마케팅(광고성) 앱 알림 수신 %@가 %@ 정상적으로 처리 되었습니다.", ad_activity ? @"동의" : @"거부", [AKCommonUtil getTodayString:@"yyyy년 MM월 dd일"]]];
                }
            }];
        } else if ([scheme[@"command"] isEqualToString:@"callMyFeed"]) {
            NSLog(@"json : %@  error : ",@"callMyFeed");
            [[finger sharedData] requestPushListPageWithBlock:1 Cnt:50 :^(NSDictionary *posts, NSError *error) {
                
                //NSString *todayStr = [AKCommonUtil getTodayString:@"yyyyMMdd"];
                if(!error) {
                    if(posts) {
                        int total= 0;
                        NSMutableArray *objList = [NSMutableArray new];
                        for (NSDictionary *dic in posts[@"pushList"]) {
                            
                            NSDictionary * obj = @{@"msgTag":dic[@"msgTag"], @"mode":dic[@"mode"]};
                            
                            [objList insertObject:obj atIndex:total];
                            total++;
                            NSLog(@"msg : %@  mode : %@ index : %d" ,dic[@"msgTag"],dic[@"mode"],total);
                            [[finger sharedData] requestPushCheckWithBlock:dic :^(NSString *posts, NSError *error) {
                                NSLog(@"PushList.do 읽음 처리 posts : %@ / error : %@",posts,error);
                            }];
                            
                        }
                        NSString *json = [AKCommonUtil getJsonStringFromObject : objList];
                        NSString *encodeString = [AKCommonUtil urlEncode : json];
                        
                        NSString *url = [NSString stringWithFormat:@"%@/app/PushList.do?idxList=%@", BASE_URL, encodeString];
                        
                        [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
                    }
                }
                
            }];
            
        } else if ([scheme[@"command"] isEqualToString:@"callMyFeedCnt"]) {
            
            [[finger sharedData] requestPushListPageWithBlock:1 Cnt:50 :^(NSDictionary *posts, NSError *error) {
                NSLog(@"posts : %@  error : %@",posts,error);
                NSString *todayStr = [AKCommonUtil getTodayString:@"yyyyMMdd"];
                if(!error) {
                    if(posts) {
                        int total= 0;
                        int cnt = 0;
                        for (NSDictionary *dic in posts[@"pushList"]) {
                            
                            if([dic[@"opened"] isEqual:@"N"] && [[dic[@"date"] substringToIndex:8] isEqual:todayStr] ) {
                                cnt++;
                            }
                            total++;
                        }
                        NSLog(@"callMyFeedCnt(%d,%d)",cnt,total);
                        NSString *sScript = [NSString stringWithFormat:@"callMyFeedCnt(%d,%d)",cnt,total];
                        if (self->_delegate && [self->_delegate respondsToSelector:@selector(callJavaScriptFromWeb:)]) {
                            [self->_delegate callJavaScriptFromWeb:sScript];
                        }
                        
                    }
                }
            }];
        }
        else if ([scheme[@"command"] isEqualToString:@"setIDFA"]) {
            NSString *sScript = [NSString stringWithFormat:@"setIDFA('%@')",[self identifierForAdvertising]];
            if (_delegate && [_delegate respondsToSelector:@selector(callJavaScriptFromWeb:)]) {
                [_delegate callJavaScriptFromWeb:sScript];
            }
        }
        // add 20210111 선물하기에서 메뉴에서 주소록 목록을 전달함
        else if ([scheme[@"command"] isEqualToString:@"getContactList"]) {
            
            if ([CNContactStore class]) {
                //ios9 or later
                CNEntityType entityType = CNEntityTypeContacts;
                if( [CNContactStore authorizationStatusForEntityType:entityType] == CNAuthorizationStatusNotDetermined)
                 {
                     CNContactStore * contactStore = [[CNContactStore alloc] init];
                     [contactStore requestAccessForEntityType:entityType completionHandler:^(BOOL granted, NSError * _Nullable error) {
                         if(granted){
                             //[self getAllContact];
                             [self openContact];//add 20210121 p65458
                         }
                     }];
                 }
                else if( [CNContactStore authorizationStatusForEntityType:entityType]== CNAuthorizationStatusAuthorized)
                {
                    //[self getAllContact];
                    [self openContact];//add 20210121 p65458
                }
            }
            
            
            //NSError *error = nil;
            //jsonStr = [AKCommonUtil urlEncode:jsonStr];
            //[AKCommonUtil getContactlist:error];//20210113 주소록 가져오기
            //[self openContact];//20210113 연동 가져오기
        }
    }
}



- (NSString *)identifierForAdvertising {
    if([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]) {
        NSUUID *IDFA = [[ASIdentifierManager sharedManager] advertisingIdentifier];
        return [IDFA UUIDString];
    }
    return nil;
}

- (void)returnPushStatus {
    [[finger sharedData] requestPushInfoWithBlock:^(NSDictionary *posts, NSError *error) {
        NSLog(@"posts : %@  errora : %@",posts,error);
        if (posts != nil) {
            NSString *sScript = [NSString stringWithFormat:@"pushStatus(%@,%@)",[posts[@"activity"] isEqualToString:@"A"]?@"true":@"false",[posts[@"ad_activity"] isEqualToString:@"A"]?@"true":@"false"];
            if (self->_delegate && [self->_delegate respondsToSelector:@selector(callJavaScriptFromWeb:)]) {
                [self->_delegate callJavaScriptFromWeb:sScript];
            }
        }
    }];
}



- (void)openContact {
    CNContactPickerViewController *cnContactPickerViewController = [[CNContactPickerViewController alloc] init];
        [cnContactPickerViewController setDelegate:self];
        // #. 이메일이 없다면 선택이 안되게 진행하기.
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"emailAddresses.@count > 0"];
    //    cnContactPickerViewController.predicateForEnablingContact = predicate;
        
        // #. 번호가 없다면 선택 안되게.
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"phoneNumbers.@count > 0"];
        cnContactPickerViewController.predicateForEnablingContact = predicate;
        
        // 이동하기
        //[self presentViewController:cnContactPickerViewController animated:YES completion:nil];
        [[AKCommonUtil getVisibleController] presentViewController:cnContactPickerViewController animated:YES completion:nil];

}


- (void) contactPickerDidCancel:(CNContactPickerViewController *)picker {
    NSLog(@"선택 취소");
    [picker dismissViewControllerAnimated:YES completion:nil];
}
 
- (void) contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact {
    NSLog(@"값 선택 : %@",contact);
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    //javascript 방식으로 인해 블럭 20210115 p65458*/
    //[AKCommonUtil parseContactWithContact:contact];

    //javascript 방식으로 사용 20210115 p65458
    NSString * firstName =  [contact.givenName stringByReplacingOccurrencesOfString:@"'" withString:@""];
    NSString * lastName =  [contact.familyName stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    //NSString * phone = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
        NSString * phone = [[contact.phoneNumbers[0] valueForKey:@"value"] valueForKey:@"digits"];//대표번호하나만 사용
    NSString *JSInjection = [NSString stringWithFormat:@"toSendTargetInfo('%@','%@');", [lastName stringByAppendingString:firstName], phone]; //@"javascript:toSendTargetInfo(name,number)";
    //[webView evaluateJavaScript:JSInjection completionHandler:nil];
     [self->_delegate callJavaScriptFromWeb:JSInjection];

}
@end
