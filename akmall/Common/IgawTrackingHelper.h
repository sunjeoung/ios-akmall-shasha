//
//  IgawTrackingHelper.h
//  AKMall
//
//  Created by KimJinoug on 2017. 1. 19..
//  Copyright © 2017년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface IgawTrackingHelper : NSObject

+ (BOOL)trackingPurchase:(WKWebView *)targetWebview;

@end
