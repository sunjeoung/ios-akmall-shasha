//
//  AKCommonUtil.h
//  akmall
//
//  Created by KimJinoug on 2016. 10. 20..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

#import "GlobalHeader.h"
#import "AKBaseViewController.h"
#import "AppDelegate.h"
#import "AKSelectView.h"
#import "AKVersionPopupView.h"
#import <Contacts/Contacts.h>

@class AKBaseViewController;

@interface AKCommonUtil : NSObject

+ (AKBaseViewController * __nullable)getVisibleController;

#pragma mark - Select Popup

+ (void)showSelect:(UIView * __nonnull)parent delegate:(id<AKSelectViewDelegate> __nullable)delegate dataList:(NSArray * __nonnull)dataList valueList:(NSArray * __nullable)valueList selectedIndex:(int)index;

#pragma mark - Popup WebView

+ (void)showSnsShare:(UIView * __nonnull)parent url:(NSString * __nonnull)url;
+ (void)showPopupWebView:(UIView * __nonnull)parent url:(NSString * __nonnull)url;

#pragma mark - Full Popup

+ (void)showFullPopup:(UIView * __nonnull)parent data:(NSDictionary * __nonnull)data;

#pragma mark - Version Popup

+ (void)showVersionPopup:(UIView * __nonnull)parent delegate:(id<AKVersionPopupViewDelegate> __nullable)delegate type:(NSString * __nullable)type;

#pragma mark - History Webview

+ (void)showCategoryView:(UIView * __nonnull)parent;

#pragma mark - image from color

+ (UIImage * __nonnull)imageFromColor:(UIColor * __nonnull)color;

#pragma mark - UIButton Border

+ (void)setBorder:(UIView * __nonnull)btn width:(CGFloat)width color:(UIColor * __nonnull)color;

#pragma mark - Currency

+ (NSString * __nullable)getCurrencyFormat:(int)number;

#pragma mark - Date

+ (NSString * __nullable)getDateString:(NSDate * __nonnull)date format:(NSString * __nullable)format;
+ (NSString * __nullable)getTodayString:(NSString * __nullable)format;

#pragma mark - Alert

+ (void)showAlertWithMsg:(NSString * __nullable)msg;
+ (void)showAlert:(NSString * __nullable)title msg:(NSString * __nullable)msg;
+ (void)showAlert:(NSString * __nullable)title msg:(NSString * __nullable)msg cancelBlock:(void (^ __nullable)(UIAlertAction * __nullable action))cancelHandler confirmBlock:(void (^ __nullable)(UIAlertAction * __nullable action))confirmHandler;
+ (void)showAlert:(NSString * __nullable)title msg:(NSString * __nullable)msg cancelTitle:(NSString * __nullable)cancelTitle cancelBlock:(void (^ __nullable)(UIAlertAction * __nullable action))cancelHandler;
+ (void)showAlert:(NSString * __nullable)title msg:(NSString * __nullable)msg cancelTitle:(NSString * __nullable)cancelTitle cancelBlock:(void (^ __nullable)(UIAlertAction * __nullable action))cancelHandler confirmTitle:(NSString * __nullable)confirmTitle confirmBlock:(void (^ __nullable)(UIAlertAction * __nullable action))confirmHandler;

#pragma mark - push webview

+ (void)pushWebViewController:(NSString * __nonnull)url;
+ (void)openOutWebView:(NSString * __nonnull)url;
+ (void)openPlayerWebView:(NSString * __nonnull)url;
+ (void)openWeb:(NSString * __nonnull)url;
+ (void)openURL:(NSString * __nonnull)url;

#pragma mark - webview scheme

+ (NSDictionary * __nullable)parseScheme:(NSString * __nullable)url;

#pragma mark - URL

+ (NSString * __nonnull)getRequestUrl:(NSString * __nullable)url;

#pragma mark - check nil

+ (NSString * __nonnull)checkNil:(NSString * __nullable)str;

#pragma mark - check url

+ (BOOL)containsUrlString:(NSString * __nonnull)urlString checkString:(NSString * __nonnull)checkString;
+ (BOOL)containsStringList:(NSArray * __nonnull)stringList inString:(NSString * __nonnull)str;

#pragma mark - Javascript

+ (void)callJavascript:(WKWebView * __nonnull)webView function:(NSString * __nonnull)function object:(id __nullable)object;
+ (void)callJavascriptNoEncode:(WKWebView * __nonnull)webView function:(NSString * __nonnull)function object:(id __nullable)object;
+ (void)checkTargetBlank:(WKWebView * __nonnull)webView;
+ (void)getTitle:(WKWebView * __nonnull)webView;
+ (void)checkBack:(WKWebView * __nonnull)webView;

#pragma mark - JSON

+ (NSString * __nullable)getJsonStringFromObject:(id __nullable)object;
+ (id __nullable)getObjectFromJsonString:(NSString * __nonnull)jsonStr;

#pragma mark - url encode/decode

+ (NSString * __nullable)urlEncode:(NSString * __nonnull)str;
+ (NSString * __nullable)urlDecode:(NSString * __nonnull)str;

#pragma mark - BASE64

+ (NSString * __nullable)base64Encode:(NSString * __nullable)str;

#pragma mark - Cookie

+ (NSString * __nullable)getCookieValue:(NSString * __nonnull)key;
+ (NSString * __nullable)getCookieValue:(NSString *)key web:(WKWebView *)webView;
+ (void)setCookie:(NSString * __nonnull)key value:(NSString * __nonnull)value;

#pragma mark - UUID

+ (NSString * __nonnull)getUUID;

#pragma mark - Go Home

+ (void)goHome:(NSString * __nullable)url;

#pragma mark - Check WebView Error

+ (BOOL)checkWebViewError:(NSError * __nullable)error;

// add 20210111 선물하기에서 메뉴에서 주소록 목록을 전달함
#pragma mark - get device addrList

+ (void)getContactlist:(NSError * __nullable)error;

+ (void)getAllContact;


+ (void)parseContactWithContact :(CNContact* )contact;
+ (NSMutableArray *)parseAddressWithContac: (CNContact *)contact;
@end
