//
//  IgawTrackingHelper.m
//  AKMall
//
//  Created by KimJinoug on 2017. 1. 19..
//  Copyright © 2017년 sjsofttech. All rights reserved.
//

#import "IgawTrackingHelper.h"
#import <IgaworksCommerce/IgaworksCommerce.h>

@implementation IgawTrackingHelper

static NSString *lastPurchaseJson = @"";

+ (NSString *)callJavaScriptReturnValue:(NSString *)script webView:(WKWebView *)web
{
    __block NSString *resultString = nil;
    __block BOOL finished = NO;

    [web evaluateJavaScript:script completionHandler:^(id result, NSError *error) {
        if (error == nil) {
            if (result != nil) {
                resultString = [NSString stringWithFormat:@"%@", result];
            }
        }
        finished = YES;
    }];

    while (!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }

    return resultString;
}

+ (BOOL)trackingPurchase:(WKWebView *)targetWebview {
    NSString *purchaseJsonString = [IgawTrackingHelper callJavaScriptReturnValue:@"purchaseJson" webView:targetWebview];
 
    // 중복호출 방지
    if ([purchaseJsonString isEqualToString:lastPurchaseJson]) {
        return NO;
    }
    
    if (purchaseJsonString && ![purchaseJsonString isEqualToString:@""]) {
        NSLog(@"purchaseJsonString %@", purchaseJsonString);
        lastPurchaseJson = purchaseJsonString;
        // IGAW연동
        [IgaworksCommerce purchase:purchaseJsonString];
        
        return YES;
    }
    else {
        lastPurchaseJson = @"";
        return NO;
    }
}

@end
