//
//  AKProperty.h
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 11..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GlobalHeader.h"

@interface AKProperty : NSObject

+ (void)setLogin:(BOOL)isLogin;
+ (BOOL)isLogin;
+ (void)setLoginToken:(NSString * __nonnull)loginToken;
+ (NSString * __nonnull)getLoginToken;
+ (void)setNotice:(NSString * __nonnull)notice;
+ (NSString * __nullable)getNotice;
+ (void)setDeviceToken:(NSString * __nonnull)token;
+ (NSString * __nullable)getDeviceToken;
+ (void)setIsDenyTime:(BOOL)isDeny;

+ (BOOL)isDenyTime;
+ (void)setStartTime:(NSString * __nonnull)time;
+ (NSString * __nullable)getStartTime;
+ (void)setEndTime:(NSString * __nonnull)time;
+ (NSString * __nullable)getEndTime;
+ (void)setPopupTodayDate:(NSString * __nonnull)popupId;
+ (NSString * __nullable)getPopupTodayDate:(NSString * __nonnull)popupId;
+ (void)setPushId:(NSString * __nonnull)pushId;
+ (NSString * __nullable)getPushId;
+ (void)setDeepLinkUrl:(NSString * __nonnull)url;
+ (NSString * __nullable)getDeepLinkUrl;

+ (void)setKeepId:(NSString *)keepId;
+ (NSString * __nullable)getkeepId;

@end
