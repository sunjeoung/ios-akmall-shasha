//
//  GlobalHeader.h
//  akmall
//
//  Created by KimJinoug on 2016. 10. 20..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#ifndef GlobalHeader_h
#define GlobalHeader_h

//#define PRIVATE_SERVER

#ifdef PRIVATE_SERVER
//#define BASE_URL                    @"http://91.3.115.209" //민석
//#define BASE_URL                    @"http://91.3.115.179" //수원
#define BASE_URL                    @"https://devm.akmall.com"
#else
#define BASE_URL                    @"https://m.akmall.com"
#endif

#ifdef DEBUG
#define NSLog(...) NSLog(__VA_ARGS__)
    #define APPKEY                      @"SVU4A2XRJNDA75S9RC9CL7UQ3RMO89ZG"
    #define APPSCRETE                   @"SzgVHgpuUmCPRh299eyhMYr2cjlWALss"
#else
#define NSLog(...) (void)0
    #define APPKEY                      @"D14UBB7BLJKBL4R59FOHNOR03095FRN4"
    #define APPSCRETE                   @"2nRxM1z5vcMh7rRFGri2UryfY2na6o3g"
#endif

#define UIColorMake(r,g,b)          [[UIColor alloc] initWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0f]

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define APP_GROUP_NAME              @"group.com.akmall.touchAKmall"
#define LOGIN_KEY                   @"login_key"
#define LOGINTOKEN_KEY              @"logintoken_key"
#define NOTICE_KEY                  @"notice_key"
#define DEVICETOKEN_KEY             @"deviceToken"
#define DENYTIME_KEY                @"denytime_key"
#define STARTTIME_KEY               @"starttime_key"
#define ENDTIME_KEY                 @"endtime_key"
#define POPUPTODAYDATE_KEY          @"popuptodaydate"
#define PUSHID_KEY                  @"pushid_key"
#define DEEPLINKURL_KEY             @"deeplinkurl_key"

#define LISTTYPE_NORMAL             1
#define LISTTYPE_DOUBLE             2
#define LISTTYPE_GALLERY            3

// 카테고리 타입
#define CATEGORY_NONE               0
#define CATEGORY_BIG                1
#define CATEGORY_ITEM               2
#define CATEGORY_BRAND              3
#define CATEGORY_AKNOW              4
#define CATEGORY_MAIN               5

// native category 호출 키 스트링
#define CATEGORY_BIG_KEY            @"/ShopFront.do"
#define CATEGORY_ITEM1_KEY          @"/CtgMClsf.do"
#define CATEGORY_ITEM2_KEY          @"/CtgSClsf.do"
#define CATEGORY_BRAND1_KEY         @"/BrandCtgMClsf.do"
#define CATEGORY_BRAND2_KEY         @"/BrandCtgSClsf.do"
#define CATEGORY_AKNOW_KEY          @"/DeptStore.do"

// push url
#define PUSH_DETAIL_URL             @"/app/lib.do?isAkApp=iPhone&act=viewPushDetail&push_id="

// webpage url
#define URL_SNS_SHARE               @"/goods/pSnsShare.do?native=Y"
#define URL_LIKE                    @"/common/AppPage.do?forwardPage=pWishInpt"
#define URL_CATEGORY_FRONT          @"/display/ShopFront.do?isAkApp=iPhone"
#define URL_CATEGORY_FRONT_ITEM1    @"/display/CtgMClsf.do?isAkApp=iPhone"
#define URL_CATEGORY_FRONT_ITEM2    @"/display/CtgSClsf.do?isAkApp=iPhone"
#define URL_CATEGORY_FRONT_BRAND1   @"/display/BrandCtgMClsf.do?isAkApp=iPhone"
#define URL_CATEGORY_FRONT_BRAND2   @"/display/BrandCtgSClsf.do?isAkApp=iPhone"
#define URL_CATEGORY_FRONT_AKNOW    @"/akplaza/DeptStore.do?isAkApp=iPhone"
#define URL_CATEGORY_RIGHT          @"/common/AppPage.do?forwardPage=CateLayer"
#define URL_FILTER_RIGHT            @"/common/AppPage.do?forwardPage=FilterLayer"
#define URL_HISTORY                 @"/common/AppPage.do?forwardPage=HistoryLayer"
#define URL_SEARCH                  @"/search/SearchMain.do"
#define URL_CATEGORY                @"/common/menu.do"
#define URL_BAG                     @"/order/ShoppingCart.do"
#define URL_MYAK                    @"/mypage/MyPlaceMain.do"
#define URL_LOGIN                   @"/login/Login.do"
#define URL_CHECK                   @"/event/RightVisit.do"
#define URL_CAR                     @"/mypage/OrderDeliInquiry.do"
#define URL_EVENT                   @"/event/EventMain.do"
#define URL_LIKEIT                  @"/mypage/MyBelongingGoods.do"
#define URL_PRODUCT_DETAIL          @"/goods/GoodsDetail.do"
#define URL_PRODUCT_RELATIVE        @"/common/AppPage.do?forwardPage=RelatedGoodsList"
#define URL_PRIVACY                 @"/info/PrsnlTreat.do"
#define URL_USE_LABEL               @"/info/UseStiplt.do"
#define URL_COMPANY                 @"http://ftc.go.kr/info/bizinfo/communicationView.jsp?apv_perm_no=2007378010630200807&area1=&area2=&currpage=1&searchKey=04&searchVal=1298542351&stdate=&enddate="
#define URL_PC_MAIN                 @"http://www.akmall.com/main/Main.do?viewpc=Y"
#define URL_QUESTION                @"/customer/OneToOneQna.do"
#define URL_MY_FEEDLIST             @"/mypage/MyFeedListApp.do"
#define URL_CUSTOMER_CENTER         @"/customer/CustCenterMain.do"

// url
#define URL_TEST                    @"/login/CheckCustCertNoAjax.do"
#define URL_CATEGORY_BEST           @"/display/selectGoodsByBestAjax.do"
#define URL_CATEGORY_NEW            @"/display/selectGoodsByNewAjax.do"
#define URL_CATEGORY_ITEM           @"/display/PrdListAjax.do"
#define URL_CATEGORY_AKNOW          @"/akplaza/DeptStoreGoodsListAjax.do"

#define URL_USER_INFO               @"/app/userInfo.do"
#define URL_APP_LIB                 @"/app/lib.do"
#define URL_LOGOUT                  @"/login/Logout.do"
#define URL_WIDGET_INFO             @"/widget/info.do"

#pragma mark - add by hans

#define URL_PRELOAD                 @"/app/preload.do"

#ifdef PRIVATE_SERVER
#define URL_MAIN                    @"/main/Main.do"
#define URL_TABMENU_LIST            @"/main/TabListNew.do"
#else
#define URL_MAIN                    @"/main/Main.do?isAkApp=iPhone"
#define URL_TABMENU_LIST            @"/main/TabList.do"
#endif
#endif /* GlobalHeader_h */
