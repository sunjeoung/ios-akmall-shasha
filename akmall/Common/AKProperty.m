//
//  AKProperty.m
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 11..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKProperty.h"
#import "AKCommonUtil.h"

@implementation AKProperty

+ (void)setLogin:(BOOL)isLogin {
    NSUserDefaults *ud = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUP_NAME];
    
    [ud setBool:isLogin forKey:LOGIN_KEY];
    [ud synchronize];
}

+ (BOOL)isLogin {
    NSUserDefaults *ud = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUP_NAME];
    
    return [ud boolForKey:LOGIN_KEY];
}

+ (void)setLoginToken:(NSString *)loginToken {
    NSUserDefaults *ud = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUP_NAME];
    
    [ud setObject:loginToken forKey:LOGINTOKEN_KEY];
    [ud synchronize];
}

+ (NSString *)getLoginToken {
    NSUserDefaults *ud = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUP_NAME];
    NSString *loginToken = [ud stringForKey:LOGINTOKEN_KEY];
    
    return loginToken ? loginToken : @"";
}

+ (void)setNotice:(NSString *)notice {
    NSUserDefaults *ud = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUP_NAME];
    
    [ud setObject:notice forKey:NOTICE_KEY];
    [ud synchronize];
}

+ (NSString *)getNotice {
    NSUserDefaults *ud = [[NSUserDefaults alloc] initWithSuiteName:APP_GROUP_NAME];
    NSString *notice = [ud stringForKey:NOTICE_KEY];
    
    return notice ? notice : @"";
}

+ (void)setDeviceToken:(NSString *)token {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    [ud setObject:token forKey:DEVICETOKEN_KEY];
    [ud synchronize];
}

+ (NSString *)getDeviceToken {
    return [[NSUserDefaults standardUserDefaults] objectForKey:DEVICETOKEN_KEY];
}

+ (void)setIsDenyTime:(BOOL)isDeny {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    [ud setBool:isDeny forKey:DENYTIME_KEY];
    [ud synchronize];
}

+ (BOOL)isDenyTime {
    return [[NSUserDefaults standardUserDefaults] boolForKey:DENYTIME_KEY];
}

+ (void)setStartTime:(NSString *)time {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    [ud setObject:time forKey:STARTTIME_KEY];
    [ud synchronize];
}

+ (NSString *)getStartTime {
    return [[NSUserDefaults standardUserDefaults] objectForKey:STARTTIME_KEY];
}

+ (void)setEndTime:(NSString *)time {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    [ud setObject:time forKey:ENDTIME_KEY];
    [ud synchronize];
}

+ (NSString *)getEndTime {
    return [[NSUserDefaults standardUserDefaults] objectForKey:ENDTIME_KEY];
}

+ (void)setPopupTodayDate:(NSString *)popupId {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    [ud setObject:[AKCommonUtil getDateString:[NSDate date] format:@"yyyyMMdd"] forKey:[NSString stringWithFormat:@"%@_%@", POPUPTODAYDATE_KEY, popupId]];
    [ud synchronize];
}

+ (NSString *)getPopupTodayDate:(NSString *)popupId {
    return [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_%@", POPUPTODAYDATE_KEY, popupId]];
}

+ (void)setPushId:(NSString *)pushId {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    [ud setObject:pushId forKey:PUSHID_KEY];
    [ud synchronize];
}

+ (NSString *)getPushId {
    return [[NSUserDefaults standardUserDefaults] objectForKey:PUSHID_KEY];
}

+ (void)setDeepLinkUrl:(NSString *)url {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    [ud setObject:url forKey:DEEPLINKURL_KEY];
    [ud synchronize];
}

+ (NSString *)getDeepLinkUrl {
    return [[NSUserDefaults standardUserDefaults] objectForKey:DEEPLINKURL_KEY];
}

+ (void)setKeepId:(NSString *)keepId
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:keepId forKey:@"KeepId"];
    [ud synchronize];
}

+ (NSString * __nullable)getkeepId
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"KeepId"];
}

@end
