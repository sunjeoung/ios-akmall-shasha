//
//  JBroken.h
//
//  Created by Sathish Venkatisan on 12/10/13.
//

#import <Foundation/Foundation.h>

float firmwareVersion(void);
BOOL isDeviceJailbroken(void);
BOOL isAppCracked(void);
BOOL isAppStoreVersion(void);
