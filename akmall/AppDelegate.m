//
//  AppDelegate.m
//  akmall
//
//  Created by KimJinoug on 2016. 10. 19..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AppDelegate.h"
#import "AKProperty.h"
#import "AKCommonUtil.h"
#import "AKNetworkUtil.h"
#import "JBroken.h"
#import "AKNewMainViewController.h"
#import "LeftMenuController.h"
#import "AKHistoryView.h"
#import <AVFoundation/AVFoundation.h>
#import "finger/finger.h"
#import <AdBrix/AdBrix.h>

// iAD (for IGAW)
#import <AdSupport/AdSupport.h>
//2015-12-01 오창욱 애드브릭스
#import <IgaworksCore/IgaworksCore.h>
//#import <LiveOps/LiveOps.h>

@interface AppDelegate ()
{
    finger *fingerManager;
    BOOL isLeftMenuOpen;
}

@property (strong, nonatomic) NSDictionary *savedLaunchOptions;
@property (strong, nonatomic) UIImageView *blockView;
@property (strong, nonatomic) UIView *dimmedView;
@property (strong, nonatomic) AKHistoryView *historyView;
@property (strong, nonatomic) LeftMenuController *leftMenu;

@end

@implementation AppDelegate

#pragma mark - Public Method

- (void)showLeftMenu
{
//    [self.window.rootViewController.view addSubview:self.leftMenu];
    //열릴때마다 새로고침 되도록 변경 20190103 민석
    [self.window.rootViewController.view addSubview: self.leftMenu = [[LeftMenuController alloc] initWithFrame:CGRectMake(-1*kScreenBoundsWidth, 0, kScreenBoundsWidth, kScreenBoundsHeight)]];
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         self.leftMenu.frame = CGRectMake(0, 0, kScreenBoundsWidth, kScreenBoundsHeight);
                     }
                     completion:^(BOOL finished) {
                         self->isLeftMenuOpen = YES;
                     }
     ];
}

- (void)hideLeftMenu:(BOOL)animated
{
    if (animated) {
        [UIView animateWithDuration:0.5f
                         animations:^{
                             self.leftMenu.frame = CGRectMake(-1*kScreenBoundsWidth, 0, kScreenBoundsWidth, kScreenBoundsHeight);
                         }
                         completion:^(BOOL finished) {
                             [self.leftMenu removeFromSuperview];
                             self->isLeftMenuOpen = NO;
                         }
         ];
    }
    else {
        [self.leftMenu removeFromSuperview];
    }
}

- (void)leftMenuRefresh
{
    [self.leftMenu removeFromSuperview];
    self.leftMenu = nil;
    self.leftMenu = [[LeftMenuController alloc] initWithFrame:CGRectMake(-1*kScreenBoundsWidth, 0, kScreenBoundsWidth, kScreenBoundsHeight)];
}

- (void)showHistoryView
{
    _dimmedView = [[UIView alloc] initWithFrame:CGRectMake(0, START_Y, kScreenBoundsWidth, kScreenBoundsHeight-START_Y)];
    _dimmedView.backgroundColor = [UIColor blackColor];
    _dimmedView.alpha = 0.8;
    _dimmedView.hidden = YES;
    [self.window.rootViewController.view addSubview:_dimmedView];
    
    CGFloat nHistoryViewWidth = kScreenBoundsWidth * 0.8;
    CGFloat nHistoryViewHeight = kScreenBoundsHeight-START_Y;

    self.historyView = [[AKHistoryView alloc] initWithFrame:CGRectMake(kScreenBoundsWidth, 0, nHistoryViewWidth, nHistoryViewHeight)];
    [self.window.rootViewController.view addSubview:self.historyView];
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         CGFloat xPos = kScreenBoundsWidth - nHistoryViewWidth;
                         self.historyView.frame = CGRectMake(xPos, 0, nHistoryViewWidth, nHistoryViewHeight);
                     }
                     completion:^(BOOL finished) {
                         self.dimmedView.hidden = NO;
                         self.isHistoryWebView = YES;
                     }
     ];
}

- (void)closeHistoryView
{
    [UIView animateWithDuration:0.5f
                     animations:^{
                         CGRect curFrame = self.historyView.frame;
                         self.historyView.frame = CGRectMake(kScreenBoundsWidth, 0, curFrame.size.width, curFrame.size.height);
                     }
                     completion:^(BOOL finished) {
                         [self.historyView removeFromSuperview];
                         self.historyView = nil;
                         [self.dimmedView removeFromSuperview];
                         self.dimmedView = nil;
                         self.isHistoryWebView = NO;
                     }
     ];
}

#pragma mark - init

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
        }];
    }
    else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        });
    }
    
    // 실행 중이 아닐 경우 푸시로 실행 시
    if (launchOptions) {
        _savedLaunchOptions = launchOptions;
    }
    
    // IGAW
    [IgaworksCore igaworksCoreWithAppKey:@"657336955" andHashKey:@"415d451a0440411e"];
    [IgaworksCore setLogLevel:IgaworksCoreLogTrace];
    
    // 나중에 앱 업데이트할때 http://support.ad-brix.com/page/ea2b6361170f4ce28b55de129c5721f3/IDFA_Guide/kos
    /* IDFA등록! 하고 있는대 */
    if (NSClassFromString(@"ASIdentifierManager")){
        NSUUID *ifa =[[ASIdentifierManager sharedManager]advertisingIdentifier];
        BOOL isAppleAdvertisingTrackingEnalbed = [[ASIdentifierManager sharedManager]isAdvertisingTrackingEnabled];
        [IgaworksCore setAppleAdvertisingIdentifier:[ifa UUIDString] isAppleAdvertisingTrackingEnabled:isAppleAdvertisingTrackingEnalbed];
        //        NSLog(@"[ifa UUIDString] %@", [ifa UUIDString]);
    }

    [AKNetworkUtil requestPreloadWithSuccess:^(NSDictionary * _Nullable response) {

        NSDictionary *dict = response[@"data"];
        // 서버에서 받은 데이터를 로컬에 저장
        if (dict[@"hiddenHeader"]) {
            [IUserDataManager setHiddenHeader:dict[@"hiddenHeader"]];
        }
        if (dict[@"hiddenTabbar"]) {
            [IUserDataManager setHiddenTabbar:dict[@"hiddenTabbar"]];
        }
        if (dict[@"newWindow"]) {
            [IUserDataManager setNewWindow:dict[@"newWindow"]];
        }
        
        if (dict[@"searchWord"] && [dict[@"searchWord"] isKindOfClass:[NSDictionary class]]) {
            self.dictSearchInfo = dict[@"searchWord"];
            [[NSNotificationCenter defaultCenter] postNotificationName:kLoadMktMessageNotification object:nil];
        }
        
        self.isNewAlarm = NO;
        
        if (dict[@"newAlarm"] && [dict[@"newAlarm"] isKindOfClass:[NSString class]]) {
            self.isNewAlarm = [dict[@"newAlarm"] isEqualToString:@"Y"];
        }
        
        self.nCartCount = 0;
        
        if (dict[@"cartCount"] && ([dict[@"cartCount"] isKindOfClass:[NSString class]] || [dict[@"cartCount"] isKindOfClass:[NSNumber class]])) {
            self.nCartCount = [dict[@"cartCount"] intValue];
        }
        
        // 모바일 팝업
        if (dict[@"mobilePopup"]) {
            [IUserDataManager setMobilePopup:dict[@"mobilePopup"]];
        }

        
    } failure:^(NSError * _Nonnull error) {
    }];
    
    //왼쪽 메뉴 미리 생성
//    self.leftMenu = [[LeftMenuController alloc] initWithFrame:CGRectMake(-1*kScreenBoundsWidth, 0, kScreenBoundsWidth, kScreenBoundsHeight)];
    
    fingerManager = [finger sharedData];
    
    NSLog(@"%@",APPKEY);
    
    [fingerManager setAppKey:APPKEY];             //App Key
    [fingerManager setAppScrete:APPSCRETE];       //App Sec
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    /*
    dispatch_async(dispatch_get_main_queue(), ^{
        UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
        
        if (self.window) {
            self.blockView = [[UIImageView alloc] initWithImage:nil];
            self.blockView.backgroundColor = [UIColor whiteColor];
            if (self.blockView) {
                [self.window addSubview:self.blockView];
                [self.blockView mas_makeConstraints:^(MASConstraintMaker *make) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        make.edges.equalTo(self.window).with.insets(padding);
                    });
                }];
            }
        }
    });
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    /*
    if (_blockView) {
        [_blockView removeFromSuperview];
        _blockView = nil;
    }
    */
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    });
    
    
    if (isDeviceJailbroken()) {
        [AKCommonUtil showAlert:nil msg:@"고객님의 안전한 쇼핑을 위해 변경되지 않은 운영체제(순정상태)를 탑재한 단말기에 한해 서비스 이용이 가능합니다." cancelTitle:@"닫기" cancelBlock:^(UIAlertAction * _Nullable action) {
            exit(0);
        }];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    NSLog(@"url = %@\noptions = %@", url, options);
    NSDictionary *dic = [AKCommonUtil parseScheme:url.absoluteString];
    UIViewController *vc = [AKCommonUtil getVisibleController];
    NSArray *arr = [url.absoluteString componentsSeparatedByString:@"returnUrl="];
    
    @try {
        NSLog(@"deeplink url = %@", [NSString stringWithFormat:@"%@", url]);
        [IgaworksCore passOpenURL:url];
        [AdBrix commerceDeeplinkOpen: [NSString stringWithFormat:@"%@", url]];
    }@catch (NSException *exception) {
        NSLog(@"commerceDeeplinkOpen error");
    }
    @finally {
    }
    if (arr.count > 1) {
        NSString *ispURL = [AKCommonUtil urlDecode:[arr objectAtIndex:1]];
        
        // 실행 중이 아닌 경우 메인 이동 후에 이동
        if (_savedLaunchOptions) {
            [AKProperty setDeepLinkUrl:ispURL];
        } else {
            [AKCommonUtil pushWebViewController:ispURL];
            //add 20210203 p65458 앱 최초 실행시 모바일웹 상품 상세페이지에서 앱으로보기 실행시  앱 초기화 완료시 저장된 값으로 상품페이지 이동
            if ([ispURL  containsString:@"GoodsDetail.do"]) {
                [appDelegate setReturnUrl:ispURL];
            } else {
                [appDelegate setReturnUrl:@""];
            }
        }
    } else {
        if ([dic[@"command"] isEqualToString:@"home"]) {
            if (![vc isKindOfClass:[AKNewMainViewController class]]) {
                [vc.navigationController popToRootViewControllerAnimated:YES];
            }
        } else if ([dic[@"command"] isEqualToString:@"search"]) {
            if (_savedLaunchOptions) {
                [AKProperty setDeepLinkUrl:URL_SEARCH];
            } else {
                [AKCommonUtil pushWebViewController:URL_SEARCH];
            }
        } else if ([dic[@"command"] isEqualToString:@"login"]) {
            if (_savedLaunchOptions) {
                [AKProperty setDeepLinkUrl:URL_LOGIN];
            } else {
                [AKCommonUtil pushWebViewController:URL_LOGIN];
            }
        } else if ([dic[@"command"] isEqualToString:@"check"]) {
            if (_savedLaunchOptions) {
                [AKProperty setDeepLinkUrl:URL_CHECK];
            } else {
                [AKCommonUtil pushWebViewController:URL_CHECK];
            }
        } else if ([dic[@"command"] isEqualToString:@"event"]) {
            if (_savedLaunchOptions) {
                [AKProperty setDeepLinkUrl:URL_EVENT];
            } else {
                [AKCommonUtil pushWebViewController:URL_EVENT];
            }
        } else if ([dic[@"command"] isEqualToString:@"bag"]) {
            if (_savedLaunchOptions) {
                [AKProperty setDeepLinkUrl:URL_BAG];
            } else {
                [AKCommonUtil pushWebViewController:URL_BAG];
            }
        } else if ([dic[@"command"] isEqualToString:@"car"]) {
            if (_savedLaunchOptions) {
                [AKProperty setDeepLinkUrl:URL_CAR];
            } else {
                [AKCommonUtil pushWebViewController:URL_CAR];
            }
        } else if ([dic[@"command"] isEqualToString:@"feed"]) {
            if (_savedLaunchOptions) {
                [AKProperty setDeepLinkUrl:URL_MY_FEEDLIST];
            } else {
                [AKCommonUtil pushWebViewController:URL_MY_FEEDLIST];
            }
        }
    }
    
    _savedLaunchOptions = nil;
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //최초 디바이스 등록 체크
    NSString *userDeviceToken = [AKProperty getDeviceToken];
    const char *data = [deviceToken bytes];
    NSMutableString *token = [NSMutableString string];
    
    //[LiveOpsPush setDeviceToken:deviceToken];
    
    for (int i = 0; i < [deviceToken length]; i++) {
        [token appendFormat:@"%02.2hhX", data[i]];
    }
    
    NSLog(@"token = %@", token);
    
    //메모리에 토큰값이 존재하지 않거나 존재하는토큰값이 서버에서 넘겨준 값이랑 틀릴경우에.. 재등록..
    if ([userDeviceToken length] == 0 || ![userDeviceToken isEqualToString:token]) {
         [AKProperty setDeviceToken:token];
//        [AKNetworkUtil requestRegisterToken:token success:^(NSDictionary * _Nullable response) {
//            [AKProperty setDeviceToken:token];
//            [AKNetworkUtil requestUpdateDeviceUser:token success:^(NSDictionary * _Nullable response) {
//            } failure:^(NSError * _Nonnull error) {
//            }];
//        } failure:^(NSError * _Nonnull error) {
//        }];
    } else {
        [AKNetworkUtil requestUpdateDeviceUser:token success:^(NSDictionary * _Nullable response) {
        } failure:^(NSError * _Nonnull error) {
        }];
    }
    
    /*핑거푸시에 기기등록*/
    [fingerManager registerUserWithBlock:deviceToken :^(NSString *posts, NSError *error) {
        NSLog(@"토큰 : %@",self->fingerManager.getToken);
        NSLog(@"토큰idx : %@",self->fingerManager.getDeviceIdx);
        NSLog(@"기기등록 posts : %@ / error : %@",posts,error);
    }];
    
    //기존 서버에 존재하던 토큰 삭제
    [AKNetworkUtil deleteTokenWithSuccess:nil failure:nil];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"error = %@", error);
    [AKProperty setDeviceToken:@""];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler {
    NSString *strPushID = userInfo[@"PID"];
    NSString *weblink = userInfo[@"weblink"];
   
    NSDictionary* dicCode = [finger receviveCode:userInfo];
    if ([weblink containsString:@"?"]) {
        weblink = [NSString stringWithFormat:@"%@&mode=%@&msgTag=%@&tid=%@", userInfo[@"weblink"], dicCode[@"PT"], userInfo[@"msgTag"], [[finger sharedData] getDeviceIdx]];
    }
    else {
        weblink = [NSString stringWithFormat:@"%@?mode=%@&msgTag=%@&tid=%@", userInfo[@"weblink"], dicCode[@"PT"], userInfo[@"msgTag"], [[finger sharedData] getDeviceIdx]];
        
    }

    NSLog(@"Push Receive %@",weblink);
    //위젯 처리
     if ([userInfo[@"aps"][@"alert"] isKindOfClass:[NSDictionary class]]) {
        [AKProperty setNotice: userInfo[@"aps"][@"alert"][@"body"]];
     }else{
         [AKProperty setNotice: userInfo[@"aps"][@"alert"]];
     }
    
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateInactive) {
        
        // 실행 중이 아닌 경우 메인 이동 후에 푸시 상세로 이동
        if (_savedLaunchOptions) {
            _savedLaunchOptions = nil;
            
            if (strPushID.length != 0) { // 기존 푸쉬 처리
                
            } else {
                if (isLeftMenuOpen) {
                    [self.leftMenu removeFromSuperview];
                    isLeftMenuOpen = NO;
                }
                [AKProperty setDeepLinkUrl:weblink];
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                [[finger sharedData] requestPushCheckWithBlock:userInfo :^(NSString *posts, NSError *error) {
                    NSLog(@"읽음 처리 posts : %@ / error : %@",posts,error);
                }];
            }
        } else {
            if (strPushID.length != 0) { // 기존 푸쉬 처리
                
            } else {
                if (isLeftMenuOpen) {
                    [self.leftMenu removeFromSuperview];
                    isLeftMenuOpen = NO;
                }
                [AKCommonUtil pushWebViewController:weblink];
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                [[finger sharedData] requestPushCheckWithBlock:userInfo :^(NSString *posts, NSError *error) {
                    NSLog(@"읽음 처리 posts : %@ / error : %@",posts,error);
                }];
            }
        }
        
    } else {
        if (strPushID.length != 0) { // 기존 푸쉬 처리

            
        } else { // finger
            if ([userInfo[@"aps"][@"alert"] isKindOfClass:[NSDictionary class]]) {
                //타이틀이 있을경우
                
                [AKCommonUtil showAlert:userInfo[@"aps"][@"alert"][@"title"] msg:userInfo[@"aps"][@"alert"][@"body"] cancelTitle:@"닫기" cancelBlock:^(UIAlertAction * _Nullable action) {
                } confirmTitle:@"보기" confirmBlock:^(UIAlertAction * _Nullable action) {
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    
                    if (self->isLeftMenuOpen) {
                        [self.leftMenu removeFromSuperview];
                        self->isLeftMenuOpen = NO;
                    }
                    
                    [AKCommonUtil pushWebViewController:weblink];
                    [[finger sharedData] requestPushCheckWithBlock:userInfo :^(NSString *posts, NSError *error) {
                        NSLog(@"읽음 처리 posts : %@ / error : %@",posts,error);
                    }];
                }];
                
            }else {
                //타이틀이 없을경우
                [AKCommonUtil showAlert:nil msg:userInfo[@"aps"][@"alert"] cancelTitle:@"닫기" cancelBlock:^(UIAlertAction * _Nullable action) {
                } confirmTitle:@"보기" confirmBlock:^(UIAlertAction * _Nullable action) {
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    
                    if (self->isLeftMenuOpen) {
                        [self.leftMenu removeFromSuperview];
                        self->isLeftMenuOpen = NO;
                    }
                    
                    [AKCommonUtil pushWebViewController:weblink];
                    [[finger sharedData] requestPushCheckWithBlock:userInfo :^(NSString *posts, NSError *error) {
                        NSLog(@"읽음 처리 posts : %@ / error : %@",posts,error);
                    }];
                }];
                
            }
        }
    }
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    [self application:[UIApplication sharedApplication] didReceiveRemoteNotification:notification.request.content.userInfo fetchCompletionHandler:^(UIBackgroundFetchResult result) {
        
    }];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    [self application:[UIApplication sharedApplication] didReceiveRemoteNotification:response.notification.request.content.userInfo fetchCompletionHandler:^(UIBackgroundFetchResult result) {
        
    }];
}

//퀵액션 버튼 실행시 사용
- (void)application:(UIApplication *)application performActionForShortcutItem:(nonnull UIApplicationShortcutItem *)shortcutItem completionHandler:(nonnull void (^)(BOOL))completionHandler {
    NSString *url;
    
    if ([shortcutItem.type isEqualToString:@"com.akmall.touchAKmall.item0"]) {
        url = [NSString stringWithFormat:@"%@/search/SearchMain.do?isAkApp=iPhone", BASE_URL];
    } else if ([shortcutItem.type isEqualToString:@"com.akmall.touchAKmall.item1"]) {
        url = [NSString stringWithFormat:@"%@/mypage/OrderDeliInquiry.do?isAkApp=iPhone", BASE_URL];
    } else if ([shortcutItem.type isEqualToString:@"com.akmall.touchAKmall.item2"]) {
        url = [NSString stringWithFormat:@"%@/akplaza/AKPlazaMain.do?isAkApp=iPhone", BASE_URL];
    }
    
    if (_savedLaunchOptions) {
        [AKProperty setDeepLinkUrl:url];
    } else {
        [AKCommonUtil pushWebViewController:url];
    }
    
    _savedLaunchOptions = nil;
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    NSLog(@"applicationDidReceiveMemoryWarning");
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

@end
