//
//  AppDelegate.h
//  akmall
//
//  Created by KimJinoug on 2016. 10. 19..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>



@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSDictionary *dictSearchInfo;
@property (nonatomic, assign) BOOL isNewAlarm;
@property (nonatomic, assign) int nCartCount;
@property (nonatomic, strong) NSString *imgHistoryUrl;
@property (nonatomic, assign) BOOL isHistoryWebView;
@property (nonatomic, strong) NSString *returnUrl;

- (void)showLeftMenu;
- (void)hideLeftMenu:(BOOL)animated;
- (void)leftMenuRefresh;

- (void)showHistoryView;
- (void)closeHistoryView;


@end

