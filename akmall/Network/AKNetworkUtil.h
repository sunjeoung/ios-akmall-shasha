//
//  AKNetworkUtil.h
//  akmall
//
//  Created by KimJinoug on 2016. 10. 20..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "AKCommonUtil.h"

typedef void (^successBlock)(NSDictionary * __nullable response);
typedef void (^failureBlock)(NSError * __nonnull error);

#define _successBlock       successBlock _Nullable
#define _failureBlock       failureBlock _Nullable

@interface AKNetworkUtil : NSObject

+ (void)requestAppStoreVersion:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestCategoryBest:(int)page param:(NSMutableDictionary * __nonnull)param success:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestCategoryNew:(int)page param:(NSMutableDictionary * __nonnull)param success:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestCategoryItem:(int)page param:(NSMutableDictionary * __nonnull)param success:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestCategoryAknow:(int)page param:(NSMutableDictionary * __nonnull)param success:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestUserInfoWithSuccess:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestRegisterToken:(NSString * __nonnull)token success:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestUpdateDeviceUser:(NSString * __nonnull)token success:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestUpdateDeny:(NSString * __nonnull)start end:(NSString * __nonnull)end success:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestUpdateShoppingAlarm:(BOOL)isAlarm success:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestUpdateBuyAlarm:(BOOL)isAlarm success:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestUpdateNosoundAlarm:(BOOL)isAlarm success:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestDenyListWithSuccess:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestLogoutWithSuccess:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestIntroWithSuccess:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestPowerLink:(NSString * __nonnull)url success:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestVersionCheckWithSuccess:(_successBlock)success failure:(_failureBlock)failure;

#pragma mark - add by hans
+ (void)requestPreloadWithSuccess:(_successBlock)success failure:(_failureBlock)failure;
+ (void)requestTabMenuListWithSuccess:(_successBlock)success failure:(_failureBlock)failure;


#pragma mark - add by ksh
+ (void)requestPushAlarmWithSuccess:(BOOL)isAlarm success:(_successBlock)success failure:(_failureBlock)failure;

+ (void)deleteTokenWithSuccess:(_successBlock)success failure:(_failureBlock)failure;
@end
