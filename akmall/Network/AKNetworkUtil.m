//
//  AKNetworkUtil.m
//  akmall
//
//  Created by KimJinoug on 2016. 10. 20..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKNetworkUtil.h"
#import "AKProperty.h"

@implementation AKNetworkUtil

// 통신 매니저 생성(html)
+ (AFHTTPSessionManager *)getManager {
    return [self getManager:@"text/html"];
}

// 통신 매니저 생성
+ (AFHTTPSessionManager *)getManager:(NSString *)contentType {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithArray:@[contentType]]];
    
    return manager;
}

// 응답 공통 처리
+ (BOOL)onCommonResponse:(NSDictionary *)response showErrorAlert:(BOOL)isShowAlert {
    NSString *resultCode = response[@"resultCode"];
    NSString *resultMsg = response[@"resultMsg"];
    
    if ([resultCode isEqualToString:@"0000"]) {
        return YES;
    } else {
        if (isShowAlert && resultMsg && ([resultMsg isEqualToString:@""] == NO)) {
            [AKCommonUtil showAlertWithMsg:resultMsg];
        }
    }
    
    return NO;
}

+ (void)sendRequest:(NSString *)urlString parameters:(NSDictionary *)params success:(void (^)(NSDictionary *response))success failure:(void (^)(NSError * __nonnull))failure {
    [self sendRequest:urlString parameters:params success:success failure:failure showErrorAlert:YES requestTimeout:0];
}

// 요청 공통 처리
+ (void)sendRequest:(NSString *)urlString parameters:(NSDictionary *)params success:(void (^)(NSDictionary *response))success failure:(void (^)(NSError * __nonnull))failure showErrorAlert:(BOOL)isShowAlert requestTimeout:(NSTimeInterval)timeout {
    AFHTTPSessionManager *manager = [self getManager];
    NSString *url = [AKCommonUtil getRequestUrl:urlString];
    
    NSLog(@"url = %@", url);
    NSLog(@"isShowAlert : %@", (isShowAlert ? @"YES" : @"NO")); //output : YES
    NSLog(@"params = %@", params);
    [[AKCommonUtil getVisibleController] startIndicator];
    
    if (timeout != 0) {
        [manager.requestSerializer setTimeoutInterval:timeout];
    }
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        [[AKCommonUtil getVisibleController] stopIndicator];

        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dic = (NSDictionary *)responseObject;
            
            if (!dic[@"resultCode"] || [dic[@"resultCode"] isEqualToString:@"0000"]) {
                if (success) {
                    success(dic);
                }
            }
            else {
                [self onCommonResponse:dic showErrorAlert:isShowAlert];
            }
        }
        else if (failure) {
            NSError *error = [NSError errorWithDomain:@"akmall" code:-1 userInfo:@{NSLocalizedDescriptionKey: @"ResponseObject is invalid."}];
            
            failure(error);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [[AKCommonUtil getVisibleController] stopIndicator];
        
        NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
        [inputFormatter setDateFormat:@"yyyyMMddHHmm"];
        NSString *date = [inputFormatter stringFromDate:[NSDate date]];
        unsigned long long unsignedLongLongValue = strtoull([date UTF8String], NULL, 0);

         // 점검일 체크
        if(unsignedLongLongValue >= 202001240000 && unsignedLongLongValue < 202001260000) {
           
            NSLog(@"네트워크 오류 얼럿 안함");
        }else {
            if (isShowAlert) {
                NSLog(@"Error: Error");
                [AKCommonUtil showAlert:nil msg:@"네트워크에 접속할 수 없습니다. 네트워크 연결상태를 확인해 주세요." cancelTitle:@"앱종료" cancelBlock:^(UIAlertAction * _Nullable action) {
                    exit(0);
                } confirmTitle:@"재시도" confirmBlock:^(UIAlertAction * _Nullable action) {
                    [self sendRequest:urlString parameters:params success:success failure:failure];
                }];
            }
        }
        
        
        
        NSLog(@"Error: %@", error);
        
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark - app store version

// 앱스토어에서 앱 정보 가져오기
+ (void)requestAppStoreVersion:(_successBlock)success failure:(_failureBlock)failure {
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *appID = infoDictionary[@"CFBundleIdentifier"];
    AFHTTPSessionManager *manager = [self getManager:@"text/javascript"];
    
    [[AKCommonUtil getVisibleController] startIndicator];
    [manager GET:[NSString stringWithFormat:@"http://itunes.apple.com/kr/lookup?bundleId=%@", appID] parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        [[AKCommonUtil getVisibleController] stopIndicator];
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [[AKCommonUtil getVisibleController] stopIndicator];
        [AKCommonUtil showAlertWithMsg:@"버전 정보를 가져오는 중 네트워크 오류가 발생하였습니다."];
        
        if (failure) {
            failure(error);
        }
    }];
}

// 카테고리 베스트 목록 요청
+ (void)requestCategoryBest:(int)page param:(NSMutableDictionary *)param success:(_successBlock)success failure:(_failureBlock)failure {
    param[@"native"] = @"Y";
    param[@"pageIdx"] = [NSString stringWithFormat:@"%d", page];
    [self sendRequest:URL_CATEGORY_BEST parameters:param success:success failure:failure];
}

// 카테고리 신상품 목록 요청
+ (void)requestCategoryNew:(int)page param:(NSMutableDictionary *)param success:(_successBlock)success failure:(_failureBlock)failure {
    param[@"native"] = @"Y";
    param[@"pageIdx"] = [NSString stringWithFormat:@"%d", page];
    [self sendRequest:URL_CATEGORY_NEW parameters:param success:success failure:failure];
}

// 카테고리 아이템 목록 요청
+ (void)requestCategoryItem:(int)page param:(NSMutableDictionary *)param success:(_successBlock)success failure:(_failureBlock)failure {
    param[@"native"] = @"Y";
    param[@"pageIdx"] = [NSString stringWithFormat:@"%d", page];
    [self sendRequest:URL_CATEGORY_ITEM parameters:param success:success failure:failure];
}

// 카테고리 AKNOW 목록 요청
+ (void)requestCategoryAknow:(int)page param:(NSMutableDictionary *)param success:(_successBlock)success failure:(_failureBlock)failure {
    param[@"native"] = @"Y";
    param[@"pageIdx"] = [NSString stringWithFormat:@"%d", page];
    [self sendRequest:URL_CATEGORY_AKNOW parameters:param success:success failure:failure];
}

// 사용자 정보 요청
+ (void)requestUserInfoWithSuccess:(_successBlock)success failure:(_failureBlock)failure {
    [self sendRequest:URL_USER_INFO parameters:nil success:success failure:failure];
}

// 푸시 키 등록 요청
+ (void)requestRegisterToken:(NSString *)token success:(_successBlock)success failure:(_failureBlock)failure {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    param[@"act"] = @"DeviceRegist";
    param[@"phonetype"] = @"0";
    param[@"version"] = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    param[@"appid"] = [AKCommonUtil getUUID];
    param[@"token"] = [AKCommonUtil checkNil:token];
    param[@"returnType"] = @"json";
    [self sendRequest:URL_APP_LIB parameters:param success:success failure:failure];
}

// 사용자 업데이트 요청
+ (void)requestUpdateDeviceUser:(NSString *)token success:(_successBlock)success failure:(_failureBlock)failure {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    param[@"act"] = @"updateDeviceUser";
    param[@"phonetype"] = @"0";
    param[@"version"] = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    param[@"appid"] = [AKCommonUtil getUUID];
    param[@"token"] = [AKCommonUtil checkNil:token];
    param[@"userid"] = [AKCommonUtil checkNil:[AKCommonUtil getCookieValue:@"keep_id"]];
    param[@"returnType"] = @"json";
    [self sendRequest:URL_APP_LIB parameters:param success:success failure:failure];
}

// 알림 금지 시간 설정
+ (void)requestUpdateDeny:(NSString *)start end:(NSString *)end success:(_successBlock)success failure:(_failureBlock)failure {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    param[@"act"] = @"updateDeny";
    param[@"type"] = @"denytime";
    param[@"starthh"] = [AKCommonUtil checkNil:start];
    param[@"endhh"] = [AKCommonUtil checkNil:end];
    param[@"token"] = [AKProperty getDeviceToken];
    param[@"returnType"] = @"json";
    [self sendRequest:URL_APP_LIB parameters:param success:success failure:failure];
}

// 쇼핑 알림 설정
+ (void)requestUpdateShoppingAlarm:(BOOL)isAlarm success:(_successBlock)success failure:(_failureBlock)failure {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    param[@"act"] = @"updateDeny";
    param[@"type"] = @"denytime";
    param[@"shopping_alarm_yn"] = isAlarm ? @"Y" : @"N";
    param[@"token"] = [AKProperty getDeviceToken];
    param[@"returnType"] = @"json";
    [self sendRequest:URL_APP_LIB parameters:param success:success failure:failure];
}

// 구매 알림 설정
+ (void)requestUpdateBuyAlarm:(BOOL)isAlarm success:(_successBlock)success failure:(_failureBlock)failure {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    param[@"act"] = @"updateDeny";
    param[@"type"] = @"denytime";
    param[@"buy_alarm_yn"] = isAlarm ? @"Y" : @"N";
    param[@"token"] = [AKProperty getDeviceToken];
    param[@"returnType"] = @"json";
    [self sendRequest:URL_APP_LIB parameters:param success:success failure:failure];
}

// 무음 알림 설정
+ (void)requestUpdateNosoundAlarm:(BOOL)isAlarm success:(_successBlock)success failure:(_failureBlock)failure {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    param[@"act"] = @"updateDeny";
    param[@"type"] = @"denytime";
    param[@"nosound_yn"] = isAlarm ? @"Y" : @"N";
    param[@"token"] = [AKProperty getDeviceToken];
    param[@"returnType"] = @"json";
    [self sendRequest:URL_APP_LIB parameters:param success:success failure:failure];
}

// 알림 설정 목록 받아오기
+ (void)requestDenyListWithSuccess:(_successBlock)success failure:(_failureBlock)failure {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    param[@"act"] = @"getDenyList";
    param[@"token"] = [AKProperty getDeviceToken];
    param[@"returnType"] = @"json";
    
    [self sendRequest:URL_APP_LIB parameters:param success:success failure:failure];
}

// 로그아웃 요청
+ (void)requestLogoutWithSuccess:(_successBlock)success failure:(_failureBlock)failure {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    param[@"native"] = @"Y";
    [self sendRequest:URL_LOGOUT parameters:param success:success failure:failure];
}

// 인트로 화면 요청
+ (void)requestIntroWithSuccess:(_successBlock)success failure:(_failureBlock)failure {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    param[@"act"] = @"appIntro";
    param[@"returnType"] = @"json";
    [self sendRequest:URL_APP_LIB parameters:param success:success failure:failure showErrorAlert:NO requestTimeout:3];
}

// 파워 링크 요청
+ (void)requestPowerLink:(NSString *)url success:(_successBlock)success failure:(_failureBlock)failure {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    NSArray *arr = [url componentsSeparatedByString:@"?"];
    
    url = arr[0];
    
    if (arr.count > 1) {
        NSArray *p = [arr[1] componentsSeparatedByString:@"&"];
        
        for (NSString *str in p) {
            NSArray *p1 = [str componentsSeparatedByString:@"="];
            
            param[p1[0]] = p1[1];
        }
    }
    
    [self sendRequest:url parameters:param success:success failure:failure];
}

// 버전 체크 요청
+ (void)requestVersionCheckWithSuccess:(_successBlock)success failure:(_failureBlock)failure {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    param[@"act"] = @"versionInfo";
    param[@"phonetype"] = @"0";
    param[@"returnType"] = @"json";
    [self sendRequest:URL_APP_LIB parameters:param success:success failure:failure];
}

#pragma mark - add by hans

+ (void)requestPreloadWithSuccess:(_successBlock)success failure:(_failureBlock)failure {
    
    [self sendRequest:URL_PRELOAD parameters:nil success:success failure:failure];
}

// 메인 탭 메뉴 목록 가져오기
+ (void)requestTabMenuListWithSuccess:(_successBlock)success failure:(_failureBlock)failure {

    [self sendRequest:URL_TABMENU_LIST parameters:nil success:success failure:failure];
}

#pragma mark - add by ksh

//TODO: 안드로이드 기준으로 구현, 파라미터 확인 필요 !!!
+ (void)requestPushAlarmWithSuccess:(BOOL)isAlarm success:(successBlock)success failure:(failureBlock)failure {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    param[@"act"]               = @"DeviceRegist";
    param[@"appid"]             = [AKCommonUtil getUUID];;
    param[@"deny_all"]          = isAlarm ? @"no" : @"yes";
    param[@"deny"]              = @"0";
    param[@"phonetype"]         = @"0";
    param[@"shopping_alarm_yn"] = isAlarm ? @"Y" : @"N";
    param[@"buy_alarm_yn"]      = isAlarm ? @"Y" : @"N";
    param[@"token"]             = [AKProperty getDeviceToken];
    param[@"returnType"]        = @"json";
    param[@"version"]           = APP_VERSION;

    [self sendRequest:URL_APP_LIB parameters:param success:success failure:failure];
}

+ (void)deleteTokenWithSuccess:(_successBlock)success failure:(_failureBlock)failure {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    param[@"act"] = @"updateUseYn";
    param[@"appid"] = [AKCommonUtil getUUID];
    param[@"returnType"] = @"json";
    [self sendRequest:URL_APP_LIB parameters:param success:success failure:failure];
}
@end
