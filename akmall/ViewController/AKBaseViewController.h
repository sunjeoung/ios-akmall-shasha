//
//  AKBaseViewController.h
//  akmall
//
//  Created by KimJinoug on 2016. 10. 20..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Masonry.h"
#import "AKNetworkUtil.h"

@interface AKBaseViewController : UIViewController

@property (strong, nonatomic) UIProgressView *topProgressView;

- (void)startIndicator;
- (void)stopIndicator;

- (void)hideButtons:(BOOL)isHide;
- (void)addBottomView;
- (void)animationUpFloatButtons:(BOOL)isShowTopBtn;
- (void)animationDownFloatButtons:(BOOL)isShowTopBtn;
- (void)clickBackBtn;
- (void)clickTopBtn;


@end
