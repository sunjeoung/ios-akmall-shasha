//
//  AKMainWebViewController.m
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 30..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKMainWebViewController.h"
#import "AKProperty.h"
#import "IgawTrackingHelper.h"
#import "AKWebView.h"

@interface AKMainWebViewController ()
{
    
}

@property (weak, nonatomic) IBOutlet UIView *contentsView;

@property (weak, nonatomic) AKWebView *webView;
@property (assign) BOOL dontShowPopup;

@end

@implementation AKMainWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self addBottomView];
    
    
    _schemeManager = [[AKSchemeManager alloc] init];
    _schemeManager.delegate = self;
    _webView.mediaPlaybackRequiresUserAction = NO;
    _webView.allowsInlineMediaPlayback = YES;
    _webView.keyboardDisplayRequiresUserAction = NO;
    _progressProxy = [[NJKWebViewProgress alloc] init]; // instance variable
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[AKCommonUtil getRequestUrl:URL_MAIN]]]];
    
    [self hideButtons:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSString *pushId = [AKProperty getPushId];
    NSString *deepLinkUrl = [AKProperty getDeepLinkUrl];
    
    if (pushId && ![pushId isEqualToString:@""]) {
        NSString *url = [[NSString stringWithFormat:@"%@%@", PUSH_DETAIL_URL, pushId] stringByRemovingPercentEncoding];
        
        [AKCommonUtil pushWebViewController:url];
        [AKProperty setPushId:@""];
    } else if (deepLinkUrl && ![deepLinkUrl isEqualToString:@""]) {
        [AKCommonUtil pushWebViewController:deepLinkUrl];
        [AKProperty setDeepLinkUrl:@""];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *url = request.URL.absoluteString;
    
    return [_schemeManager processScheme:webView url:url categoryType:CATEGORY_MAIN];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    // 20170207, target=_blank 사용 안하기로 함.
//    [AKCommonUtil checkTargetBlank:webView];
    // 결제완료 Tracking
    [IgawTrackingHelper trackingPurchase:webView];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    if ([AKCommonUtil checkWebViewError:error]) {
        [AKCommonUtil showAlert:nil msg:@"네트워크에 접속할 수 없습니다. 네트워크 연결상태를 확인해 주세요." cancelTitle:@"앱종료" cancelBlock:^(UIAlertAction * _Nullable action) {
            exit(0);
        } confirmTitle:@"재시도" confirmBlock:^(UIAlertAction * _Nullable action) {
            [webView reload];
        }];
    }
}

#pragma mark - NJKWebViewProgressDelegate

- (void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress {
    [self.topProgressView setProgress:progress animated:NO];
    
    if (progress == 1) {
        [self performSelector:@selector(hiddenTopProgressView) withObject:nil afterDelay:0.1];
    } else {
        self.topProgressView.hidden = NO;
    }
}

#pragma mark - Private

- (void)hiddenTopProgressView {
    self.topProgressView.hidden = YES;
}

#pragma mark - AKSchemeManagerDelegate

- (void)sendMainPopup:(NSArray *)list {
    for (NSDictionary *dic in list) {
        // native popup
        if ([dic[@"popup_div_code"] intValue] == 110) {
            if (!_dontShowPopup && ![[AKProperty getPopupTodayDate:dic[@"mobile_popup_id"]] isEqualToString:[AKCommonUtil getTodayString:@"yyyyMMdd"]]) {
                [AKCommonUtil showFullPopup:self.view data:dic];
                _dontShowPopup = YES;
            }
        }
    }
}

- (void)callWishPopup:(NSString *)goodsId {
    [AKCommonUtil showPopupWebView:self.view url:[AKCommonUtil getRequestUrl:[NSString stringWithFormat:@"%@&goods_id=%@", URL_LIKE, goodsId]]];
}

#pragma mark - Public

- (void)refreshWeb:(NSString *)url {
    if (url && ![url isEqualToString:@""]) {
        if ([url hasPrefix:@"http"]) {
            [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
        } else {
            [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[AKCommonUtil getRequestUrl:url]]]];
        }
    } else {
        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[AKCommonUtil getRequestUrl:URL_MAIN]]]];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
