//
//  AKSpeechViewController.h
//  akmall
//
//  Created by KimJinoug on 2016. 10. 24..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKBaseViewController.h"

#if !TARGET_OS_SIMULATOR
#import <KakaoNewtoneSpeech/KakaoNewtoneSpeech.h>
#endif

#if TARGET_OS_SIMULATOR
@interface AKSpeechViewController : AKBaseViewController
#else
@interface AKSpeechViewController : AKBaseViewController <MTSpeechRecognizerDelegate>
#endif



@end
