//
//  AKIntroWebViewController.h
//  AKMall
//
//  Created by KimJinoug on 2016. 12. 6..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKBaseViewController.h"
#import "AKCommonUtil.h"

@interface AKIntroWebViewController : AKBaseViewController <AKVersionPopupViewDelegate>

@end
