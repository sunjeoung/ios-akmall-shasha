//
//  AKSpeechViewController.m
//  akmall
//
//  Created by KimJinoug on 2016. 10. 24..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKSpeechViewController.h"
#import "SubViewController.h"

#define API_KEY     @"5334c031510f3420309c9cbd0a0b1dd8"

@interface AKSpeechViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *speechBack;
@property (weak, nonatomic) IBOutlet UIButton *speechBtn;

#if !TARGET_OS_SIMULATOR
@property (strong, nonatomic) MTSpeechRecognizerClient *speechRecognizerClient;
#endif

@property (assign) BOOL isRecording;
@property (strong, nonatomic) NSString *resultText;

@end

@implementation AKSpeechViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
#if !TARGET_OS_SIMULATOR
    if (![MTSpeechRecognizer isRecordingAvailable]) {
        [AKCommonUtil showAlert:nil msg:@"마이크 접근 허용을 켜주세요." cancelBlock:^(UIAlertAction * _Nullable action) {
            [self.navigationController popViewControllerAnimated:YES];
        } confirmBlock:^(UIAlertAction * _Nullable action) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
    }
#endif
    
    NSMutableArray *images = [NSMutableArray array];
    
    for (int i = 1; i <= 72; i++) {
        [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"loading_%02d", i]]];
    }
    
    _speechBack.animationImages = images;
    _speechBack.animationDuration = 5;
    _speechBack.animationRepeatCount = 0;
    
    [self clickSpeech:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
#if !TARGET_OS_SIMULATOR
    [_speechRecognizerClient cancelRecording];
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)initSpeech {
#if !TARGET_OS_SIMULATOR
    NSDictionary *config = @{SpeechRecognizerConfigKeyApiKey: API_KEY, SpeechRecognizerConfigKeyServiceType: SpeechRecognizerServiceTypeWeb};
    
    _speechRecognizerClient = [[MTSpeechRecognizerClient alloc] initWithConfig:config];
    _speechRecognizerClient.delegate = self;
#endif
}

- (void)startRecording {
    _isRecording = YES;
    [_speechBtn setSelected:YES];
    [self initSpeech];
#if !TARGET_OS_SIMULATOR
    [_speechRecognizerClient startRecording];
#endif
    [_speechBack startAnimating];
}

- (void)stopRecording {
    _isRecording = NO;
    [_speechBtn setSelected:NO];
#if !TARGET_OS_SIMULATOR
    [_speechRecognizerClient stopRecording];
#endif
    [_speechBack stopAnimating];
}

#pragma mark - IBAction

- (IBAction)clickSpeech:(id)sender {
    if (_isRecording) {
        [self stopRecording];
    } else {
        [self startRecording];
    }
}

#pragma mark - MTSpeechRecognizerDelegate

#if !TARGET_OS_SIMULATOR
- (void)onResults:(NSArray *)results confidences:(NSArray *)confidences marked:(BOOL)marked {
    NSLog(@"results = %@", results);
    NSLog(@"confidences = %@", confidences);
    NSLog(@"marked = %d", marked ? 1 : 0);
    _resultText = results[0];
    NSLog(@"_resultText = %@", _resultText);

    [self stopRecording];
    [self performSegueWithIdentifier:@"ExitSpeech" sender:self];
}

- (void)onError:(MTSpeechRecognizerError)errorCode message:(NSString *)message {
    [self stopRecording];
}

- (void)onAudioLevel:(float)audioLevel {
}

- (void)onBeginningOfSpeech {
}

- (void)onReady {
}

- (void)onEndOfSpeech {
}

- (void)onPartialResult:(NSString *)partialResult {
}

- (void)onFinished {
}

#endif

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ExitSpeech"]) {
        SubViewController *vc = (SubViewController *)segue.destinationViewController;
        vc.speechResult = _resultText;
    }
}

@end
