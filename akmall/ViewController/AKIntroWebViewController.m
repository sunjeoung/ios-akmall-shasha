//
//  AKIntroWebViewController.m
//  AKMall
//
//  Created by KimJinoug on 2016. 12. 6..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKIntroWebViewController.h"
#import "AKNewMainViewController.h"
#import "IntroPopupController.h"

@interface AKIntroWebViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *defaultIntro;

@property (assign) float dtime;
@property (assign) BOOL isCheckedVersion;
@property (assign) BOOL isGoMain;
@property (strong, nonatomic) NSString *btnType;
@property (strong, nonatomic) NSString *link;

@end

@implementation AKIntroWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor blackColor];
    _defaultIntro.hidden = YES;
    
    [AKNetworkUtil requestIntroWithSuccess:^(NSDictionary * _Nullable response) {
        NSDictionary *resultDatas = response[@"resultDatas"];
        
        if ([resultDatas[@"use_yn"] isEqualToString:@"Y"]) {
            self.defaultIntro.hidden = NO;
            self.defaultIntro.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:resultDatas[@"link"]]]];
            self.dtime = [resultDatas[@"dtime"] floatValue];
        }
        [self checkVersion];
    } failure:^(NSError * _Nonnull error) {
       NSLog(@"requestIntroWithSuccess failure");
       NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
       [inputFormatter setDateFormat:@"yyyyMMddHHmm"];
       NSString *date = [inputFormatter stringFromDate:[NSDate date]];
       unsigned long long unsignedLongLongValue = strtoull([date UTF8String], NULL, 0);

        // 점검일 체크
        // lib.do?act=versionInfo 실패시에만 체크
       if(unsignedLongLongValue >= 202001240000 && unsignedLongLongValue < 202001260000) {
          [AKCommonUtil showAlert:@"서비스 일시 중단 안내" msg:@"당사 전산시스템 작업으로 인하여 서비스가 일시 중단되오니, 양해 부탁드립니다. 보다 나은 서비스로 보답하겠습니다. 감사합니다. \n중단일시\n1/24(금) 00시 ~ 1/25(토) 24시" cancelTitle:@"앱종료" cancelBlock:^(UIAlertAction * _Nullable action) {
               exit(0);
           }];
           self.defaultIntro.hidden = NO;
           NSLog(@"requestIntroWithSuccess 서비스 일시 중단 안내");
       }else{
           self.defaultIntro.hidden = NO;
           [self checkVersion];
       }

    }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - Private

- (void)checkVersion {
    [AKNetworkUtil requestVersionCheckWithSuccess:^(NSDictionary * _Nullable response) {
        NSDictionary *resultDatas = response[@"resultDatas"];
        NSString *lVersion = [resultDatas[@"LASTEST_VERSION"] stringByReplacingOccurrencesOfString:@"." withString:@""];
        NSString *deviceVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] stringByReplacingOccurrencesOfString:@"." withString:@""];
        
        if ([resultDatas[@"MUST_YN"] isEqualToString:@"Y"] && [deviceVersion integerValue] < [lVersion integerValue]) {
            self.btnType = resultDatas[@"BTN_TYPE"];
            self.link = resultDatas[@"LINK"];
            [AKCommonUtil showVersionPopup:self.view delegate:self type:self.btnType];
        } else {
            [self performSelector:@selector(passVersionPopup) withObject:nil afterDelay:1.5f];
        }
    } failure:^(NSError * _Nonnull error) {
        self.isCheckedVersion = YES;
        [self performSelector:@selector(passVersionPopup) withObject:nil afterDelay:1.5f];
    }];
}

- (void)passVersionPopup {
    _isCheckedVersion = YES;
    [self gotoMain];
}

- (BOOL)shouldBeShowIntroPushAlarm:(NSDate *)pastDate {
    if (pastDate == nil) {
        return YES;
    }

    NSDate *nowDate = [NSDate date];

    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger calendarUnit = NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;

    NSDateComponents *dateComp = [calendar components: calendarUnit
                                             fromDate: nowDate
                                               toDate: pastDate
                                              options:0];
    NSInteger day = -(dateComp.day);
    // 30일 이후에 표시
    if (day > 30) {
        return YES;
    }
    
    //fingerPush 최초 진입 체크
    if (![IUserDataManager getFingerPushShow]) {
        return YES;
    }
    
    
    return NO;
}

- (void)gotoMain {
    if (_isCheckedVersion) {
        [UIApplication sharedApplication].statusBarHidden = NO;

        BOOL show = [self shouldBeShowIntroPushAlarm:[IUserDataManager getDidTouchPushAlarmDate]];
        
        if([IUserDataManager getShowRights] && !show) {
            AKNewMainViewController *controller = [[AKNewMainViewController alloc] init];
            [self.navigationController setViewControllers:@[controller] animated:YES];
        }
        else if (![IUserDataManager getShowRights]) {
            IntroPopupController *controller = [[IntroPopupController alloc] init];
            controller.type = @"rights";
            [self.navigationController setViewControllers:@[controller] animated:YES];
        }
        else {
            IntroPopupController *controller = [[IntroPopupController alloc] init];
            controller.type = @"push";
            [self.navigationController setViewControllers:@[controller] animated:YES];
        }
    }
}

#pragma mark - AKVersionPopupViewDelegate

- (void)clickedUpdate {
    [AKCommonUtil openURL:_link];
    exit(0);
}

- (void)clickedExit {
    if ([_btnType isEqualToString:@"C"]) {
        [self passVersionPopup];
    } else {
        exit(0);
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
