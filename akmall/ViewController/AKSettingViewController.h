//
//  AKSettingViewController.h
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 2..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKBaseViewController.h"
#import <WebKit/WebKit.h>

@interface AKSettingViewController : AKBaseViewController

@property (nonatomic, strong) WKWebView *webView;
@end
