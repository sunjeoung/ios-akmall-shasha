//
//  AKMainWebViewController.h
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 30..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKBaseViewController.h"

@interface AKMainWebViewController : AKBaseViewController

- (void)refreshWeb:(NSString *)url;

@end
