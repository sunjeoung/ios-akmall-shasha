//
//  AKSettingViewController.m
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 2..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKSettingViewController.h"
#import "AKProperty.h"
#import "finger/finger.h"

@interface AKSettingViewController ()


@property (weak, nonatomic) IBOutlet UILabel *loginId;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UISwitch *autoLoginSw;
@property (weak, nonatomic) IBOutlet UISwitch *shoppingSw;
@property (weak, nonatomic) IBOutlet UISwitch *pushSw;
@property (weak, nonatomic) IBOutlet UISwitch *autoPlaySw;
@property (weak, nonatomic) IBOutlet UILabel *latestVersion;
@property (weak, nonatomic) IBOutlet UILabel *currentVersion;
@property (weak, nonatomic) IBOutlet UIButton *updateBtn;
@property (weak, nonatomic) IBOutlet UIButton *telBtn;
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;
@property (weak, nonatomic) IBOutlet UIButton *settingBtn;

@property (assign) BOOL isSelectStartTime;
@property (strong, nonatomic) NSMutableArray *timeList;
@property (assign) BOOL isLogin;

@end

@implementation AKSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *cVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    
    _autoLoginSw.on = [self isAutoLogin];
    _pushSw.on = [AKProperty isDenyTime];
    _autoPlaySw.on = [self isAutoPlay];
    
    [AKCommonUtil setBorder:_loginBtn width:1 color:UIColorMake(0xBB, 0xBB, 0xBB)];
    [AKCommonUtil setBorder:_updateBtn width:1 color:UIColorMake(0xBB, 0xBB, 0xBB)];
    [AKCommonUtil setBorder:_telBtn width:1 color:UIColorMake(0xBB, 0xBB, 0xBB)];
    [AKCommonUtil setBorder:_chatBtn width:1 color:UIColorMake(0xBB, 0xBB, 0xBB)];
    [AKCommonUtil setBorder:_settingBtn width:1 color:UIColorMake(0xBB, 0xBB, 0xBB)];
    _timeList = [NSMutableArray array];
    
    for (int i = 1; i <= 24; i++) {
        [_timeList addObject:[NSString stringWithFormat:@"%02d:00", i]];
    }
    _currentVersion.text = [NSString stringWithFormat:@"현재버전 V%@", cVersion];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self requestUserInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (BOOL)isAutoLogin {
    return [AKProperty isLogin];
}

- (void)setIsAutoLogin:(BOOL)isAutoLogin {
    NSString *value = [AKProperty isLogin] ? @"Y" : @"N";
    
    [AKCommonUtil setCookie:@"loginf" value:[NSString stringWithFormat:@"%@%@", value, isAutoLogin ? @"Y" : @"N"]];
}

- (BOOL)isAutoPlay {
    return [[AKCommonUtil getCookieValue:@"autoplay" web:self.webView] isEqualToString:@"Y"];
}

- (void)setAutoPlay:(BOOL)isAutoPlay {
    [AKCommonUtil setCookie:@"autoplay" value:isAutoPlay ? @"Y" : @"N"];
}

- (void)requestUserInfo {
    [AKNetworkUtil requestUserInfoWithSuccess:^(NSDictionary * _Nullable response) {
        self.isLogin = [response[@"hasLogin"] isEqualToString:@"Y"];
        [self updateLoginState:response[@"userInfo"][@"custId"]];
    } failure:^(NSError * _Nonnull error) {
        [self requestAppStoreVersion];
    }];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    //내 토근 정보
    [[finger sharedData] requestPushInfoWithBlock:^(NSDictionary *posts, NSError *error) {
        NSLog(@"posts : %@  error : %@",posts,error);
        if (posts != nil) {
            self.pushSw.on = [posts[@"activity"] isEqualToString:@"A"];
            self.shoppingSw.on = [posts[@"ad_activity"] isEqualToString:@"A"];
        }
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)updateLoginState:(NSString *)userId {
    _loginId.text = _isLogin ? userId : @"로그인 되어 있지 않습니다.";
    [_loginBtn setTitle:_isLogin ? @"로그아웃" : @"로그인" forState:UIControlStateNormal];
}

- (void)requestAppStoreVersion {
    [AKNetworkUtil requestAppStoreVersion:^(NSDictionary * __nullable response) {
        NSString *cVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
        int cVerValue = [[cVersion stringByReplacingOccurrencesOfString:@"." withString:@""] intValue];
        
        // 20170204, 버전 비교 로직 수정
        if ([response[@"resultCount"] intValue] > 0) {
            NSString *lVersion = response[@"results"][0][@"version"];
            int lVerValue = [[lVersion stringByReplacingOccurrencesOfString:@"." withString:@""] intValue];
            self.latestVersion.text = [NSString stringWithFormat:@"최신버전 V%@", lVersion];
            if (cVerValue < lVerValue) {
                self.latestVersion.hidden = NO;
                self.updateBtn.hidden = NO;
                
            }
        }
    } failure:^(NSError * __nonnull error) {
    }];
}

#pragma mark - IBAction

- (IBAction)clickClose:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickUpdate:(id)sender {
    [AKCommonUtil openURL:@"https://itunes.apple.com/kr/app/teochi-akmol-akmall/id490615718"];
}

- (IBAction)clickPush:(id)sender {
    UISwitch *sw = (UISwitch *)sender;
    
    [[finger sharedData] setEnable:_pushSw.on :^(NSString *posts, NSError *error) {
        if(!error) {
            [AKCommonUtil showAlertWithMsg:[NSString stringWithFormat:@"[AK몰] 앱 알림 수신 %@가 %@ 정상적으로 처리 되었습니다.", sw.on ? @"동의" : @"거부", [AKCommonUtil getTodayString:@"yyyy년 MM월 dd일"]]];
        } else {
            sw.on = !sw.on;
            [AKCommonUtil showAlertWithMsg:[NSString stringWithFormat:@"시스템에러 %@",error]];
        }
    }];
}

- (IBAction)clickLogin:(id)sender {
    if (_isLogin) {
//        [AKNetworkUtil requestLogoutWithSuccess:^(NSDictionary * _Nullable response) {
//            self.isLogin = NO;
//            [self updateLoginState:nil];
//            [AKCommonUtil showAlert:nil msg:@"로그아웃 되었습니다." cancelTitle:@"확인" cancelBlock:^(UIAlertAction * _Nullable action) {
//                [AKCommonUtil goHome:nil];
//            }];
//        } failure:^(NSError * _Nonnull error) {
//        }];
        
        [self.navigationController popViewControllerAnimated:NO];
        [AKCommonUtil pushWebViewController:@"/login/Logout.do"];
        
    } else {
        // 로그인 화면으로 이동
        [AKCommonUtil pushWebViewController:URL_LOGIN];
    }
}

- (IBAction)clickTel:(id)sender {
    [AKCommonUtil openURL:@"telprompt:15882055"];
}

- (IBAction)clickChat:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
    [AKCommonUtil pushWebViewController:URL_QUESTION];
}

- (IBAction)clickSetting:(id)sender {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (IBAction)clickAutoLogin:(id)sender {
    UISwitch *sw = (UISwitch *)sender;
    
    [self setIsAutoLogin:sw.on];
//    [AKProperty setLogin:sw.on];
//    [AKProperty setLoginToken:[AKCommonUtil getCookieValue:@"loginToken"]];
}

- (IBAction)clickShopping:(id)sender {
    UISwitch *sw = (UISwitch *)sender;
    
    [[finger sharedData] requestSetAdPushEnable:_shoppingSw.on :^(NSString *posts, NSError *error) {
        if(!error) {
            [AKCommonUtil showAlertWithMsg:[NSString stringWithFormat:@"[AK몰] 마케팅(광고성) 앱 알림 수신 %@가 %@ 정상적으로 처리 되었습니다.", sw.on ? @"동의" : @"거부", [AKCommonUtil getTodayString:@"yyyy년 MM월 dd일"]]];
        } else {
            sw.on = !sw.on;
            [AKCommonUtil showAlertWithMsg:[NSString stringWithFormat:@"시스템에러 %@",error]];
        }
    }];
}

- (IBAction)clickAutoPlay:(id)sender {
    UISwitch *sw = (UISwitch *)sender;
    
    [self setAutoPlay:sw.on];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
