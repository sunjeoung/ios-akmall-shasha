//
//  AKBaseViewController.m
//  akmall
//
//  Created by KimJinoug on 2016. 10. 20..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKBaseViewController.h"

@interface AKBaseViewController ()

@property (weak, nonatomic) UIView *bottomView;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomHeight;

@property (strong, nonatomic) UIActivityIndicatorView *indicator;
@property (strong, nonatomic) UIButton *backBtn;
@property (strong, nonatomic) UIButton *topBtn;

@end

@implementation AKBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:_indicator];
    
    [_indicator mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
    }];
    
    _topProgressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    _topProgressView.tintColor = UIColorMake(0xCB, 0x00, 0x5C);
    _topProgressView.trackTintColor = [UIColor whiteColor];
    _topProgressView.hidden = YES;
    [self.view addSubview:_topProgressView];
    
    [_topProgressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.mas_topLayoutGuideBottom);
        make.height.equalTo(@3);
    }];
}

#pragma mark - Public

- (void)startIndicator {
    [self.view bringSubviewToFront:_indicator];
    self.view.userInteractionEnabled = YES;
    [_indicator startAnimating];
    _indicator.hidden = NO;
}

- (void)stopIndicator {
    self.view.userInteractionEnabled = YES;
    [_indicator stopAnimating];
    _indicator.hidden = YES;
}

- (void)hideButtons:(BOOL)isHide {
    _backBtn.hidden = isHide;
    _topBtn.hidden = isHide;
}

- (void)addBottomView {
    
    // back button
    _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backBtn setBackgroundImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [_backBtn addTarget:self action:@selector(clickBackBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_backBtn];
    
    [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).with.offset(10);
        make.bottom.equalTo(self.view.mas_top).with.offset(-10);
    }];
    
    // top button
    _topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_topBtn setBackgroundImage:[UIImage imageNamed:@"btn_top"] forState:UIControlStateNormal];
    [_topBtn addTarget:self action:@selector(clickTopBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_topBtn];
    
    [_topBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).with.offset(-10);
        make.bottom.equalTo(self.view.mas_bottom).with.offset([CommonFunc safeBottomY]);
    }];
    
    // 플로팅 버튼을 bottom view 아래로
    [self.view bringSubviewToFront:_bottomView];
}

- (void)animationUpFloatButtons:(BOOL)isShowTopBtn {
    [UIView animateWithDuration:0.2 animations:^{
        // bottom
        CGRect frame = self.bottomView.frame;
        frame.origin.y = self.view.frame.size.height - 48;
        frame.size.height = 48;
        self.bottomView.frame = frame;
        // back 버튼
        frame = self.backBtn.frame;
        frame.origin.y = self.bottomView.frame.origin.y - (frame.size.height + 10);
        self.backBtn.frame = frame;
        // top 버튼
        frame = self.topBtn.frame;
        frame.origin.y = self.bottomView.frame.origin.y - (isShowTopBtn ? frame.size.height + 10 : 0);
        self.topBtn.frame = frame;
        
    } completion:^(BOOL finished) {
        //self.bottomHeight.constant = 48;
        [self.topBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.view.mas_right).with.offset(-10);
            make.bottom.equalTo(self.view.mas_bottom).with.offset(isShowTopBtn ? -(self.bottomView.frame.size.height + 10) : self.topBtn.frame.size.height - self.bottomView.frame.size.height);
        }];
    }];
}

- (void)animationDownFloatButtons:(BOOL)isShowTopBtn {
    [UIView animateWithDuration:0.2 animations:^{
        // bottom
        CGRect frame = self.bottomView.frame;
        frame.origin.y = self.view.frame.size.height;
        self.bottomView.frame = frame;
        // back 버튼
        frame = self.backBtn.frame;
        frame.origin.y = self.view.frame.size.height - (frame.size.height + 10);
        self.backBtn.frame = frame;
        // top 버튼
        frame = self.topBtn.frame;
        frame.origin.y = self.view.frame.size.height - (isShowTopBtn ? frame.size.height + 10 : 0);
        self.topBtn.frame = frame;
        
    } completion:^(BOOL finished) {
        //self.bottomHeight.constant = 0;
        [self.topBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.view.mas_right).with.offset(-10);
            make.bottom.equalTo(self.view.mas_bottom).with.offset(isShowTopBtn ? -10 : self.topBtn.frame.size.height);
        }];
    }];
}

- (void)clickBackBtn {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)clickTopBtn {
}


@end
