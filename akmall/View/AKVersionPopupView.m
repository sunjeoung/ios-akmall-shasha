//
//  AKVersionPopupView.m
//  AKMall
//
//  Created by KimJinoug on 2016. 12. 28..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKVersionPopupView.h"
#import "GlobalHeader.h"

@interface AKVersionPopupView ()

@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *exitButton;

@end

@implementation AKVersionPopupView

#pragma mark - Public

- (void)reloadPopupView {
    NSDictionary *attr = @{NSForegroundColorAttributeName: UIColorMake(0x99, 0x99, 0x99),
                           NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSAttributedString *labelText = [[NSAttributedString alloc] initWithString:[_popupType isEqualToString:@"C"] ? @"다음에" : @"앱종료" attributes:attr];
    
    _content.text = [_popupType isEqualToString:@"C"] ? @"새 버전이 추가 되었습니다.\n업데이트후 이용해주세요!\n업데이트를 누르시면 스토어로 이동합니다.\n(현재 버전으로도 앱사용이 가능합니다.)" : @"원활한 서비스 이용을 위해 업데이트가 필요합니다.\n업데이트를 누르시면 스토어로 이동합니다.";
    _exitButton.attributedText = labelText;
}

#pragma mark - IBAction

- (IBAction)clickUpdate:(id)sender {
    if (_delegate) {
        [_delegate clickedUpdate];
    }
}

- (IBAction)clickExit:(id)sender {
    if (_delegate) {
        [_delegate clickedExit];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
