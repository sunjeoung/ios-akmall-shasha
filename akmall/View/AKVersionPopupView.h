//
//  AKVersionPopupView.h
//  AKMall
//
//  Created by KimJinoug on 2016. 12. 28..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AKVersionPopupViewDelegate

@required
- (void)clickedUpdate;
- (void)clickedExit;

@end

@interface AKVersionPopupView : UIView

@property (strong, nonatomic) NSString *popupType;
@property (assign) id<AKVersionPopupViewDelegate> delegate;

- (void)reloadPopupView;

@end
