//
//  AKSelectView.m
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 2..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKSelectView.h"
#import "AKSelectCell.h"
#import "AKCommonUtil.h"

@interface AKSelectView ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;

@end

@implementation AKSelectView

- (void)awakeFromNib {
    [super awakeFromNib];
}

#pragma mark - Public

- (void)reloadData {
    _tableHeight.constant = MIN(_dataList.count * 41, 300);
    [_tableView reloadData];
}

#pragma mark - IBAction

- (IBAction)tapClose:(id)sender {
    [self removeFromSuperview];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AKSelectCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AKSelectCell"];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"AKSelectCell" owner:self options:nil] objectAtIndex:0];
    }
    
    cell.title.text = _dataList[indexPath.row];
    cell.title.textColor = _selectedIndex == indexPath.row ? UIColorMake(0xE5, 0x1C, 0x52) : UIColorMake(0x33, 0x33, 0x33);
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 41;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self removeFromSuperview];
    
    if (_delegate) {
        [_delegate selectedItem:_dataList[indexPath.row] value:_valueList ? _valueList[indexPath.row] : nil];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
