//
//  AKListMenuView.m
//  AKMall
//
//  Created by KimJinoug on 2016. 10. 31..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKListMenuView.h"

@interface AKListMenuView ()

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UIButton *btnBag;

@end

@implementation AKListMenuView

#pragma mark - Public

- (void)openMenu:(BOOL)isOpen {
    _isMenuOpen = isOpen;
    [_btnMenu setBackgroundImage:[UIImage imageNamed:isOpen ? @"list_btn_close" : @"list_btn_menu"] forState:UIControlStateNormal];
    _btnShare.hidden = !isOpen;
    _btnLike.hidden = !isOpen;
    _btnBag.hidden = !isOpen;
}

#pragma mark - IBAction

- (IBAction)clickMenu:(id)sender {
    [self openMenu:!_isMenuOpen];
    
    if (_delegate) {
        [_delegate clickedMenu:self];
    }
}

- (IBAction)clickShare:(id)sender {
    if (_delegate) {
        [_delegate clickedShare:self];
    }
}

- (IBAction)clickLike:(id)sender {
    if (_delegate) {
        [_delegate clickedLike:self];
    }
}

- (IBAction)clickBag:(id)sender {
    if (_delegate) {
        [_delegate clickedBag:self];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
