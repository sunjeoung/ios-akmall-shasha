//
//  AKFooterView.h
//  AKMall
//
//  Created by KimJinoug on 2016. 12. 1..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AKFooterViewDelegate

@required
- (void)clickedFooterLog;
- (void)clickedFooterPcView;
- (void)clickedFooterSetting;
- (void)clickedFooterUseLabel;
- (void)clickedFooterPrivacy;
- (void)clickedFooterCompany;

@end

@interface AKFooterView : UIView

@property (assign) id<AKFooterViewDelegate> delegate;

- (void)setLoginState:(BOOL)isLogin;

@end
