//
//  AKFooterView.m
//  AKMall
//
//  Created by KimJinoug on 2016. 12. 1..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKFooterView.h"
#import "AKCommonUtil.h"

@interface AKFooterView ()

@property (weak, nonatomic) IBOutlet UIButton *logBtn;

@end

@implementation AKFooterView

#pragma mark - Public

- (void)setLoginState:(BOOL)isLogin {
    [_logBtn setTitle:isLogin ? @"로그아웃" : @"로그인" forState:UIControlStateNormal];
}

#pragma mark - IBAction

- (IBAction)clickLog:(id)sender {
    if (_delegate) {
        [_delegate clickedFooterLog];
    }
}

- (IBAction)clickPcView:(id)sender {
    if (_delegate) {
        [_delegate clickedFooterPcView];
    }
}

- (IBAction)clickSetting:(id)sender {
    if (_delegate) {
        [_delegate clickedFooterSetting];
    }
}

- (IBAction)clickAppDown:(id)sender {
    [AKCommonUtil openURL:@"https://itunes.apple.com/kr/app/ak-plaza-baeghwajeom/id499453449"];
}

- (IBAction)clickUseLabel:(id)sender {
    if (_delegate) {
        [_delegate clickedFooterUseLabel];
    }
}

- (IBAction)clickPrivacy:(id)sender {
    if (_delegate) {
        [_delegate clickedFooterPrivacy];
    }
}

- (IBAction)clickCompany:(id)sender {
    if (_delegate) {
        [_delegate clickedFooterCompany];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
