//
//  AKPopupWebView.m
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 18..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKPopupWebView.h"
#import "AKCommonUtil.h"
#import "AKSubWebView.h"
 
@interface AKPopupWebView () <AKSubWebViewDelegate>
{
    AKSubWebView *akWebView;
}

@property (weak, nonatomic) IBOutlet UIView *contentsView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeight;

@end

@implementation AKPopupWebView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    akWebView = [[AKSubWebView alloc] initWithFrame:self.contentsView.bounds];
    akWebView.delegate = self;
    akWebView.webView.scrollView.maximumZoomScale = 5.0;
    akWebView.webViewType = AKSubWebViewTypePopupView;
    [self.contentsView addSubview:akWebView];
}

#pragma mark - Public

- (void)loadUrl:(NSString *)url {
    [akWebView requestUrl:url];
}

#pragma mark - AKSubView Delegate

- (void)closePopup:(NSString *)type {
    [self removeFromSuperview];
}

- (void)updateHeight:(CGFloat)nHeight
{
    _webViewHeight.constant = nHeight;
    [self layoutIfNeeded];
}

- (void)closeAndOpenWebview:(NSString *)url {
    if ([appDelegate isHistoryWebView]) {
        [appDelegate closeHistoryView];
    }
    [AKCommonUtil pushWebViewController:url];
    [self removeFromSuperview];
}


@end
