//
//  AKListMenuView.h
//  AKMall
//
//  Created by KimJinoug on 2016. 10. 31..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AKListMenuView;

@protocol AKListMenuViewDelegate

@required
- (void)clickedMenu:(AKListMenuView *)view;
- (void)clickedShare:(AKListMenuView *)view;
- (void)clickedLike:(AKListMenuView *)view;
- (void)clickedBag:(AKListMenuView *)view;

@end

@interface AKListMenuView : UIView

@property (assign) BOOL isMenuOpen;
@property (assign) id<AKListMenuViewDelegate> delegate;

- (void)openMenu:(BOOL)isOpen;

@end
