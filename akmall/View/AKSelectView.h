//
//  AKSelectView.h
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 2..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AKSelectViewDelegate <NSObject>

@required
- (void)selectedItem:(NSString *)str value:(NSString *)value;

@end

@interface AKSelectView : UIView<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSArray *dataList;
@property (strong, nonatomic) NSArray *valueList;
@property (assign) int selectedIndex;
@property (assign) id<AKSelectViewDelegate> delegate;

- (void)reloadData;

@end
