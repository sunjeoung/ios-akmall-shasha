//
//  AKFullPopupView.m
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 29..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKFullPopupView.h"
#import "AKCommonUtil.h"
#import "UIImageView+WebCache.h"
#import "AKProperty.h"

@interface AKFullPopupView ()

@property (weak, nonatomic) IBOutlet UIImageView *popupImage;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn;
@property (weak, nonatomic) IBOutlet UILabel *checkLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@property (assign) BOOL isCheck;

@end

@implementation AKFullPopupView

- (void)awakeFromNib {
    [super awakeFromNib];
    [AKCommonUtil setBorder:_closeBtn width:1 color:UIColorMake(0xE5, 0x1C, 0x52)];
}

#pragma mark - Public

- (void)loadImage {
    NSDictionary *file = _data[@"file"][0];
    NSString *disp = _data[@"cookie_un_disp_term_apply_yn"];
    
    [_popupImage sd_setImageWithURL:[NSURL URLWithString:file[@"banner_url"]]];
    _checkBtn.hidden = ![disp isEqualToString:@"Y"];
    _checkLabel.hidden = ![disp isEqualToString:@"Y"];
}

#pragma mark - IBAction

- (IBAction)clickCheck:(id)sender {
    _isCheck = !_isCheck;
    [_checkBtn setSelected:_isCheck];
}

- (IBAction)clickClose:(id)sender {
    if (_isCheck) {
        [AKProperty setPopupTodayDate:_data[@"mobile_popup_id"]];
    }
    
    [self removeFromSuperview];
}

- (IBAction)clickImage:(id)sender {
    NSDictionary *file = _data[@"file"][0];
    
    [AKCommonUtil pushWebViewController:file[@"content_url"]];
    [self clickClose:nil];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
