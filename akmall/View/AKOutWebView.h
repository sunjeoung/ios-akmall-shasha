//
//  AKOutWebView.h
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 25..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AKOutWebView : UIView

- (void)loadUrl:(NSString *)url;
- (void)openAnimation:(UIView *)parent;

@end
