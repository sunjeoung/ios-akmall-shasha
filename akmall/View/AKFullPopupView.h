//
//  AKFullPopupView.h
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 29..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AKFullPopupView : UIView

@property (strong, nonatomic) NSDictionary *data;

- (void)loadImage;

@end
