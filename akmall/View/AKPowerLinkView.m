//
//  AKPowerLinkView.m
//  AKMall
//
//  Created by KimJinoug on 2016. 12. 6..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKPowerLinkView.h"
#import "AKCommonUtil.h"

@interface AKPowerLinkView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *urlLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (strong, nonatomic) NSString *url;

@end

@implementation AKPowerLinkView

- (void)awakeFromNib {
    [super awakeFromNib];
    [AKCommonUtil setBorder:self width:1 color:UIColorMake(0xCC, 0xCC, 0xCC)];
}

#pragma mark - Public

- (void)updateContent:(NSDictionary *)dic {
    _titleLabel.text = dic[@"title"];
    _contentLabel.text = dic[@"desc"];
    _urlLabel.text = dic[@"vUrl"];
    _url = dic[@"cUrl"];
}

#pragma mark - IBAction

- (IBAction)clickAds:(id)sender {
    [AKCommonUtil pushWebViewController:_url];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
