//
//  AKPopupWebView.h
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 18..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AKSchemeManager.h"

@interface AKPopupWebView : UIView

- (void)loadUrl:(NSString *)url;
- (void)updateHeight:(CGFloat)nHeight;

@end
