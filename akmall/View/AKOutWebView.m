//
//  AKOutWebView.m
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 25..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKOutWebView.h"
#import "AKCommonUtil.h"
#import "AKSubWebView.h"

@interface AKOutWebView () <AKSubWebViewDelegate>
{
    
}

@property (weak, nonatomic) IBOutlet UIView *contentsView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) UIProgressView *topProgressView;
@property (nonatomic, strong) AKSubWebView *webView;

@end

@implementation AKOutWebView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.webView = [[AKSubWebView alloc] initWithFrame:self.contentsView.bounds];
    self.webView.delegate = self;
    self.webView.webViewType = AKSubWebViewTypeOutView;
    [self.contentsView addSubview:self.webView];
    
    _topProgressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    _topProgressView.tintColor = UIColorMake(0xCB, 0x00, 0x5C);
    _topProgressView.trackTintColor = [UIColor whiteColor];
    _topProgressView.hidden = YES;
    [self addSubview:_topProgressView];
    
    [_topProgressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.contentsView.mas_top);
        make.height.equalTo(@3);
    }];
}

#pragma mark - Private

- (void)hiddenTopProgressView {
    self.topProgressView.hidden = YES;
}

#pragma mark - Public

- (void)loadUrl:(NSString *)url {
    [self.webView requestUrl:url];
}

- (void)openAnimation:(UIView *)parent {
    CGRect frame = self.frame;
    
    frame.origin.x = parent.frame.size.width;
    self.frame = frame;
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.frame;
        frame.origin.x = 0;
        self.frame = frame;
    } completion:nil];
}

#pragma mark - AKSubWebView Delegate

- (void)updateTitle:(NSString *)sTitle
{
    if (sTitle) {
        _titleLabel.text = sTitle;
    }
}

- (void)closePopupView:(NSString *)type
{
    [self clickClose:nil];
}

#pragma mark - IBAction

- (IBAction)clickClose:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.frame;
        
        frame.origin.x = self.superview.frame.size.width;
        self.frame = frame;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
