//
//  AKDoubleCell.h
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 2..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AKBestCell.h"

@protocol AKDoubleCellDelegate

@required
- (void)clickItem:(int)index;

@end

@interface AKDoubleCell : AKBestCell

@property (weak, nonatomic) IBOutlet UIButton *pButton1;
@property (weak, nonatomic) IBOutlet UIView *pView2;
@property (weak, nonatomic) IBOutlet UIButton *pButton2;
@property (weak, nonatomic) IBOutlet UIImageView *image2;
@property (weak, nonatomic) IBOutlet UILabel *tagName2;
@property (weak, nonatomic) IBOutlet UIImageView *tagPick2;
@property (weak, nonatomic) IBOutlet UIImageView *tagSelect2;
@property (weak, nonatomic) IBOutlet UILabel *title2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTop2;
@property (weak, nonatomic) IBOutlet UIView *salesView2;
@property (weak, nonatomic) IBOutlet UILabel *percent2;
@property (weak, nonatomic) IBOutlet UILabel *salePrice2;
@property (weak, nonatomic) IBOutlet UILabel *originPrice2;
@property (weak, nonatomic) IBOutlet UIView *noSalesView2;
@property (weak, nonatomic) IBOutlet UILabel *noSalePrice2;
@property (weak, nonatomic) IBOutlet UIView *rentalView2;
@property (weak, nonatomic) IBOutlet UILabel *rentalSalePrice2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rentalSalePriceHeight2;
@property (weak, nonatomic) IBOutlet UILabel *rentalSalePriceDesc2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rentalSalePriceDescHeight2;
@property (weak, nonatomic) IBOutlet UILabel *rentalPrice2;
@property (weak, nonatomic) IBOutlet UILabel *rentalMonth2;
@property (weak, nonatomic) IBOutlet UIView *menuView2;
@property (weak, nonatomic) IBOutlet UILabel *freeDelivery2;
@property (weak, nonatomic) IBOutlet UIView *verticalLine2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalLineLeft2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalLineRight2;
@property (weak, nonatomic) IBOutlet UILabel *count2;
@property (weak, nonatomic) IBOutlet UIView *bottomView2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewTop2;

@property (strong, nonatomic) AKListMenuView *listMenuView2;

@property (assign) id<AKDoubleCellDelegate> delegate;

- (void)setData:(NSDictionary *)dic data2:(NSDictionary *)dic2 index1:(NSInteger)index1 index2:(NSInteger)index2 listMenuViewDelegate:(id<AKListMenuViewDelegate>)delegate doubleCellDelegate:(id<AKDoubleCellDelegate>)delegate2;

@end
