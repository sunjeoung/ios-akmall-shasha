//
//  AKPowerLinkCell.h
//  AKMall
//
//  Created by KimJinoug on 2016. 12. 6..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AKPowerLinkCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *linkView;

- (void)setData:(NSArray *)ads;

@end
