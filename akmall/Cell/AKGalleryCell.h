//
//  AKGalleryCell.h
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 2..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AKBestCell.h"

@interface AKGalleryCell : AKBestCell

@property (weak, nonatomic) IBOutlet UIView *line;

@property (strong, nonatomic) NSLayoutConstraint *lineTop;

@end
