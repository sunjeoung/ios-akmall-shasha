//
//  AKBestCell.m
//  AKMall
//
//  Created by KimJinoug on 2016. 10. 31..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKBestCell.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import "AKCommonUtil.h"

@implementation AKBestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    // Initialization code
    _listMenuView = [[[NSBundle mainBundle] loadNibNamed:@"AKListMenuView" owner:self options:nil] objectAtIndex:0];
    
    [_menuView addSubview:_listMenuView];
    [_listMenuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.menuView).with.insets(padding);
    }];
    
    [_relativeBtn addTarget:self action:@selector(clickRelativeGood:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Private

- (int)getTitleTopMargin {
    return _tagName.hidden && _tagSelect.hidden ? 10 : 37;
}

- (void)removeLineTop {
    
}

- (void)addLineTopToRental {
    
}

- (void)addLineTopToNoRental {
    
}

- (void)clickRelativeGood:(id)sender {
    if (_relativeDelegate && [_relativeDelegate respondsToSelector:@selector(clickRelativeProduct:)]) {
        [_relativeDelegate clickRelativeProduct:_relativeBtn];
    }
}

- (CGFloat)getOriginalSalePriceFontSize {
    return 12;
}

- (void)updateBottomViewConstraint {
    
}

#pragma mark - Public

- (NSString *)getImagePath:(NSString *)imagePath {
    if (![imagePath hasPrefix:@"http"]) {
        imagePath = [NSString stringWithFormat:@"%@%@", BASE_URL, imagePath];
    }
    
    return [imagePath stringByReplacingOccurrencesOfString:@"@@@" withString:@"350"];
}

- (void)setData:(NSDictionary *)dic index:(NSInteger)index listMenuViewDelegate:(id<AKListMenuViewDelegate>)delegate relativeProductDelegate:(id<AKRelativeProductDelegate>)delegate2 {
    NSString *menuOpen = dic[@"menuOpen"];
    NSString *goodsKindCode = [AKCommonUtil checkNil:dic[@"goods_kind_code"]];
    BOOL isFreeDelivery = [[AKCommonUtil checkNil:dic[@"free_deliv_yn"]] isEqualToString:@"Y"];
    int saleRate = [dic[@"info"][@"sale_rate"] intValue];
    int salePrice = [dic[@"info"][@"sale_price"] intValue];
    int finalPrice = [dic[@"info"][@"final_price"] intValue];
    
    _listMenuView.delegate = delegate;
    _listMenuView.tag = index;
    _topLineHeight.constant = index == 0 ? 1 : 0;
    [_image sd_setImageWithURL:[NSURL URLWithString:[self getImagePath:[AKCommonUtil checkNil:dic[@"getImagePath"]]]]];
    
    if ([[AKCommonUtil checkNil:dic[@"isDeptVendor"]] isEqualToString:@"Y"]) {
        _tagName.hidden = [[AKCommonUtil checkNil:dic[@"launch_yn"]] isEqualToString:@"N"];
        _tagName.text = [AKCommonUtil checkNil:dic[@"getPlazaName"]];
        _tagPick.hidden = _tagName.hidden || (![[AKCommonUtil checkNil:dic[@"smart_pick_yn"]] isEqualToString:@"Y"] && ![[AKCommonUtil checkNil:dic[@"smart_pick_yn"]] isEqualToString:@"P"]);
        _tagSelect.hidden = ![[AKCommonUtil checkNil:dic[@"launch_yn"]] isEqualToString:@"N"];
    } else {
        _tagName.hidden = YES;
        _tagPick.hidden = YES;
        _tagSelect.hidden = YES;
    }
    
    _title.text = [AKCommonUtil checkNil:dic[@"goods_name"]];
    _titleTop.constant = [self getTitleTopMargin];
    _listMenuView.isMenuOpen = [menuOpen isEqualToString:@"Y"];
    [_listMenuView openMenu:[menuOpen isEqualToString:@"Y"]];
    _count.text = [NSString stringWithFormat:@"상품평(%d)", [dic[@"comment_cnt"] intValue]];
    [self removeLineTop];
    
    if ([goodsKindCode isEqualToString:@"009"] || [goodsKindCode isEqualToString:@"010"]) {
        _rentalView.hidden = NO;
        _salesView.hidden = YES;
        _noSalesView.hidden = YES;
        [self addLineTopToRental];
    } else {
        _rentalView.hidden = YES;
        _salesView.hidden = saleRate == 0;
        _noSalesView.hidden = saleRate > 0;
        [self addLineTopToNoRental];
    }
    
    if (!_salesView.hidden) {
        NSDictionary *attr = @{NSFontAttributeName: [UIFont fontWithName:@"Arial" size:[self getOriginalSalePriceFontSize]],
                               NSForegroundColorAttributeName: UIColorMake(0x99, 0x99, 0x99),
                               NSStrikethroughStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]};
        NSAttributedString *labelText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@원", [AKCommonUtil getCurrencyFormat:salePrice]] attributes:attr];
        
        _percent.text = [NSString stringWithFormat:@"%d", saleRate];
        _salePrice.text = [AKCommonUtil getCurrencyFormat:finalPrice];
        _originPrice.attributedText = labelText;
    } else if (!_noSalesView.hidden) {
        _noSalePrice.text = [AKCommonUtil getCurrencyFormat:finalPrice];
    } else if (!_rentalView.hidden) {
        if ([goodsKindCode isEqualToString:@"010"]) {
            _rentalSalePrice.text = [NSString stringWithFormat:@"할인가 %@원", [AKCommonUtil getCurrencyFormat:finalPrice]];
            _rentalSalePriceHeight.constant = 19;
            _rentalSalePriceDesc.text = @"(최초 1회차 결제)";
            _rentalSalePriceDescHeight.constant = 19;
        } else {
            _rentalSalePrice.text = @"";
            _rentalSalePriceHeight.constant = 0;
            _rentalSalePriceDesc.text = @"";
            _rentalSalePriceDescHeight.constant = 0;
        }
        
        _rentalPrice.text = [NSString stringWithFormat:@"%@원(/월)", [AKCommonUtil getCurrencyFormat:[dic[@"rental_month_price"] intValue]]];
        _rentalMonth.text = [NSString stringWithFormat:@"%d개월", [dic[@"rental_months"] intValue]];
    }
    
    _freeDelivery.hidden = !isFreeDelivery;
    _freeDelivery.text = isFreeDelivery ? @"무료배송" : @"";
    _verticalLine.hidden = !isFreeDelivery;
    _verticalLineLeft.constant = isFreeDelivery ? 5 : 0;
    _verticalLineRight.constant = isFreeDelivery ? 5 : 0;
    _relativeDelegate = delegate2;
    
    [self updateBottomViewConstraint];
}

@end
