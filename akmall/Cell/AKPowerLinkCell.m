//
//  AKPowerLinkCell.m
//  AKMall
//
//  Created by KimJinoug on 2016. 12. 6..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKPowerLinkCell.h"
#import "AKPowerLinkView.h"
#import "Masonry.h"

@implementation AKPowerLinkCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Public

- (void)setData:(NSArray *)ads {
    UIView *prevView = nil;
    
    for (UIView *view in _linkView.subviews) {
        [view removeFromSuperview];
    }
    
    for (NSDictionary *dic in ads) {
        AKPowerLinkView *view = [[[NSBundle mainBundle] loadNibNamed:@"AKPowerLinkView" owner:self options:nil] objectAtIndex:0];
        
        [view updateContent:dic];
        [_linkView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.linkView.mas_left);
            make.right.equalTo(self.linkView.mas_right);
            
            if (prevView) {
                make.top.equalTo(prevView.mas_bottom).with.offset(10);
            } else {
                make.top.equalTo(self.linkView.mas_top);
            }
            
            if (dic == ads.lastObject) {
                make.bottom.equalTo(self.linkView.mas_bottom);
            }
        }];
        prevView = view;
    }
}

@end
