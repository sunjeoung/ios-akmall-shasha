//
//  AKBestCell.h
//  AKMall
//
//  Created by KimJinoug on 2016. 10. 31..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AKListMenuView.h"

@protocol AKRelativeProductDelegate <NSObject>

@optional
- (void)clickRelativeProduct:(UIButton *)btn;

@end

@interface AKBestCell : UITableViewCell

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLineHeight;
@property (weak, nonatomic) IBOutlet UILabel *number;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *tagName;
@property (weak, nonatomic) IBOutlet UIImageView *tagPick;
@property (weak, nonatomic) IBOutlet UIImageView *tagSelect;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTop;
@property (weak, nonatomic) IBOutlet UIView *salesView;
@property (weak, nonatomic) IBOutlet UILabel *percent;
@property (weak, nonatomic) IBOutlet UILabel *salePrice;
@property (weak, nonatomic) IBOutlet UILabel *originPrice;
@property (weak, nonatomic) IBOutlet UIView *noSalesView;
@property (weak, nonatomic) IBOutlet UILabel *noSalePrice;
@property (weak, nonatomic) IBOutlet UIView *rentalView;
@property (weak, nonatomic) IBOutlet UILabel *rentalSalePrice;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rentalSalePriceHeight;
@property (weak, nonatomic) IBOutlet UILabel *rentalSalePriceDesc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rentalSalePriceDescHeight;
@property (weak, nonatomic) IBOutlet UILabel *rentalPrice;
@property (weak, nonatomic) IBOutlet UILabel *rentalMonth;
@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UILabel *freeDelivery;
@property (weak, nonatomic) IBOutlet UIView *verticalLine;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalLineLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalLineRight;
@property (weak, nonatomic) IBOutlet UILabel *count;
@property (weak, nonatomic) IBOutlet UIButton *relativeBtn;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewTop;

@property (strong, nonatomic) AKListMenuView *listMenuView;
@property (assign) id<AKRelativeProductDelegate> relativeDelegate;

- (NSString *)getImagePath:(NSString *)imagePath;
- (void)setData:(NSDictionary *)dic index:(NSInteger)index listMenuViewDelegate:(id<AKListMenuViewDelegate>)delegate relativeProductDelegate:(id<AKRelativeProductDelegate>)delegate2;
@end
