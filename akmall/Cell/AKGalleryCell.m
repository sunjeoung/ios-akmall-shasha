//
//  AKGalleryCell.m
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 2..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKGalleryCell.h"
#import "AKCommonUtil.h"

@implementation AKGalleryCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark - Private

- (int)getTitleTopMargin {
    return self.tagName.hidden && self.tagSelect.hidden ? 15 : 42;
}

- (void)removeLineTop {
    if (_lineTop) {
        [NSLayoutConstraint deactivateConstraints:@[_lineTop]];
    }
}

- (void)addLineTopToRental {
    _lineTop = [NSLayoutConstraint constraintWithItem:self.line attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.rentalView attribute:NSLayoutAttributeBottom multiplier:1 constant:8];
    [NSLayoutConstraint activateConstraints:@[_lineTop]];
}

- (void)addLineTopToNoRental {
    _lineTop = [NSLayoutConstraint constraintWithItem:self.line attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.salesView.hidden ? self.noSalesView : self.salesView attribute:NSLayoutAttributeBottom multiplier:1 constant:8];
    [NSLayoutConstraint activateConstraints:@[_lineTop]];
}

#pragma mark - Public

- (NSString *)getImagePath:(NSString *)imagePath {
    if (![imagePath hasPrefix:@"http"]) {
        imagePath = [NSString stringWithFormat:@"%@%@", BASE_URL, imagePath];
    }
    
    return [imagePath stringByReplacingOccurrencesOfString:@"@@@" withString:@"500"];
}

@end
