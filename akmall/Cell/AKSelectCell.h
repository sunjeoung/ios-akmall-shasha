//
//  AKSelectCell.h
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 2..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AKSelectCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;

@end
