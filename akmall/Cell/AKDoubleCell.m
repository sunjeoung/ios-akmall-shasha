//
//  AKDoubleCell.m
//  AKMall
//
//  Created by KimJinoug on 2016. 11. 2..
//  Copyright © 2016년 sjsofttech. All rights reserved.
//

#import "AKDoubleCell.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import "AKCommonUtil.h"

@implementation AKDoubleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.listMenuView2 = [[[NSBundle mainBundle] loadNibNamed:@"AKListMenuView" owner:self options:nil] objectAtIndex:0];
    
    [_menuView2 addSubview:_listMenuView2];
    [_listMenuView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.menuView2).with.insets(padding);
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Private

- (int)getTitleTopMargin2 {
    return _tagName2.hidden && _tagSelect2.hidden ? 10 : 37;
}

- (CGFloat)getOriginalSalePriceFontSize {
    return 14;
}

- (void)updateBottomViewConstraint {
    if (!self.rentalView.hidden) {
        NSLayoutConstraint *lc = [NSLayoutConstraint constraintWithItem:self.bottomView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.rentalView attribute:NSLayoutAttributeBottom multiplier:1 constant:5];
        
        [NSLayoutConstraint deactivateConstraints:@[self.bottomViewTop]];
        self.bottomViewTop = lc;
        [NSLayoutConstraint activateConstraints:@[self.bottomViewTop]];
    } else {
        NSLayoutConstraint *lc = [NSLayoutConstraint constraintWithItem:self.bottomView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.salesView attribute:NSLayoutAttributeBottom multiplier:1 constant:5];
        
        [NSLayoutConstraint deactivateConstraints:@[self.bottomViewTop]];
        self.bottomViewTop = lc;
        [NSLayoutConstraint activateConstraints:@[self.bottomViewTop]];
    }
}

- (void)updateBottomViewConstraint2 {
    if (!_rentalView2.hidden) {
        NSLayoutConstraint *lc = [NSLayoutConstraint constraintWithItem:_bottomView2 attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_rentalView2 attribute:NSLayoutAttributeBottom multiplier:1 constant:5];
        
        [NSLayoutConstraint deactivateConstraints:@[_bottomViewTop2]];
        _bottomViewTop2 = lc;
        [NSLayoutConstraint activateConstraints:@[_bottomViewTop2]];
    } else {
        NSLayoutConstraint *lc = [NSLayoutConstraint constraintWithItem:_bottomView2 attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_salesView2 attribute:NSLayoutAttributeBottom multiplier:1 constant:5];
        
        [NSLayoutConstraint deactivateConstraints:@[_bottomViewTop2]];
        _bottomViewTop2 = lc;
        [NSLayoutConstraint activateConstraints:@[_bottomViewTop2]];
    }
}

#pragma mark - IBAction

- (IBAction)clickItem:(id)sender {
    if (_delegate) {
        UIButton *btn = (UIButton *)sender;
        
        [_delegate clickItem:(int)btn.tag];
    }
}

#pragma mark - Public

- (void)setData:(NSDictionary *)dic data2:(NSDictionary *)dic2 index1:(NSInteger)index1 index2:(NSInteger)index2 listMenuViewDelegate:(id<AKListMenuViewDelegate>)delegate doubleCellDelegate:(id<AKDoubleCellDelegate>)delegate2 {
    [self setData:dic index:index1 listMenuViewDelegate:delegate relativeProductDelegate:nil];
    _pButton1.tag = index1;
    _delegate = delegate2;
    
    if (dic2) {
        NSString *menuOpen2 = dic2[@"menuOpen"];
        NSString *goodsKindCode = [AKCommonUtil checkNil:dic2[@"goods_kind_code"]];
        BOOL isFreeDelivery = [[AKCommonUtil checkNil:dic2[@"free_deliv_yn"]] isEqualToString:@"Y"];
        int saleRate = [dic2[@"info"][@"sale_rate"] intValue];
        int salePrice = [dic2[@"info"][@"sale_price"] intValue];
        int finalPrice = [dic2[@"info"][@"final_price"] intValue];
        
        _pView2.hidden = NO;
        _pButton2.tag = index2;
        _listMenuView2.delegate = delegate;
        _listMenuView2.tag = index2;
        [_image2 sd_setImageWithURL:[NSURL URLWithString:[self getImagePath:[AKCommonUtil checkNil:dic2[@"getImagePath"]]]]];
        
        if ([[AKCommonUtil checkNil:dic2[@"isDeptVendor"]] isEqualToString:@"Y"]) {
            _tagName2.hidden = [[AKCommonUtil checkNil:dic2[@"launch_yn"]] isEqualToString:@"N"];
            _tagName2.text = [AKCommonUtil checkNil:dic2[@"getPlazaName"]];
            _tagPick2.hidden = _tagName2.hidden || (![[AKCommonUtil checkNil:dic2[@"smart_pick_yn"]] isEqualToString:@"Y"] && ![[AKCommonUtil checkNil:dic2[@"smart_pick_yn"]] isEqualToString:@"P"]);
            _tagSelect2.hidden = ![[AKCommonUtil checkNil:dic2[@"launch_yn"]] isEqualToString:@"N"];
        } else {
            _tagName2.hidden = YES;
            _tagPick2.hidden = YES;
            _tagSelect2.hidden = YES;
        }
        
        _title2.text = [AKCommonUtil checkNil:dic2[@"goods_name"]];
        _titleTop2.constant = [self getTitleTopMargin2];
        _listMenuView2.isMenuOpen = [menuOpen2 isEqualToString:@"Y"];
        [_listMenuView2 openMenu:[menuOpen2 isEqualToString:@"Y"]];
        _count2.text = [NSString stringWithFormat:@"상품평(%d)", [dic2[@"comment_cnt"] intValue]];
        
        if ([goodsKindCode isEqualToString:@"009"] || [goodsKindCode isEqualToString:@"010"]) {
            _rentalView2.hidden = NO;
            _salesView2.hidden = YES;
            _noSalesView2.hidden = YES;
        } else {
            _rentalView2.hidden = YES;
            _salesView2.hidden = saleRate == 0;
            _noSalesView2.hidden = saleRate > 0;
        }
        
        if (!_salesView2.hidden) {
            NSDictionary *attr = @{NSFontAttributeName: [UIFont fontWithName:@"Arial" size:[self getOriginalSalePriceFontSize]],
                                   NSForegroundColorAttributeName: UIColorMake(0x99, 0x99, 0x99),
                                   NSStrikethroughStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]};
            NSAttributedString *labelText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@원", [AKCommonUtil getCurrencyFormat:salePrice]] attributes:attr];
            
            _percent2.text = [NSString stringWithFormat:@"%d", saleRate];
            _salePrice2.text = [AKCommonUtil getCurrencyFormat:finalPrice];
            _originPrice2.attributedText = labelText;
        } else if (!_noSalesView2.hidden) {
            _noSalePrice2.text = [AKCommonUtil getCurrencyFormat:finalPrice];
        } else if (!_rentalView2.hidden) {
            if ([goodsKindCode isEqualToString:@"010"]) {
                _rentalSalePrice2.text = [NSString stringWithFormat:@"할인가 %@원", [AKCommonUtil getCurrencyFormat:finalPrice]];
                _rentalSalePriceHeight2.constant = 19;
                _rentalSalePriceDesc2.text = @"(최초 1회차 결제)";
                _rentalSalePriceDescHeight2.constant = 19;
            } else {
                _rentalSalePrice2.text = @"";
                _rentalSalePriceHeight2.constant = 0;
                _rentalSalePriceDesc2.text = @"";
                _rentalSalePriceDescHeight2.constant = 0;
            }
            
            _rentalPrice2.text = [NSString stringWithFormat:@"%@원(/월)", [AKCommonUtil getCurrencyFormat:[dic2[@"rental_month_price"] intValue]]];
            _rentalMonth2.text = [NSString stringWithFormat:@"%d개월", [dic2[@"rental_months"] intValue]];
        }
        
        _freeDelivery2.hidden = !isFreeDelivery;
        _freeDelivery2.text = isFreeDelivery ? @"무료배송" : @"";
        _verticalLine2.hidden = !isFreeDelivery;
        _verticalLineLeft2.constant = isFreeDelivery ? 5 : 0;
        _verticalLineRight2.constant = isFreeDelivery ? 5 : 0;
        
        [self updateBottomViewConstraint2];
    } else {
        _pView2.hidden = YES;
    }
}

@end
